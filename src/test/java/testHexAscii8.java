import com.sun.xml.internal.ws.encoding.MtomCodec;
import com.tess.modbus.register.Register;
import com.tess.modbus.register.TypeMapping;
import org.junit.Test;

import java.nio.ByteBuffer;
import java.util.Arrays;

/**
 * Created by Admin on 11.02.2019.
 */
public class testHexAscii8 {
    @Test
    public void testUniToBin(){
        Register r = new Register("Aa", 0, new TypeMapping("HEXASCII8"));
        ByteBuffer bb = ByteBuffer.allocate(8);
        r.getTypeMapping().getConverter().UniToBin(bb, 0, "1000000000000006");
        System.out.println(Arrays.toString(bb.array()));
    }

    @Test
    public void BinToUni(){
        Register r = new Register("Aa", 0, new TypeMapping("HEXASCII8"));
        ByteBuffer bb = ByteBuffer.allocate(8);
        bb.put((byte)0x0);
        bb.put((byte)0x10);
        bb.put((byte)0x0);
        bb.put((byte)0x0);
        bb.put((byte)0x0);
        bb.put((byte)0x0);
        bb.put((byte)0x6);
        bb.put((byte)0x0);
        System.out.println(r.getTypeMapping().getConverter().BinToUni(bb, 0));

    }
}
