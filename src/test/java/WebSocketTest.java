import com.google.gson.Gson;
import jdk.nashorn.internal.parser.JSONParser;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;
import org.json.simple.JSONObject;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Admin on 05.12.2018.
 */
public class WebSocketTest {
    public static class Command{
        public String cmd;

        public Command(String cmd) {
            this.cmd = cmd;
        }
    }
    public static class Auth extends Command{

        public String login = "sa";
        public String password = "111";

        public Auth() {
            super("auth_req");
        }
    }

    public static void main(String[] args) throws URISyntaxException, IOException, InterruptedException {
        System.out.println("WebSocket");
        Socket so = new Socket();
        so.connect(new InetSocketAddress("10.0.0.35", 8002));
        System.out.println(so.isConnected());
        so.close();
        Map<String,String> httpHeaders = new HashMap<String, String>();
        httpHeaders.put( "Cookie", "test" );
        Auth auth = new Auth();
        Gson gson = new Gson();
        String mess = gson.toJson(auth);

        WebSocketClient mWs = new WebSocketClient( new URI( "ws://10.0.0.35:8002" ), httpHeaders )
        {
            @Override
            public void onMessage( String message ) {

                //JSONObject obj = new JSONParser(message);// JSONObject(message);
                //String channel = obj.getString("channel");
                System.out.println(message);
            }

            @Override
            public void onOpen( ServerHandshake handshake ) {
                System.out.println( "opened connection" );
            }

            @Override
            public void onClose( int code, String reason, boolean remote ) {
                System.out.println( "closed connection" );
            }

            @Override
            public void onError( Exception ex ) {
                ex.printStackTrace();
            }

        };


        mWs.connectBlocking();
        //while(mWs.isOpen() == false);

        mWs.send(mess);
        Thread.sleep(1000);
        Command cmd = new Command("get_device_appdata_req");
        mWs.send(gson.toJson(cmd));
        Thread.sleep(1000);
        mWs.close();
    }
}
