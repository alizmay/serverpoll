import com.tess.multiserver.TelnetMonitor;

import java.io.IOException;

/**
 * Test unit for TelnetMonitor
 * Created by TESS on 15.06.2017.
 */
public class TelnetMonitorTest {
    public static void main(String arg[]) throws IOException, InterruptedException {
        TelnetMonitor telnetMonitor = new TelnetMonitor(9000);
        telnetMonitor.start();
        telnetMonitor.join();
    }
}
