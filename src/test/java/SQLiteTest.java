import com.tess.dao.SQLite;
import com.tess.modbus.register.*;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Set;

/**
 * Created by TESS on 04.08.2016.
 * TEST SQLite
 */
public class SQLiteTest {
    public static void main( String args[] ) throws IOException, ParseException, SQLException {
        SQLite sqLite = new SQLite();
        sqLite.init();
        System.out.println(sqLite.isExist("STU2000.Measure"));
        final Registers regs = RegistersFactory.getInstance("STU-1 2.060");
        Set<Register>set  = regs.getSetByGroup("DeviceHead");
        RegisteredValueSet registeredValues = new RegisteredValueSet();
        registeredValues.add(new RegisteredValue(regs.getRegister("DeviceHead:URI"),"com.rtu://COM4?1"));
/*        registeredValues.add(new RegisteredValue(regs.getRegister("DeviceHead:Device"),"STU-1 2.061"));
        registeredValues.add(new RegisteredValue(regs.getRegister("DeviceHead:SerNo"),"1"));
        registeredValues.add(new RegisteredValue(regs.getRegister("DeviceHead:Vendor"),"TESS-Engineering"));
        registeredValues.add(new RegisteredValue(regs.getRegister("DeviceHead:URI"),"com.rtu://COM2?1"));
        Map<String, RegisteredValueSet>map = new HashMap<>();
        map.put("Common",registeredValues);
        sqLite.insert(map);*/
        sqLite.update("Common", registeredValues,new RegisteredValueSet(){{add(new RegisteredValue(regs.getRegister("DeviceHead:SerNo"),"1"));}});
        List<RegisteredValueSet> res = sqLite.select("Common", set,
                new RegisteredValueSet(){{add(new RegisteredValue(regs.getRegister("DeviceHead:SerNo"),"1"));}});
        for(RegisteredValue val:res.get(0))
            System.out.println(val.getRegister().getMark() + " = " + val.getValue());
        sqLite.close();
    }
}
