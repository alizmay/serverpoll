import com.tess.CfgRoot;
import com.tess.host.modbus.StuHost;
import com.tess.modbus.register.Register;
import com.tess.modbus.register.RegisteredValue;
import com.tess.modbus.register.RegisteredValueSet;
import org.junit.Test;

import java.lang.invoke.MethodHandles;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.TreeSet;
import java.util.logging.Logger;

/**
 * Created by Admin on 05.07.2019.
 */
public class TypeConvertersTest {
    static Logger log = Logger.getLogger(MethodHandles.lookup().toString());
    @Test
    public void testLoraCfg() throws Exception {
        CfgRoot.initLogger();
        StuHost host = new StuHost();
        host.connect("com.rtu:/COM8?1");
        RegisteredValueSet ethalon = host.getRegisters().getRvs("auth=ABP, legalDutyCycle=OFF, autoDataRate=ON,"+
                "pa_boost = ON, ru868=OFF, dataRateMin=DR0, dataRateMax=DR5");
        host.write(ethalon);
        TreeSet<Register>set = ethalon.getSet();
        RegisteredValueSet test = host.read(set);
        for(RegisteredValue  e : ethalon)
            for(RegisteredValue t : test)
                if(e.getRegister().getMark().equals(t.getRegister().getMark())){
                    assert(e.getValue().equals(t.getValue()));
                }
    }
}
