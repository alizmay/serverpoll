import com.tess.dao.SQLite;

import com.tess.modbus.register.RegisteredValue;
import com.tess.modbus.register.RegisteredValueSet;
import com.tess.modbus.register.Registers;


import java.util.HashMap;
import java.util.Map;

/**
 * Created by TESS on 10.08.2016.
 */
public class SpringArchiveTest {
    public static void main(String[] arg) throws Exception {
        Registers registers = new Registers("bkn");
        SQLite sqlite = new SQLite();
        if(sqlite.isExist("bkn.Unit"))sqlite.createTable("bkn", registers.getSetByGroup("Unit"));
        if(sqlite.isExist("bkn.Group"))sqlite.createTable("bkn", registers.getSetByGroup("Group"));

        Map<String, RegisteredValueSet>in = new HashMap<>();
        RegisteredValueSet rvs = new RegisteredValueSet();
        rvs.add(new RegisteredValue(registers.getRegister("Group:NameGroup"), "example"));
        rvs.add(new RegisteredValue(registers.getRegister("Group:GroupID"), "1"));
//        in.put("bkn", )
//        sqlite.insert();
    }
}
