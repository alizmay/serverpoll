package com.tess.dao;

import com.tess.modbus.register.*;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.TreeSet;

import static org.junit.Assert.*;

/**
 * Created by Admin on 22.06.2018.
 */
public class SQLDataBaseTest {

    @Test
    public void testRead() throws Exception {
        DataBase db = new MsSQLServer2000();
        db.init();
        List<RegisteredValueSet>list = new ArrayList<>();
        Registers regs = RegistersFactory.load("POLLER");
        TreeSet<Register>ts = regs.get("IP", "Bkns", "Depot");
        System.out.println(db.read(list, "POLLER", ts, null));
        for (RegisteredValueSet registeredValues : list) {
            for (RegisteredValue registeredValue : registeredValues) {
                System.out.println(registeredValue.getRegister().getMark() + " = " + registeredValue.getValue());
            }
            System.out.println();
        }
    }

    @Test
    public void testWrite() throws Exception {
        DataBase db = new MsSQLServer2000();
        db.init();
        Registers regs = RegistersFactory.load("POLLER");
        RegisteredValueSet rvs = regs.getRvs("sysclock = 14.10.2018 10:00:00");
        RegisteredValueSet keys = rvs.pickOut("SerNo=00001");
        String log = db.write("", rvs, keys );
        System.out.println(log);
    }

    @Test
    public void testT() throws Exception{
        double f = -0.000000001;
        String s = String.format(Locale.ENGLISH, "%5.3f",f);
        System.out.println(s);
    }

}