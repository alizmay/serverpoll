package com.tess.dao;

import org.eclipse.paho.client.mqttv3.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by Admin on 23.08.2019.
 */
public class MqttPahoTest {
    final String TOPIC = "URG/Current/q1";
    final String PUBLISHER_ID = "#TESS#STU_M3#0001";
    IMqttClient publisher;
    IMqttClient subscriber;
    boolean mayDisconnect = false;

    @Before
    public void initPublisher() throws MqttException {
        publisher = new MqttClient("tcp://mqtt.eclipse.org:1883", PUBLISHER_ID);

        subscriber = new MqttClient("tcp://mqtt.eclipse.org:1883", PUBLISHER_ID);
//        subscriber.connect(options);

    }

    @After
    public void closePublisher() throws MqttException{
        publisher.disconnect();
        publisher.close();
        subscriber.disconnect();
        subscriber.close();
    }

    @Test
    public void testPublish() throws Exception {
        MqttConnectOptions options = new MqttConnectOptions();
        options.setAutomaticReconnect(true);
        options.setCleanSession(true);
        options.setConnectionTimeout(10);
        publisher.connect(options);
        MqttMessage mm = new MqttMessage("5.34".getBytes());
        mm.setQos(1);
        mm.setRetained(false);
        publisher.publish(TOPIC, mm);
        while(mayDisconnect == false);
    }

    @Test
    public void testSubscribe() throws Exception{
        MqttConnectOptions options = new MqttConnectOptions();
        options.setAutomaticReconnect(true);
        options.setCleanSession(true);
        options.setConnectionTimeout(10);
        subscriber.connect(options);
        subscriber.subscribe(TOPIC, new IMqttMessageListener() {
            @Override
            public void messageArrived(String topic, MqttMessage message) throws Exception {
                System.out.println(topic + ":" + message);
                mayDisconnect = true;
            }
        });
    }
}