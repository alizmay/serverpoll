package com.tess.host;

import com.tess.CfgRoot;
import com.tess.dao.MsSQLServer2000;
import com.tess.dao.SQLite;
import com.tess.host.dao.DaoHost;
import com.tess.modbus.register.Register;
import com.tess.modbus.register.RegisteredValueSet;
import com.tess.modbus.register.Registers;
import com.tess.modbus.register.RegistersFactory;
import com.tess.multiserver.AbstractWorker;
import com.tess.virtualstu.VirtualSTUThread;
import org.junit.Test;

import java.io.*;
import java.lang.invoke.MethodHandles;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

/**
 * Created by Admin on 12.07.2018.
 */
public class DaoHostTest {
    static Logger log = Logger.getLogger(MethodHandles.lookup().toString());
    static final String LOGGING_PROPERTIES = "\\Config\\logging.properties";

    public static void initLogger() {
        try (InputStream is = new FileInputStream(CfgRoot.PATH + LOGGING_PROPERTIES)) {//"E:\\tools\\JavaProjects\\TessBox\\Config\\logging.properties")) {
            LogManager.getLogManager().readConfiguration(is);
            Level l = log.getLevel();
            if (l == null) l = log.getParent().getLevel();
            log.log(l, "Current log level is " + l.toString());
        } catch (IOException e) {
            System.out.println("Logger is not available:" + e);
        }
    }

    @Test
    public void testTest() throws Exception{
        DaoHost daoHost = new DaoHost(new MsSQLServer2000());
        daoHost.connect();
        Registers rs = RegistersFactory.load("POLLER");
        List<RegisteredValueSet>r = new ArrayList<>();
        daoHost.read("", r, rs.getSetByGroup("SingleUrgUnits"), rs.getRvs("SerNo=00001"));
        RegisteredValueSet rvs = r.get(0);
        rvs = rs.getRvs("sysclock=14.10.2018 00:00:00");
        System.out.println(daoHost.write("", rvs, rs.getRvs("SerNo=00001")));
        System.out.println();

//
//        VirtualSTUThread vs = new VirtualSTUThread("urg4000n1");
//        DaoHost daoHost = new DaoHost(new SQLite());
//        AbstractWorker aw = new AbstractWorker(daoHost) {
//            @Override
//            public void run() {
//                setRegisters(RegistersFactory.getCommon());
//                while(true)
//                try {
//                    //List<RegisteredValueSet> result = read(registers.getSetByGroup("DeviceHead"), null);
//                    //System.out.println(result);
//                }catch(Exception e){
//                    logStackTrace(e);
//                    System.out.println(e);
//                }
//            }
//        };
//        daoHost.connect();
//        Thread t = new Thread(aw);
//        t.start();
//        vs.start();
//        t.join();
//        daoHost.close();
    }

}