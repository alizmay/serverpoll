package com.tess.host.xls;


import com.tess.CfgRoot;
import com.tess.ServerPollManager;
import com.tess.host.modbus.StuHost;
import com.tess.modbus.register.Register;
import com.tess.modbus.register.RegisteredValueSet;
import com.tess.modbus.register.Registers;
import com.tess.modbus.register.RegistersFactory;
import org.junit.Test;

import java.net.URI;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.*;

/**
 * Created by Admin on 21.04.2019.
 */
public class ExcelTest {

    @Test
    public void testReadArchive() throws Exception {
        CfgRoot.initLogger();
        StuHost stu = new StuHost();
        stu.connect("com.rtu:/COM6?1");
        Registers r = stu.getRegisters(); //        Registers r = RegistersFactory.load("STU2000");
        Set<Register> arc = r.getSetByGroup("AEUMark");
        Excel exc = new ExcelUni5Legacy();
        List<RegisteredValueSet> list = exc.readArchive("1010.xlsx", arc);
        //for(RegisteredValueSet rvs : list)System.out.println(rvs);
        RegisteredValueSet totals = exc.readRegisters("1010.xlsx", r.getSetByGroup("EUMark"));
        System.out.println(totals);
        //stu.close();
    }


}