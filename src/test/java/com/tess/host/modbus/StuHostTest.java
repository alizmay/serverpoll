package com.tess.host.modbus;

import com.tess.CfgRoot;
import com.tess.dataLinks.ComRTU;
import com.tess.modbus.register.*;
import com.tess.modbus.register.loader.AbstractConverter;
import com.tess.modbus.register.loader.Converter;
import com.tess.modbus.types.ArchiveClock;
import com.tess.modbus.types.Clock;
import org.junit.Test;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.invoke.MethodHandles;
import java.nio.ByteBuffer;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

/**
 * Created by Admin on 27.09.2018.
 */
public class StuHostTest {
    static Logger log = Logger.getLogger(MethodHandles.lookup().toString());
    static final String LOGGING_PROPERTIES =  "\\Config\\logging.properties";

    static void initLogger(){
        try(InputStream is = new FileInputStream(CfgRoot.PATH + LOGGING_PROPERTIES)){//"E:\\tools\\JavaProjects\\TessBox\\Config\\logging.properties")) {
            LogManager.getLogManager().readConfiguration(is);
            Level l = log.getLevel();
            if(l==null)l = log.getParent().getLevel();
            log.log(l, "Current log level is " + l.toString());
        } catch (IOException e) {
            System.out.println("Logger is not available:"+e); return;
        }
    }

    //@Test
    public void testOption3()throws Exception{
        initLogger();
        StuHost stu = new StuHost(new ComRTU());
        stu.open(null, null);
        stu.read();
        Registers r = stu.getRegisters();
        for(int i = 0; i < 10000; i++){
            RegisteredValueSet rvs = stu.getRegisters().getRvs("Options3:Scheme="+(int)(Math.random() * 31));
            rvs.add(new RegisteredValue(r.getRegister("Options3:CorrQviaT"), (Math.random() > 0.5?"ON":"OFF")));
            rvs.add(new RegisteredValue(r.getRegister("Options3:ApproxEnabled"), (Math.random() > 0.5?"ON":"OFF")));
            rvs.add(new RegisteredValue(r.getRegister("Options3:Pts"), (Math.random() <0.25?"100П":
                    Math.random()<0.5?"Pt100":Math.random() < 0.75?"500П": "Pt500")));

//            rvs.add(new RegisteredValue(r.getRegister("Options3:ModbusType"), (Math.random() <0.33?"RTU":
//                    Math.random()<0.66?"ASCII":"TCP")));
            rvs.add(new RegisteredValue(r.getRegister("Options3:ModbusType"), "TCP"));
            rvs.add(new RegisteredValue(r.getRegister("Options3:A2MinInUse"), (Math.random() > 0.5?"ON":"OFF")));
            String[] us = new String[]{"1.20", "2.40","4.80", "9.60", "14.4", "19.2", "28.8"};
            rvs.add(new RegisteredValue(r.getRegister("Options3:UartSpeed"), us[(int)(Math.random()*us.length)]));
            rvs.add(new RegisteredValue(r.getRegister("Options3:NetAddress"), "" + (int)(Math.random() * 247 + 1)));
            rvs.add(new RegisteredValue(r.getRegister("Options3:Print1"), "" + (int)(Math.random() * Integer.MAX_VALUE )));
            rvs.add(new RegisteredValue(r.getRegister("Options3:Print2"), "" + (int)(Math.random() * Integer.MAX_VALUE )));
//            RegisteredValueSet clo = new RegisteredValueSet(rvs);
            stu.write(rvs);
            RegisteredValueSet ss = stu.read(rvs.getSet());
            for(RegisteredValue rv : rvs){
                try {
                    if(rv.getRegister().equals("Options3:CorrQviaT"))continue;
                    if(!ss.getRegisteredValue(rv.getRegister().getMark()).getValue().equals(rv.getValue())) {
                        System.out.println("Mismath!");
                        throw new RuntimeException("Mismatch");
                    }
                } catch (Exception e) {
                    System.out.println("Error " + e);
                    break;
                }
            }
            System.out.println("Cycle of " + i + "is passed");

        }
    }

    //@Test
    public void testRead() throws Exception {
        initLogger();
        StuHost stu = new StuHost(new ComRTU());
        stu.open(null, null);
        stu.read();
        RegisteredValueSet rvs = stu.read(stu.getRegisters().get("Options3:ApproxEnabled,Options3:NetAddress"));
        rvs.getRegisteredValue("Options3:CorrQviaT").setValue("ВКЛ");
        rvs.getRegisteredValue("Options3:Scheme").setValue("6");
        stu.write(rvs);
        System.out.println(stu.getDeviceHead());
        stu.close();
    }



    @Test
    public void testTtrReadArchive() throws Exception{
        initLogger();
        StuHost stu = new StuHost(new ComRTU());
        stu.getPortal().open(null, null);
        stu.read();
        System.out.println(stu.getDeviceHead().getDevice());
        //stu.read(stu.getRegisters().get("Measure:sysclock"));
        String clock = stu.read("Measure:sysclock");
        ArchiveClock to = new ArchiveClock(ArchiveClock.ArchiveMode.HOUR);
        to.setRtClock(new Clock(clock).getRtClock());
        ArchiveClock from = (ArchiveClock)to.clone();
        from.addUnits(-5);
        List<RegisteredValueSet> arc = stu.read(from, to);
        for(RegisteredValueSet rvs : arc){
            for(RegisteredValue rv: rvs){
                System.out.print(rv.getValue()+" ");
            }
            System.out.println();
        }

    }

    private Map<Integer, List<LogStruct.Rec>> analizeLogList(List<LogStruct> list) {
        Map<Integer, List<LogStruct.Rec>>m = new HashMap<>();
        Clock first = list.get(list.size() - 1).time;
        for(int i = 0; i < 32 ; i++){
            m.put(i, new LinkedList<LogStruct.Rec>());
            m.get(i).add(new LogStruct.Rec(first, false));
        }
        int past = 0;
        for (int i1 = list.size() - 1; i1 >=0 ; i1--) {
            LogStruct lg = list.get(i1);
            int diff = past ^ lg.code;
            if (diff == 0x40000000) continue; //SYS omit
            for (int i = 0x1, y = 0; i != 0; i <<= 1, y++) {
                if ((diff & i) != 0) {
                    boolean active = (lg.code & i) != 0;
                    m.get(y).add(new LogStruct.Rec(lg.time, active));
                }
            }
            past = lg.code;
        }
        return m;
    }

    private List<RegisteredValueSet> createEmptyArchive(Registers r, ArchiveClock start, ArchiveClock end){
        List<RegisteredValueSet> arc = new ArrayList<>();
        for(ArchiveClock clo = new ArchiveClock(start); clo.asLong() <= end.asLong(); clo.increment(true)) {
            RegisteredValueSet rvs = new RegisteredValueSet();
            rvs.add(new RegisteredValue(r.getRegister("Archive:Datetime"), clo.toString()));
            arc.add(rvs);
        }
        return arc;
    }

    private void createSyntArchive(Registers r, List<LogStruct>lgl, List<RegisteredValueSet> arc){
        String[] psv = r.getRegister("Logger:Check").getTypeMapping().getConverter().getPossibleValues();
//        List<LogStruct>lgl = new ArrayList<>();
//        List<RegisteredValueSet> arc = new ArrayList<>();
        ArchiveClock start = new ArchiveClock(ArchiveClock.ArchiveMode.HOUR, new Clock().toString());
        ArchiveClock end = new ArchiveClock(start).addUnits(24);
        int sysstate = 0;
        int[] list32 = new int[32];
        ArchiveClock hour = new ArchiveClock(start).increment(true);
        for( Clock sysclock = new Clock(start.asLong()); sysclock.asLong() <= end.asLong(); sysclock.addMinutes(1)){
            if(sysclock.asLong() == hour.asLong()){
                if(list32[31] != 60) {
                    RegisteredValueSet rvs = r.getRvs("Archive:Datetime =" + hour.increment(false).toString());
                    hour.increment(true);
                    for (int o = 0; o < list32.length; o++)
                        rvs.add(new RegisteredValue(new Register("So:" + psv[o], -1, new TypeMapping("INT")), "" + list32[o]));
                    arc.add(rvs);
                }else
                    System.out.println();
                hour.increment(true);
                Arrays.fill(list32, 0);
            }

            int index = (int)(Math.random()*256);
            if(index == 31){
                if(sysstate == 0x80000000)sysstate = 0;
                    else sysstate = 1 << 31;
                LogStruct lg = new LogStruct(new Clock(sysclock.asLong()), sysstate);
                lgl.add(lg);
            }else if(index <32 && (sysstate & 0x80000000) == 0){
                sysstate ^= (0x01 << index);
                LogStruct lg = new LogStruct(new Clock(sysclock.asLong()), sysstate);
                lgl.add(lg);
            }
            for(int ii = 0x01, y = 0; ii != 0; ii <<= 1, y++)
                if((sysstate & ii)!=0)list32[y]++;
        }

        lgl.add(new LogStruct(hour, sysstate));
    }

    private void out(List<RegisteredValueSet>lout){
        for(RegisteredValueSet rvs: lout){
            for(RegisteredValue rv : rvs) {
                if(rv.getRegister().getUnitMark().equals("Datetime"))
                    System.out.print(rv.getRegister().getMark()+' '+rv.getValue() +"   ");
                else if(Double.parseDouble(rv.getValue())!=0)
                    System.out.print(rv.getRegister().getUnitMark()+'='+rv.getValue()+' ');
//                else System.out.print(rv.getRegister().getUnitMark());
//                System.out.print("\t\t");
            }
            System.out.println();
        }
    }

    @Test
    public void testSyntheticReadLog() throws Exception{
        initLogger();
        StuHost stu = new StuHost(new ComRTU());
        Registers r = RegistersFactory.load("STU4063");
        int  iteration = 0;
        do {
            List<LogStruct> list = new ArrayList<>();
            List<RegisteredValueSet> arc = new ArrayList<>();
            createSyntArchive(r, list, arc);
            Collections.reverse(list);
        out(arc);
            List<RegisteredValueSet>test = new ArrayList<>();
            for(RegisteredValueSet rvs : arc){
                RegisteredValue rov = rvs.getRegisteredValue("Archive:Datetime");
                RegisteredValue nrov = new RegisteredValue(rov.getRegister(), rov.getValue());
                RegisteredValueSet nrvs = new RegisteredValueSet();
                nrvs.add(nrov);
                test.add(nrvs);
            }
//            List<RegisteredValueSet> test = createEmptyArchive(r,
//                    new ArchiveClock(ArchiveClock.ArchiveMode.HOUR, arc.get(0).getRegisteredValue("Datetime").getValue()),
//                    new ArchiveClock(ArchiveClock.ArchiveMode.HOUR, arc.get(arc.size() - 1).getRegisteredValue("Datetime").getValue()));
            stu.anal(list, test, ArchiveClock.ArchiveMode.HOUR, r.getRegister("Logger:Check"));
        out(test);
            compare(arc, test, list);
            System.out.println("" + iteration++);
        }while(true);
    }

    private void compare(List<RegisteredValueSet> arc, List<RegisteredValueSet> test, List<LogStruct> list) {
        assert(arc.size() == test.size());
        for(int i = 0; i < arc.size(); i++){
            for(RegisteredValue rv : arc.get(i)){
                if(rv.getRegister().getUnitMark().equals("Datetime"))continue;
                RegisteredValueSet testrvs = test.get(i);
                if(testrvs.size() == 1 && testrvs.getRegisteredValue(0).getRegister().getUnitMark().equals("Datetime"))
                    continue;
                RegisteredValue testrv = testrvs.getRegisteredValue(rv.getRegister().getMark());
                if(testrv == null){
                    out(arc);
                    System.out.println();
                    out(test);
                    System.out.println(rv);
                }
                double e = Double.parseDouble(rv.getValue());
                double t = Double.parseDouble(testrv.getValue());
                if(e != t){
                    System.out.println("test=" + testrv.getValue() + "   etalon=" + rv.getValue());
                    assert(false);
                }
            }
        }
    }

    @Test
    public void testReadLog() throws Exception {
        initLogger();
        StuHost stu = new StuHost(new ComRTU());
        stu.open(null, null);
        stu.read();
        String clock = stu.read("sysclock");
        ArchiveClock end  = new ArchiveClock(ArchiveClock.ArchiveMode.HOUR, clock);
        end.increment(true);
        ArchiveClock start = new ArchiveClock(end);
        start.addUnits(-24);
        List<LogStruct> list = stu.readLog(stu.getRegisters().get("Logger:Time, Logger:Check"), start);
        //List<RegisteredValueSet> arc = stu.read(start, end);
        List<RegisteredValueSet>arc = createEmptyArchive(stu.getRegisters(), start, end);

        stu.anal(list, arc, start.getArchiveMode(), stu.getRegisters().getRegister("Logger:Check"));
    }

}