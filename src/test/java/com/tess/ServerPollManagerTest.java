package com.tess;

import com.tess.virtualstu.VirtualSTUThread;
import org.junit.Test;

import java.lang.invoke.MethodHandles;
import java.util.logging.Logger;

import static org.junit.Assert.*;

/**
 * Created by Admin on 13.07.2018.
 */
public class ServerPollManagerTest {
    static Logger log = Logger.getLogger(MethodHandles.lookup().toString());
    @Test
    public void testMain() throws Exception {
        CfgRoot.initLogger();
        VirtualSTUThread vs = new VirtualSTUThread("urg4000n1");
        Thread.sleep(1000);
        vs.start();
        ServerPollManager.main(new String[0]);
        System.out.println("The end");
    }
}