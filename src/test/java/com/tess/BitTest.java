package com.tess;

import com.tess.dao.MsExcel;
import com.tess.modbus.register.Register;
import com.tess.modbus.register.Registers;
import com.tess.modbus.register.RegistersFactory;
import com.tess.modbus.register.TypeMapping;
import org.junit.Test;

/**
 * Created by Admin on 12.04.2019.
 */
public class BitTest {
    @Test
    public void bitTest(){
        MsExcel excel = new MsExcel();
        Register r = new Register("eee", -1, new TypeMapping("BIT"));
        String val = "ON";
        String res = excel.convertToDBType(r, val);
        System.out.println(res);
    }
}
