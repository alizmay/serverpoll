package com.tess.mqtt;

import com.tess.host.client.RequestOwner;
import com.tess.modbus.register.RegisteredValue;
import com.tess.modbus.register.RegisteredValueSet;
import com.tess.modbus.register.Registers;
import com.tess.modbus.register.RegistersFactory;
import com.tess.modbus.types.ArchiveClock;
import junit.framework.Assert;
import org.eclipse.paho.client.mqttv3.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.Scanner;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * Created by Admin on 26.08.2019.
 */
public class MqttClientStuTest {
    final String MQTT_SERVER_MOS = "tcp://test.mosquitto.org:1883";
    final String MQTT_SERVER_LOC = "tcp://10.0.0.16:1883";
    IMqttClient subscriber = null;

    public void initSubscriber() throws MqttException {
        subscriber = new MqttClient(MQTT_SERVER_LOC, UUID.randomUUID().toString(), new MqttValidFilePersistence());
        MqttConnectOptions options = new MqttConnectOptions();
        options.setAutomaticReconnect(true);
        options.setCleanSession(true);
        options.setConnectionTimeout(10);
        subscriber.connect(options);
        Assert.assertTrue(subscriber.isConnected());
        subscriber.subscribe("PPD_00001/archive/+", new IMqttMessageListener() {
            @Override
            public void messageArrived(String topic, MqttMessage message) throws Exception {
                System.out.println(topic +':' + message);

            }
        });
    }

    @Test
    public void testMqttClientStu1() throws Exception {
        initSubscriber();
        MqttClientStu mcs = new MqttClientStu(MQTT_SERVER_LOC) {
            @Override
            public void pushArchive(com.tess.host.client.RequestOwner owner, ArchiveClock from, RegisteredValueSet united) {

            }

            @Override
            public void pushCurrent(RequestOwner owner, RegisteredValueSet set) {

            }
        };
        Registers ttf = RegistersFactory.load("POLLERTTF");
        RegisteredValueSet rvs = ttf.getRvs("Cppd:q1Hour=14.22,Cppd:T1=14.5, Cppd:P1=1.2, Cppd:AccV=6.0");
        ArchiveClock clock = new ArchiveClock(ArchiveClock.ArchiveMode.HOUR);
        rvs.add(new RegisteredValue(ttf.getRegister("Cppd:Date_"), clock.toString()));
        final StringBuilder sb = new StringBuilder();
//        mcs.sendMessage(new RequestOwner() {
//            @Override
//            public void readBack(List<RegisteredValueSet> list) {
//
//            }
//
//            @Override
//            public void log(String string) {
//                sb.append(string); sb.append('\n');
//            }
//
//            @Override
//            public String getTableName() {
//                return "PPD_00001/archive/";
//            }
//        }, clock, rvs);
        while(mcs.isDone() == false);
        System.out.println(sb.toString());
        TimeUnit.SECONDS.sleep(20);
        subscriber.disconnect();
        mcs.close();

    }

    @After
    public void close() throws MqttException {
        if(subscriber != null)subscriber.close();
    }
}