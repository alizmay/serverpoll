package com.tess.modbus;

import com.sun.deploy.panel.TreeRenderers;
import com.tess.CfgRoot;
import com.tess.dataLinks.ComRTU;
import com.tess.dataLinks.InetPortal;
import com.tess.dataLinks.Portal;
import com.tess.modbus.register.*;
import com.tess.modbus.types.ArchiveClock;
import com.tess.virtualstu.TcpServerPool;
import com.tess.virtualstu.VirtualSTU;
import com.tess.virtualstu.VirtualSTUCheater;
import com.tess.virtualstu.VirtualSTUSpammer;
import org.junit.Test;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.invoke.MethodHandles;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import static org.junit.Assert.*;

/**
 * Created by Admin on 24.06.2018.
 */
public class ModbusRegisterReadTest {
    static final String LOGGING_PROPERTIES =  "\\Config\\logging.properties";
    static Logger log = Logger.getLogger(MethodHandles.lookup().toString());
    public static void initLogger() {
        try (InputStream is = new FileInputStream(CfgRoot.PATH + LOGGING_PROPERTIES)) {//"E:\\tools\\JavaProjects\\TessBox\\Config\\logging.properties")) {
            LogManager.getLogManager().readConfiguration(is);
            Level l = log.getLevel();
            if (l == null) l = log.getParent().getLevel();
            log.log(l, "Current log level is " + l.toString());
        } catch (IOException e) {
            System.out.println("Logger is not available:" + e);
        }
    }

    private class InetPortalTest extends InetPortal{
        public InetPortalTest() {
        }

        @Override
        protected int append(byte[] buffer, int length) throws IOException {
            int len;
            try{
                byte[] fakeReply = new byte[]{(byte)0x01,(byte)0x03,(byte)0x34,
                        (byte)0x9E,(byte)0xC6,(byte)0x00,(byte)0x00,(byte)0x6F,
                        (byte)0x28,(byte)0x3E,(byte)0xCF,(byte)0x00,(byte)0x10,
                        (byte)0x00,(byte)0x00,(byte)0x4E,(byte)0x14,(byte)0x3E,
                        (byte)0xBD,(byte)0x7B,(byte)0xCD,(byte)0x47,(byte)0x6B,
                        (byte)0xA5,(byte)0x9A,(byte)0x45,(byte)0x2E,(byte)0x68,
                        (byte)0xDC,(byte)0x23,(byte)0xFE,(byte)0x41,(byte)0x2A,
                        (byte)0x00,(byte)0x00,(byte)0x00,(byte)0x00,(byte)0xB6,
                        (byte)0x31,(byte)0x41,(byte)0x20,(byte)0x91,(byte)0xCB,
                        (byte)0x41,(byte)0x33,(byte)0x32,(byte)0x0B,(byte)0x1D,
                        (byte)0x0F,(byte)0x12,(byte)0x06,(byte)0x00,(byte)0x00,
                        (byte)0x00,(byte)0x00,(byte)0xBE,(byte)0x64,(byte)0x00};
//                fakeReply = wrapper.wrapForWrite(fakeReply);

                if(length == fakeReply.length){
                    return length;
                }

                for (int i = 0; i < fakeReply.length; i++) {
                    buffer[i + length] = fakeReply[i];
                }
                len = fakeReply.length;
                //          len = inputStream.available();
                if(len != 0){
                    //inputStream.readRegister(buffer, length, len);
                    length += len;
                }
            }
            catch (Exception ignored){}
            return length;
        }

    }


    @Test
    public void testReadRegister() throws Exception {
        initLogger();
        TcpServerPool tsp = new TcpServerPool(10000);
        tsp.add(new VirtualSTU("urg4000n1"));
//        tsp.add(new VirtualSTU("urg4000n2"));
//        tsp.add(new VirtualSTU("urg4000n3"));
        tsp.start();
        InetPortalTest ip = new InetPortalTest();
        ip.open("127.0.0.1:10000",null);
        ModbusRegisterRead mrr = new ModbusRegisterRead(ip, 1);
        //Registers r = RegistersFactory.getInstance("URG2KM 4.050.i343");
        //TreeSet<Register> treeSet = r.get("Measure:q1, ");//,"q1", "sysclock", "sysstate");
        Registers r = RegistersFactory.load("POLLER");
        TreeSet<Register> treeSet = r.getSetByGroup("UrgUnits");
        RegisteredValueSet rvs = mrr.readRegister(treeSet);
        for (RegisteredValue rv : rvs) {
            System.out.println(rv);
        }
        System.out.println(rvs.getRegisteredValue(0).getValue());
        ModbusArchive ma = new ModbusArchive(ip, 1);
        RegisteredValueSet arc = ma.readArchive(new ArchiveClock(ArchiveClock.ArchiveMode.HOUR, "26.08.2018 10:00:00"),
                r.getSetByGroup("Archive"));
        System.out.println(arc);
        ip.close();
        System.out.println("Trying to close connection");
        tsp.join();
//        for(int i = 10000; i < 10003; i++){
//            TcpServerPool tsp = new TcpServerPool(i);
//            tsp.add(new VirtualSTU("urg4000n1"));
//            tsp.add(new VirtualSTU("urg4000n2"));
//            tsp.add(new VirtualSTU("urg4000n3"));
//        }
        System.out.println("Over");
    }

    @Test
    public void testReadRegisterRepeat() throws Exception{
        initLogger();
        Portal com = new ComRTU();
        com.open(null, null);
        //Registers r = RegistersFactory.load("STU4000");
        TreeSet<Register>set = new TreeSet<>();
        set.add(new Register("zerro", 0, new TypeMapping("INT")));
        set.add(new Register("first", 1, new TypeMapping("FLOAT")));
        ModbusRegisterRead mrr = new ModbusRegisterRead(com, 1);
        List<RegisteredValueSet> res = mrr.readRegistersRepeat(set, 10, 0);
        System.out.println(res);
        com.close();
    }
}