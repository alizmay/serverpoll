package com.tess.modbus;

import com.tess.CfgRoot;
import com.tess.ServerPollManager;
import com.tess.host.modbus.StuHost;
import com.tess.host.xls.Excel;
import com.tess.host.xls.ExcelUni5Legacy;
import com.tess.modbus.register.Register;
import com.tess.modbus.register.RegisteredValue;
import com.tess.modbus.register.RegisteredValueSet;
import com.tess.modbus.register.Registers;
import com.tess.modbus.types.ArchiveClock;
import org.junit.Test;

import java.lang.invoke.MethodHandles;
import java.net.URI;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

import static org.junit.Assert.*;

/**
 * Created by Admin on 19.04.2019.
 */
public class ModbusArchiveWriteTest {
    static Logger log = Logger.getLogger(MethodHandles.lookup().toString());
    boolean justRead = false;
    static{
        CfgRoot.initLogger();
    }
    @Test
    public void testWriteArchive() throws Exception {

        StuHost stu = new StuHost();
        stu.connect("com.rtu:/COM6?1");
        System.out.println(stu.getRegisters().getSetName());
        ArchiveClock from = new ArchiveClock(ArchiveClock.ArchiveMode.DAY, "02.04.19 00:00");
        ModbusArchive ma = new ModbusArchive(stu.getPortal(), 1);
        //RegisteredValueSet arc = stu.getRegisters().getRvs("Archive:q1=153");
        RegisteredValueSet arc = ma.readArchive(from, stu.getRegisters().getSetByGroup("Archive"));
        if(!justRead)arc.getRegisteredValue("Archive:q1").setValue(null/*"123.456"*/);
        System.out.println(arc);
        if(!justRead) {
            ModbusArchiveWrite maw = new ModbusArchiveWrite(stu.getPortal(), 1);
            maw.writeArchive(from, arc);
        }
        arc = ma.readArchive(from, stu.getRegisters().getSetByGroup("Archive"));
        System.out.println(arc.getRegisteredValue("q1"));
        stu.close();
    }

    @Test
    public void testConsoleReadArchive() throws Exception{


        String comm = "ADay = \"1010.xlsx\"";
        int srt = comm.indexOf("\"");
        int end = comm.indexOf("\"", srt+1);
        String fileName = comm.substring(srt+1, comm.indexOf("\"", srt+1));
        String cmm = comm.split("=")[0].trim();
        ArchiveClock.ArchiveMode am = null;
        if(cmm.equals("ADay"))am = ArchiveClock.ArchiveMode.DAY;
        else if(cmm.equals("AHour"))am = ArchiveClock.ArchiveMode.HOUR;
        else if(cmm.equals("AMon"))am = ArchiveClock.ArchiveMode.MONTH;
        else am = ArchiveClock.ArchiveMode.TWOMIN;


        StuHost stu = new StuHost();
        stu.connect("com.rtu:/COM6?1");
        Registers r = stu.getRegisters(); //        Registers r = RegistersFactory.load("STU2000");
        Set<Register> arc = r.getSetByGroup("AEUMark");
        Excel exc = new ExcelUni5Legacy();
        System.out.print("Reading arc data from " + fileName + " ...");
        List<RegisteredValueSet> list = exc.readArchive(fileName, arc);
        System.out.println("Ok");
        //for(RegisteredValueSet rvs : list)System.out.println(rvs);
        System.out.print("Reading totals data from " + fileName + " ...");
        RegisteredValueSet totals = exc.readRegisters(fileName, r.getSetByGroup("EUMark"));
        System.out.println("Ok");
        System.out.println(list.size() + " records at total");
        System.out.println("Starting to write...");
        ModbusArchiveWrite maw = new ModbusArchiveWrite(stu.getPortal(), stu.getNetAddress());
        for(RegisteredValueSet rvs : list){
            ArchiveClock recordClock = new ArchiveClock(am, rvs.getRegisteredValue("AEUMark:_aDateSh1").getValue());
            System.out.print(recordClock.toShortedString() + "...");
            maw.writeArchive(recordClock, rvs);
            System.out.println("Ok");
        }
        System.out.print("Writing totals...");
        for(RegisteredValue rv :totals) {
            RegisteredValueSet in = new RegisteredValueSet();
            in.add(rv);
            stu.write(in);
        }
        System.out.println("Ok");
        stu.close();
    }
}