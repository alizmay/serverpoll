package com.tess.modbus.register;

import com.tess.host.modbus.StuHost;
import com.tess.modbus.DeviceHead;
import com.tess.modbus.register.versionSystem.VersionFactory;
import com.tess.modbus.register.versionSystem.VersionSystem;
import com.tess.modbus.register.versionSystem.VersionSystemClass;
import org.junit.Test;

import java.io.IOException;
import java.net.URI;
import java.util.Properties;

/**
 * Created by Admin on 04.05.2019.
 */
public class RegistersFactoryTest {

//    static class VersionUniSystemClass  extends VersionSystemClass implements VersionUniSystem{
//
//        private String xltName, cutName;
//
//        public VersionUniSystemClass(Properties prop){
//            super(prop);
//            xltName = prop.getProperty("xlt");
//            cutName = prop.getProperty("cut");
//        }
//
//        @Override
//        public String getXLTName() {
//            return xltName;
//        }
//
//        @Override
//        public String getCutName() {
//            return cutName;
//        }
//    }
//
//    static class DeviceHeadUni extends DeviceHead{
//
//        private VersionUniSystem versionUniSystem = null;
//
//        public DeviceHeadUni() throws Exception {
//            super();
//        }
//
//        @Override
//        public void recognize() throws IOException {
//            versionUniSystem = loadDevInfo(new VersionFactory<VersionSystem>() {
//                @Override
//                public VersionSystem getInstance(Properties prop){
//                    return new VersionUniSystemClass(prop);
//                }
//            },  this);
//            versionSystem = versionUniSystem;
//        }
//
//        public VersionUniSystem getVersionUniSystem() {
//            return versionUniSystem;
//        }
//    }

    @Test
    public void testLoadDevInfo() throws Exception {
        StuHost stu = new StuHost();
        stu.connect("com.rtu:/COM2?1");
        DeviceHead dh = stu.getDeviceHead();
        System.out.println(dh.getVersionSystem().getRegistersName() + "  " +  dh.getVersionSystem().getXLTName());
        stu.close();

    }


}