package com.tess.modbus.register;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Admin on 23.06.2018.
 */
public class RegisteredValueSetTest {

    @Test
    public void testCopy() throws Exception {
        Registers registers = RegistersFactory.load("POLLER");
        RegisteredValueSet main = registers.getRvs("uuID=1, Channel=2, q1=45.54, Name=1123");
        RegisteredValueSet tw = registers.getRvs("V1=58.5, q1=1");
        main.copy(tw,"V1, q1 ");
        for (RegisteredValue registeredValue : main) {
            System.out.println(registeredValue);
        }
        System.out.println();
        RegisteredValueSet next = registers.getRvsByGroup("UrgGroups");
        next.copy(main, "Bkns = Channel, Depot = Name");
        for (RegisteredValue registeredValue : next) {
            System.out.println(registeredValue);
        }
    }
}