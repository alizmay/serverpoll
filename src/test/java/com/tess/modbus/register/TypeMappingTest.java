package com.tess.modbus.register;

import com.tess.modbus.register.loader.AbstractConverter;
import org.junit.Test;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import static org.junit.Assert.*;

/**
 * Created by Admin on 12.06.2018.
 */
public class TypeMappingTest {

    @Test
    public void testTransformation() throws Exception {
        Registers registers = RegistersFactory.load("POLLER");
        RegisteredValue rv = new RegisteredValue(registers.getRegister("V1"), "1.5");
        byte[] bytes = new byte[10];
        ByteBuffer bb = ByteBuffer.wrap(bytes);
        bb.order(ByteOrder.LITTLE_ENDIAN);
        rv.getRegister().getTypeMapping().transformation(bb ,0, "800.0");
        for(byte b: bytes)
            System.out.print(String.format("%02X ", b));
        System.out.println();
        Register ro = new Register("VV1", 0, new TypeMapping("TOTAL"));
        bb = ByteBuffer.wrap(bytes);
        bb.order(ByteOrder.BIG_ENDIAN);
        String v = ro.getTypeMapping().transformation(bb, 0);
        //AbstractConverter.swap();
        System.out.println(v);
    }

    @Test
    public void testTransformation1() throws Exception {

    }
}