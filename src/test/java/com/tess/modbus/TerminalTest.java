package com.tess.modbus;

import com.sun.xml.internal.ws.util.ByteArrayBuffer;
import com.tess.dataLinks.AcceptedPortal;
import com.tess.dataLinks.InetPortal;
import org.junit.Test;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

import static org.junit.Assert.*;

/**
 * Created by Admin on 16.11.2018.
 */
public class TerminalTest {


        @Test
        public void testRun() throws IOException, InterruptedException {

            InetPortal ip = new InetPortal();
            Thread th = new Thread() {
                @Override
                public void run() {
                    try {
                        ServerSocket ss = new ServerSocket(10000);
                        Socket so = ss.accept();
                        AcceptedPortal ap = new AcceptedPortal("RTU", so);
                        while(isInterrupted());
                        //Terminal ter = new Terminal(ap, new ByteArrayBuffer());

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }


            };
            th.start();
            ip.open("127.0.0.1:10000", null);
            Terminal terminal = new Terminal(ip, new BufferedOutputStream(System.out));
            terminal.run();
            th.interrupt();
            th.join();
        }
}