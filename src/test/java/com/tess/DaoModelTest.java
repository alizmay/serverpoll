package com.tess;

import com.tess.modbus.register.*;
import com.tess.modbus.types.Clock;
import org.junit.Test;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.*;

/**
 * Created by Admin on 07.06.2018.
 */
public class DaoModelTest {
    DaoModel daoModel;

    {
        this.daoModel = new DaoModel("MsSQLServer2000",RegistersFactory.load("POLLER"));;
        try {
            daoModel.connect();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testCreateTable() throws Exception{
        daoModel = new DaoModel("MsSQLServer2000",RegistersFactory.load("POLLER"));
        daoModel.connect();
        Set<Register> set = daoModel.getRegisters().getSetByGroup("UrgGroups");
        set.addAll(daoModel.getRegisters().getSetByGroup("UrgUnits"));
        daoModel.createTable(set);
        daoModel.createTable(daoModel.getRegisters().getSetByGroup("Cppd"));
        daoModel.close();
        assertTrue(true);
    }

    @Test
    public void testWrite() throws Exception{
//        daoModel = new DaoModel("MsSQLServer2000",RegistersFactory.load("POLLER"));
//        daoModel.connect();
        Registers dr = daoModel.getRegisters();
        RegisteredValueSet data = dr.getRvs("q1=1.545, V1=5.545, uuID=1, Active=ON");
        data.add(new RegisteredValue(dr.getRegister("UrgUnits:LastSession"), null));
        RegisteredValueSet keys = data.pickOut("uuID");
        System.out.println(daoModel.write(data, keys));
//        daoModel.write(dr.getRvs("IP=127.0.0.1:10000"));
//        daoModel.write(dr.getRvs("Bkns=124"), dr.getRvs("_ID=1"));

        daoModel.close();

    }

    @Test
    public void testRead() throws Exception {
        Set<Register>s = daoModel.getRegisters().getSetByGroup("UrgUnits");
        List<RegisteredValueSet>list = new ArrayList<>();
        System.out.println(daoModel.read(list, s));
        for(RegisteredValueSet rvs: list) {
            for(RegisteredValue rv:rvs) {
                System.out.println(rv);
                assert(!rv.getRegister().getTypeMapping().toString().equals("UNKNOWN"));
            }

        }

    }

    @Test
    public void testRead1() throws Exception {
        Set<Register>s = daoModel.getRegisters().getSetByGroup("UrgGroups");
        List<RegisteredValueSet>list = new ArrayList<>();
        System.out.println(daoModel.read(list, s, daoModel.getRegisters().getRvs("IP=40.0.0.1:10000")));
        for(RegisteredValueSet rvs: list)
            for(RegisteredValue rv: rvs){
                System.out.println(rv);
            }

    }

    @Test
    public void testWriteRead(){
        Registers dr = daoModel.getRegisters();
        RegisteredValueSet data = dr.getRvs("q1=1.545, V1=5.545, uuID=1, Active=ON");
        data.add(new RegisteredValue(dr.getRegister("UrgUnits:LastSession"), null));
        RegisteredValueSet keys = data.pickOut("uuID");
        System.out.println(daoModel.write(data, keys));
        Set<Register>set = data.getSet();
        List<RegisteredValueSet>list = new ArrayList<>();
        RegisteredValueSet ks = dr.getRvs("uuID = 1");
        System.out.println(daoModel.read(list, set, ks));
        RegisteredValueSet reply = list.get(0);
        assert(reply.size() == data.size());
        for (RegisteredValue registeredValue : reply) {
            for (RegisteredValue value : data) {
                if(value.getRegister().getMark().equals(registeredValue.getRegister().getMark())) {
                    assert (value.getRegister().getMark().equals(registeredValue.getRegister().getMark()));
                    if(value.getValue() == null)
                    assert(value.getValue() == registeredValue.getValue());
                        else assert (value.getValue().equals(registeredValue.getValue()));
                }
            }
        }
    }

    @Test
    public void testInsertCells(){
        Registers dr = daoModel.getRegisters();
        RegisteredValueSet data = dr.getRvs("GroupID=1, Channel=1, Active=ON");
        System.out.println(daoModel.write(data, null));
        data = dr.getRvs("GroupID=1, Channel=2, Active=OFF");
        System.out.println(daoModel.write(data, null));
        data = dr.getRvs("GroupID=1, Channel=3, Active=OFF");
        System.out.println(daoModel.write(data, null));
    }
}