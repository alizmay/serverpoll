package com.tess.dataLinks;

import com.tess.host.modbus.StuHost;
import com.tess.host.special.TrialModeV5;
import org.junit.Test;

import java.net.URI;

/**
 * Created by Admin on 02.04.2019.
 */
public class ListenComTest {

    @Test
    public void testGetReply() throws Exception {
        ComRTU cr = new ComRTU();
        StuHost stu = new StuHost(cr);
        stu.connect("com.rtu:/COM7?1");
        TrialModeV5 tm = new TrialModeV5(stu);
        tm.startTrial();
        for(int i = 0; i < 20; i++){
            String s = tm.getFlow1StringMessage();
            if(s != null) System.out.println(s);
            else Thread.sleep(300);
        }
        tm.stopTrial();
    }
}