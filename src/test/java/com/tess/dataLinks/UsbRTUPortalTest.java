package com.tess.dataLinks;

import com.tess.modbus.Bootloader;
import com.tess.modbus.ModbusDeviceID;
import com.tess.modbus.register.RegisteredValueSet;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.*;

/**
 * Created by Admin on 11.09.2018.
 */
public class UsbRTUPortalTest {
    @Test
    public void testUsbRTUPortal() throws InterruptedException, IOException {
        do {
        } while (UsbRTUPortal.isConnected() == false);
        UsbRTUPortal up = UsbRTUPortal.getUsbRTUPortal();
        up.open(null, "ms=50000");
        ModbusDeviceID mid = new ModbusDeviceID(up, 1);
        RegisteredValueSet rvs = mid.readID();
        System.out.println(rvs);
        Bootloader bld = new Bootloader(up);
        byte[] b = new byte[]{0,0,0};
        bld.load(b, 0);
        System.out.println(rvs);
    }

    @Test
    public void testFTDIAutoconnection() throws Exception {
        System.out.println("Starting monitor");
        UsbRTUPortal.startFTDIMonitor();
        UsbRTUPortal usb = null;
        while ((usb = UsbRTUPortal.getFtdiMonitor().getUsbPortal()) == null) {
            Thread.sleep(2000);
            System.out.println('.');
        }
        ;

        System.out.println();
        System.out.println("Connected: #" + UsbRTUPortal.getFtdiMonitor().getName() + " " + usb);
        System.out.println("Wait to disconnect...");
        while ((usb = UsbRTUPortal.getFtdiMonitor().getUsbPortal()) != null){
            Thread.sleep(2000);
            System.out.print(".");
        }

    }

}