package com.tess.dataLinks;

import com.tess.modbus.wrappers.WrapperRTU;
import org.junit.Test;

import java.net.URI;

import static org.junit.Assert.*;

/**
 * Created by Admin on 21.11.2019.
 */
public class ModemPortalTest {

    @Test
    public void testOpen() throws Exception {
        StringBuilder sb = new StringBuilder();
        try {

        Portal p = Portals.portalFactory(new URI("modem.rtu:/COM6?1#baud=9600,ms=20000,tries=2,is=atq0e0v1&d2,num=89176615999"),sb);
//            Portal p = new ModemPortal(new WrapperRTU());
            p.setLogBuilder(sb);
            p.open(null, null);

            p.setLogBuilder(sb);
            p.close();
        }catch(Exception e){

        }finally {
            System.out.println(sb.toString());
        }

    }
}