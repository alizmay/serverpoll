package com.tess.dataLinks;

import com.tess.CfgRoot;
import org.junit.Test;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.invoke.MethodHandles;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import static org.junit.Assert.*;

/**
 * Created by Admin on 21.11.2018.
 */
public class CDCMonitorTest {
    static final String LOGGING_PROPERTIES =  "\\Config\\logging.properties";
    static Logger log = Logger.getLogger(MethodHandles.lookup().toString());
    public static void initLogger() {
        try (InputStream is = new FileInputStream(CfgRoot.PATH + LOGGING_PROPERTIES)) {//"E:\\tools\\JavaProjects\\TessBox\\Config\\logging.properties")) {
            LogManager.getLogManager().readConfiguration(is);
            Level l = log.getLevel();
            if (l == null) l = log.getParent().getLevel();
            log.log(l, "Current log level is " + l.toString());
        } catch (IOException e) {
            System.out.println("Logger is not available:" + e);
        }
    }

    @Test
    public void testStart() throws Exception {
        CDCMonitor cdcmo = new CDCMonitor();
        cdcmo.start();
        while(!cdcmo.isConnected());
        System.out.println(cdcmo.getComRTU() == null? "null":cdcmo.getComRTU());
    }
}