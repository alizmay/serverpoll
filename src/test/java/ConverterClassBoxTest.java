import com.tess.modbus.register.Register;
import com.tess.modbus.register.TypeMapping;

import java.nio.ByteBuffer;

/**
 * Created by Admin on 12.01.2018.
 */
public class ConverterClassBoxTest {
    public static void main(String args[]){
//        ConverterClassBox ccb = new ConverterClassBox(ClassLoader.getSystemClassLoader());
//        ByteBuffer bb = ByteBuffer.allocate(10);
//        bb.put((byte)0x12);
//        bb.put((byte)0x34);
//        String ss = ccb.match("INT").BinToUni(bb,0);
//        System.out.println(ss);
        Register r = new Register("Ee", 0x0000, new TypeMapping("IBIT0"));
        ByteBuffer bb = ByteBuffer.allocate(20);
        bb.putShort((short)0x1234);
        String res = r.getTypeMapping().transformation(bb, 0);
        System.out.println(res);
        res = r.getTypeMapping().getConverter().getPossibleValues()[1];
        r.getTypeMapping().transformation(bb, 0, res);
        System.out.println(r.getTypeMapping().toString());
        System.out.println(r.getTypeMapping().transformation(bb, 0));
    }


}
