
import com.tess.Console;
import com.tess.dataLinks.ComRTU;
import com.tess.host.modbus.StuHost;
import com.tess.modbus.types.ArchiveClock;
import com.tess.modbus.wrappers.WrapperASCII;
import org.junit.Test;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by TESS on 08.07.2016.
 * Test com rtu
 */
public class PortalBusComRtuTest {
    @Test
    public void comRtuTest() throws IOException {
        ComRTU cr = new ComRTU(new WrapperASCII());
        StuHost stu = new StuHost(cr);
        stu.open("COM2", null);
        stu.read();
        stu.close();

    }
//    public static void main(String args[]) throws IOException, URISyntaxException {
//        PortalBus pb = new PortalBus(new URI("com.rtu://COM4?1"));
//        pb.connect("COM4?1");
//
//        //pb.connect("89.113.5.20:502?1");
//        Map<String, String> data = new HashMap<>();
//        data.put("Device", null);
//        data.put("sysclock", null);
//        pb.moveData(data);
//        for(Map.Entry<String, String>pair:data.entrySet()){
//            Console.out(pair.getKey() + ":" + pair.getValue() );
//        }
//        ArchiveClock ac = new ArchiveClock(ArchiveClock.ArchiveMode.HOUR, data.getRegister("sysclock"));
//        Map<ArchiveClock, Map<String, String>>archiveMap = new HashMap<>();
//        archiveMap.put(new ArchiveClock(ac), null);
//        archiveMap.put(new ArchiveClock(ac.increment(false)), null);
//        archiveMap.put(new ArchiveClock(ac.increment(false)), null);
//        archiveMap.put(new ArchiveClock(ac.increment(false)), null);
//        pb.moveData(archiveMap);
//        for(Map.Entry<ArchiveClock, Map<String, String>>pair:archiveMap.entrySet()){
//            System.out.println(pair.getKey() + ":");
//            for(Map.Entry<String, String>pp:pair.getValue().entrySet()){
//                System.out.println("    " + pp.getKey() + ":" + pp.getValue());
//            }
//        }
//        pb.disconnect();
//    }
}

