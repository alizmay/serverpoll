package com.tess.multiserver;

import com.tess.Console;

import java.io.*;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketAddress;

/**
 * TelnetMonitor for management of server
 * Created by TESS on 15.06.2017.
 */
public class TelnetMonitor extends Thread {
    private ServerSocket serverSocket;

    public TelnetMonitor(int port) throws IOException {
        setDaemon(true);
        setName("TelnetMonitor");
        Console.outt("Telnet monitor is openening");
        serverSocket = new ServerSocket(port, 1, InetAddress.getByName("127.0.0.1"));
    }

    @Override
    public void run() {
        int rep;
        Console.outt("Telnet monitor is listening to:" + serverSocket.getInetAddress().toString()+":"+serverSocket.getLocalPort());
        try {

            while (true) {
                Socket socket = serverSocket.accept();
                Console.outt("Connection is accepted: " + socket.getInetAddress()+":"+socket.getLocalPort());
                BufferedReader br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                PrintWriter bw = new PrintWriter(socket.getOutputStream(), true);
                sleep(1000);
                br.readLine();
                while(true) {
                    bw.println("Telnet monitor menu:");
                    bw.println("1. Exit monitor");
                    bw.println("2. Stop multithread server\n");
                    String reply  = br.readLine();
                    try {
                        rep = Integer.parseInt(reply);
                        if(rep == 1 || rep == 2)break;
                    } catch (NumberFormatException e) {bw.println("Wrong answer\n");}
                }
                if(rep == 1){bw.close(); br.close();}
                if(rep == 2){bw.close(); br.close(); socket.close();serverSocket.close();break;}
            }
        }catch(Exception e){
            Console.outt(e.getMessage());
            throw new RuntimeException(e);
        }
        Console.outt("Telnet monitor is being stopped its work");
    }
}
