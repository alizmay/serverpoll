package com.tess.multiserver;


import com.tess.dataLinks.InetPortal;
import com.tess.dataLinks.Portal;
import com.tess.host.client.ClientClass;
import com.tess.dataLinks.AcceptedPortal;
import com.tess.host.modbus.StuHost;

import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * MultiThreadServer, thanks to jdevnotes
 * Created by TESS on 15.06.2017.
 */
public class MultiModbusServer implements Runnable {
    static Logger log = Logger.getLogger(MultiModbusServer.class.getName());
    public static int serverPort = 502;
    //ServerSocket serverSocket = null;
    boolean isStopped = false;
    static long ipFilterHi = 0;
    static long ipFilterLow = 0xFFFFFFFF;
    public static InetPortal debugportal;
    //private GPRSClientSpecific client;
    private ClientClass client;
    private ServerListen serverListen = null;
    //static LinkedList<SingleWorker> workers = new LinkedList<>();
    static LinkedList<Future<String>> workers = new LinkedList<>();
    ExecutorService pool;
    public MultiModbusServer(ClientClass client){
        this.client = client;
    }


    @Override
    public void run(){
//        DaoHost daoHost;
//        try {
//            daoHost = new DaoHost(new MsSQLServer2000());
//            daoHost.connect();
//            //Registers regs = RegistersFactory.load("POLLER");
//            Registers regs = client.getRegisters();
//            daoHost.createTables("", regs.getSetByGroup("Cppd"));
//            daoHost.createTables("", regs.getSetByGroup("SingleUrgUnits"));
//        }catch(Exception e){
//            log.severe(e.getClass().getSimpleName()+':'+e.getMessage());
//            return;
//        }
        pool  = Executors.newCachedThreadPool();
        serverListen = new ServerListen(serverPort);
        serverListen.start();
        while(!isStopped()){
            Socket clientSocket;
            InetAddress visitor;
            try{
                clientSocket = serverListen.getClientSocket();
                if(clientSocket != null) {
                    visitor = clientSocket.getInetAddress();
                    Portal p = new AcceptedPortal("RTU", clientSocket);
                    StuHost sh = new StuHost(p);
                    sh.setRegisters(client.getRegisters());
                    SingleWorker w = new SingleWorker(client, sh, visitor);
                    workers.add(pool.submit(w));
                }else TimeUnit.MILLISECONDS.sleep(500);
                if(workers.size() > 0) {
                    Iterator<Future<String>> it = workers.iterator();
                    while (it.hasNext()) {
                        Future<String> sw = it.next();
                        if (!sw.isDone())continue;
                        else {
                            log.info(sw.get());
                            it.remove();
                            log.info("" + workers.size() + " workers in progress\n-------------------------------------\n");
                        }
                    }
                }
            } catch (Exception e) {
                log.log(Level.SEVERE, "MutiMmodbusServer", e);
                break;
            }
        }
        serverListen.interrupt();
        try { serverListen.join();} catch (InterruptedException ignored) {}
    }

    static long getLong(byte[] b){
        long a;
        a = (long)((b[0]<<24)) & 0xFFFFFFFF;
        a |= (long)((b[1]<<16)) & 0xFFFFFF;
        a |= (long)((b[2]<<8)) & 0xFFFF;
        a |= (long)((b[3])) & 0xFF;
        return a;
    }


    private boolean trueVisitor(InetAddress visitor) {
        byte[] b = visitor.getAddress();
        Long l;
        long a = getLong(b);
        if( (a - ipFilterLow) < 0 || (a - ipFilterHi) > 0)return false;
        return true;
    }

    static synchronized void resultWorker(SingleWorker worker){
        String res = worker.getResult();
        //#warning
        //workers.remove(worker);
        log.info(res);
        //log.info(""+ workers.size() + " total active workers");
    }

    public synchronized boolean isStopped(){
        return isStopped;
    }

    public synchronized void stop(){
        isStopped = true;
    }

    public static void setLog(Logger log) {
        MultiModbusServer.log = log;
    }

    public static void setIpFilter(String low, String high){

        try {
            InetAddress inetAddress = InetAddress.getByName(low);
            byte b[] = inetAddress.getAddress();
            ipFilterLow = getLong(b);
            inetAddress = InetAddress.getByName(high);
            b = inetAddress.getAddress();
            ipFilterHi = getLong(b);
        } catch (UnknownHostException ignored) {

        }

    }

}
