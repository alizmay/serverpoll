package com.tess.multiserver;

import com.tess.modbus.register.Register;
import com.tess.modbus.register.RegisteredValueSet;
import com.tess.modbus.types.ArchiveClock;

import java.util.List;
import java.util.Set;

/**
 * Created by Admin on 05.09.2019.
 */
public interface PollWorker {
    List<RegisteredValueSet> pull(Set<Register> set, RegisteredValueSet keys) throws InterruptedException ;

    void pushArchive(ArchiveClock from, RegisteredValueSet... sets);

    void pushCurrent(RegisteredValueSet... sets);
}
