package com.tess.multiserver;

import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Logger;

/**
 * Created by Admin on 23.08.2019.
 */
public class ServerListen extends Thread {
    static Logger log = Logger.getLogger(MethodHandles.lookup().toString());
    Socket clientSocket = null;
    ServerSocket serverSocket = null;
    int port = 0;
    AtomicBoolean push = new AtomicBoolean(false);
    private IOException serverException = null;


    ServerListen(int port) {
        super("ServerListenThread");
        this.port = port;
    }

    private void openServerSocket() throws IOException {
        log.info("Port " + MultiModbusServer.serverPort + " will be listening");
        serverSocket = new ServerSocket(MultiModbusServer.serverPort);
    }

    private void closeServerSocket(){
        if(serverSocket != null)
        try {
            serverSocket.close();
        } catch (IOException ignored) {}
    }

    @Override
    public void run() {
        try {
            openServerSocket();
            while (!isInterrupted()) {
                clientSocket = serverSocket.accept();
//                log.info("New connection: " + clientSocket.getInetAddress().toString()+":"+clientSocket.getPort());
                System.out.println("New connection: " + clientSocket.getRemoteSocketAddress());
                push.set(true);
                while (push.get() && !isInterrupted()) ;
            }
        } catch (IOException e){
            serverException = e;
        } finally {
            closeServerSocket();
            log.info("Listening is stopped");
            clientSocket = null;
            push.set(true);
        }

    }

    public AtomicBoolean getPush() {
        return push;
    }

    public Socket getClientSocket() throws Exception {
        if(!push.get())return null;
        push.set(false);
        if(serverException !=null)throw serverException;
        return clientSocket;
    }

    public IOException getServerException() {
        return serverException;
    }

}
