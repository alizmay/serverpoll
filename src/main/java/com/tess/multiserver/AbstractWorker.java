package com.tess.multiserver;

import com.tess.host.client.ClientClass;
import com.tess.host.client.RequestOwner;
import com.tess.modbus.register.Register;
import com.tess.modbus.register.RegisteredValueSet;
import com.tess.modbus.register.Registers;
import com.tess.modbus.types.ArchiveClock;

import java.io.Closeable;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Logger;

/**
 * Created by Admin on 12.07.2018.
 */
public abstract class AbstractWorker implements Callable<String>, RequestOwner, PollWorker {
    static Logger log = Logger.getLogger(MultiModbusServer.class.getName());
    protected StringBuilder logBuilder = new StringBuilder();
    protected Registers registers;
    //protected DaoHost dao;
    protected ClientClass client;
    //protected StuHost stu = null;

    public AbstractWorker(ClientClass client) {
        this.client = client;
//        this.stu = stu;
//        if(stu != null && log.getParent().getLevel() == Level.FINE)stu.setLogBuilder(logBuilder);
    }

    public void setRegisters(Registers registers) {
        this.registers = registers;
    }

    AtomicBoolean readOpComplete = new AtomicBoolean();
    List<RegisteredValueSet> readBackList = new ArrayList<>();

    @Override
    public List<RegisteredValueSet> pull(Set<Register> set, RegisteredValueSet keys) throws InterruptedException {
        if(client == null)return null;
        readOpComplete.set(false);
        client.requestMessage(this, set, keys);
        int to = 1000;
        while (readOpComplete.get() == false && (to--) > 0) {msdelay(10);}//Thread.sleep(10);
        if (readOpComplete.get() == false) throw new RuntimeException("Read operation failure");
        List<RegisteredValueSet> copy = new ArrayList<>(readBackList.size());
        copy.addAll(readBackList);
        readBackList.clear();
        return copy;
    }

    @Override
    public void pushArchive(ArchiveClock from, RegisteredValueSet... sets){//arcSet, RegisteredValueSet unit, RegisteredValueSet dh) {
        RegisteredValueSet united = new RegisteredValueSet();
        for(RegisteredValueSet rvs : sets)united.addAll(rvs);
        client.pushArchive(this, from, united);
    }

    @Override
    public void pushCurrent(RegisteredValueSet...sets){
        RegisteredValueSet united = new RegisteredValueSet();
        for(RegisteredValueSet rvs : sets)united.addAll(rvs);
        client.pushCurrent(this, united);
    }

    @Override
    public void readBack(List<RegisteredValueSet> list) {
        readBackList = list;
        readOpComplete.set(true);
    }

    @Override
    public void log(String string) {
        synchronized (logBuilder) {
            logBuilder.append(string + '\n');
        }
    }

    public void logStackTrace(Exception e) {
        StringWriter sw = new StringWriter();
        e.printStackTrace(new PrintWriter(sw));
        log(sw.toString());
    }

    @Override
    public String getTableName() {
        if(registers == null)return "";
        return registers.getSetName();
    }

    void close() throws IOException {
        //client.close();
        //stu.close();
    }

    public String getResult(){
        return logBuilder.toString();
    }

    public String getResult(boolean todelete){
        String res = logBuilder.toString();
        if(todelete)logBuilder = new StringBuilder();
        return res;
    }
    private Object lock = new Object();
    protected void msdelay(long ms) throws InterruptedException {
        synchronized (lock){
             lock.wait(ms);
        }
    }
}
