package com.tess.multiserver;

import com.tess.ServerPollManager;
import com.tess.host.client.ClientClass;
import com.tess.host.modbus.StuHost;
import com.tess.modbus.register.Register;
import com.tess.modbus.register.RegisteredValue;
import com.tess.modbus.register.RegisteredValueSet;
import com.tess.modbus.types.ArchiveClock;
import com.tess.modbus.types.Clock;

import java.io.IOException;
import java.net.InetAddress;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.logging.Level;

/**
 * Created by Admin on 27.08.2019.
 */
class SingleWorker extends AbstractWorker {
    InetAddress visitor;
    StuHost stu;

    public SingleWorker(ClientClass client, StuHost stu, InetAddress visitor) {
        super(client);
//            setRegisters(RegistersFactory.load("POLLER"));
//            registers.setSetName("");

        registers = stu.getRegisters();
        this.visitor = visitor;
        this.stu = stu;
        if(stu != null && log.getParent().getLevel() == Level.FINE)stu.setLogBuilder(logBuilder);
    }

    @Override
    public String call() {
        //log.info("Worker is being launched");
        log("Worker is being launched");
        try {
            String instr = visitor.toString();
            msdelay(2000);
            //log.info("New connection: " + instr);
            log("New connection: " + instr);
//                log.info("go to sleep");
//                try{for(int i = 1; i < 2000; i++){Thread.sleep(1);}}catch(Exception e){
//                    System.out.println(e.getMessage());
//                }
//                log.info("After sleep");
//                if(stu == null) System.out.println("null");
            RegisteredValueSet dh = stu.read();
            String serNo = dh.getRegisteredValue("DeviceHead:SerNo").getValue();
//                System.out.println("DEBUG!!!!!!");
//                if(serNo.equals("00002"))serNo = "0002d";

            RegisteredValueSet keys = new RegisteredValueSet();
            keys.add(new RegisteredValue(registers.getRegister("SingleUrgUnits:SerNo"), serNo));
            List<RegisteredValueSet> l = pull(registers.getSetByGroup("SingleUrgUnits"), keys);
            if (l == null || l.size() == 0 || l.size() > 1)
                throw new RuntimeException("No units are discovered or too much units");
            RegisteredValueSet unitList = l.get(0);
            msdelay(2000);
            //try{Thread.sleep(2000);}catch(Exception e){}
            RegisteredValueSet reply = stu.read(registers.getSetByGroup("SingleUrgUnits"));
            for (RegisteredValue rv : reply) {
                if (rv.getRegister().getAddress() >= 0)
                    log(rv.getRegister().getUnitMark() + " = " + rv.getValue());
            }
//                log("q1 = " + reply.getRegisteredValue("q1").getValue() + '\n');
//                log("T1 = " + reply.getRegisteredValue("T1").getValue() + '\n');
//               log("sysclock = " + reply.getRegisteredValue("sysclock").getValue());
            Clock sysclockClock = new Clock(reply.getRegisteredValue("sysclock").getValue());
            try {
                stu.timeCorrection(reply.getRegisteredValue("sysclock"), 5);
                sysclockClock = new Clock();
//                    log("sysclock has been corrected to " + sysclockClock.toString());
            } catch (IOException e) {
                logStackTrace(e);
            }
//TODO Scripter needs here
//                stu.write("GprsConfig:IP=185.117.144.175");
//                stu.write("GprsConfig:Port=9000");
//                stu.write("GprsConfig:Alarm=0:0:4:0");
//                stu.reset();


            //ARCHIVE  (Suggested a valid sysclock of the device)

            Set<Register> archiveSet = registers.getSetByGroup("Archive");
            ArchiveClock to = new ArchiveClock(ArchiveClock.ArchiveMode.HOUR, sysclockClock.toString());
            to.increment(false); //Excluding uncompleted current hour
            RegisteredValue lastSession = unitList.getRegisteredValue("LastSession");
            String fromString = lastSession.getValue();
            ArchiveClock from;
            if (fromString == null) {
                from = new ArchiveClock(to);
                from.addMinutes(-ServerPollManager.depthHours * 60); //Start point date is a day ago
            } else {
                from = new ArchiveClock(ArchiveClock.ArchiveMode.HOUR, fromString);
                if (to.asLong() - from.asLong() > ServerPollManager.depthHours * 60) {
                    from = new ArchiveClock(to);
                    from.addMinutes(-ServerPollManager.depthHours * 60); //Start point date is a day ago
                }
            }

            from.increment(true);//TODO uncomment
            if (from.compareTo(to) <= 0) {
                log(String.format(Locale.ENGLISH, "Download archive from %s to %s", from.toString(), to.toString()));
                while (from.compareTo(to) <= 0) {
                    RegisteredValueSet arcSet = stu.readArchive(from, archiveSet);
                    String lo = "Archive record " + from.toString() + " ";
                    for (RegisteredValue rv : arcSet) {
                        if (rv.getRegister().getAddress() > 0)
                            lo += rv.getRegister().getUnitMark() + " = " + rv.getValue() + ' ';
                    }
                    log(lo);
//                        log(from.toString() + ": q1 = " + arcSet.getRegisteredValue("q1").getValue() + " m3/h  T1=" +
//                                arcSet.getRegisteredValue("T1").getValue());
                    pushArchive(from, arcSet, unitList, dh);
                    if (arcSet.size() != 0) lastSession.setValue(from.toString());
                    from.increment(true);
                }
            }

            //RegisteredValueSet update = reply.pickOut("q1, V1, sysclock, sysstate, T1");
            RegisteredValueSet update = new RegisteredValueSet();
            for (RegisteredValue rv : reply) if (rv.getRegister().getAddress() >= 0) update.add(rv);
            update.add(lastSession);
            update.copy(unitList,"Drill");
            //client.pushCurrent(this, update, keys);
            pushCurrent(update, dh);
            msdelay(2000);
            //try { Thread.sleep(2000);} catch (InterruptedException ignored) {} //awaiting logs from queueMonitor
        } catch (Exception e) {
            logStackTrace(e);
        } finally {
            try { stu.close();} catch (IOException ignored) {}
            return getResult();
        }
    }

}
