package com.tess.modbus.register;

import com.tess.modbus.exceptions.RegistersException;

/**
 * Parser that defines what json file refers to certain version of device
 * Created by TESS on 10.08.2016.
 */
public interface ParserTypeName {
    String getReferenceType(String deviceType) throws RegistersException;
}
