package com.tess.modbus.register;

import com.tess.CfgRoot;
import com.tess.modbus.exceptions.RegistersException;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by TESS on 30.11.2017.
 */
public class AtlasParserTypeName implements ParserTypeName {
    private static final String PATH = "\\Config\\";
    @Override
    public String getReferenceType(String deviceType) throws RegistersException {
        if (deviceType == null) return "Common";
        String array[] = deviceType.split("[\\. ]");
        if(array.length == 0)return "Common";
        File f = new File(CfgRoot.PATH + PATH);
        File[] list = f.listFiles();
        if(list == null)return "Common";
        Map<Integer, File> versions = new TreeMap<>();
        if("URG2KM".equals(array[0])) {
            for (File ff:list)
                if (ff.isFile() && ff.getName().contains(".json") && ff.getName().contains("URG")){
                    Integer integer = Integer.parseInt(ff.getName().substring(ff.getName().length()-9,
                            ff.getName().length()-5));
                    versions.put(integer, ff);
                }
        }else if("STU-1".equals(array[0])) {
            for (File ff : list)
                if (ff.isFile() && ff.getName().contains(".json") && ff.getName().contains("STU")) {
                    Integer integer = Integer.parseInt(ff.getName().substring(ff.getName().length() - 9,
                            ff.getName().length() - 5));
                    versions.put(integer, ff);
                }
        }
        Integer version = Integer.parseInt(array[1]+array[2]);
        File file = null;
        for(Map.Entry<Integer, File>pair: versions.entrySet())
            if(version.compareTo(pair.getKey()) >= 0)file = pair.getValue();
        if(file == null)return "Common";
        return file.getName().substring(0, file.getName().length()-5);
    }
}
