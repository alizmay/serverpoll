package com.tess.modbus.register;

import java.util.*;

/**
 * Created by TESS on 30.05.2016.
 * Set of registered values
 */
public class RegisteredValueSet extends TreeSet<RegisteredValue> {

    public static Map<String, Set<Register>> getSetGrouped(Set<Register> table) {
        Map<String, Set<Register>> m = new HashMap<>();
        for(Register r : table){
            if(!m.containsKey(r.getGroupMark()))
                m.put(r.getGroupMark(), new TreeSet<Register>());
            m.get(r.getGroupMark()).add(r);
        }
        return m;
    }
//    public static Map<String, String> getMap(RegisteredValueSet rvs){
//        Map<String, String> m = new HashMap<>();
//        for(RegisteredValue rv:rvs)m.put(rv.getRegister().getMark(),rv.getValue());
//        return m;
//    }

    public RegisteredValueSet() {
        super();
    }

    public RegisteredValueSet(Set<Register> set){
        super();
        for(Register r:set)
            add(new RegisteredValue(r, null));
    }

    public RegisteredValueSet(RegisteredValueSet src ){
        addAll(src);
//        for(RegisteredValue rv:src){
//            add(new RegisteredValue(rv.getRegister(), rv.getValue()));
//        }
    }

    public TreeSet<Register> getSet(){
        TreeSet<Register> set = new TreeSet<>();
        for(RegisteredValue rv: this)
            set.add(rv.getRegister());
        return set;
    }

    public RegisteredValue getRegisteredValue(String mark) {
        RegisteredValue res;
        mark = mark.trim();
        boolean woGroup = !(mark.contains(":"));
        if (mark != null)
            for (Iterator<RegisteredValue> it = iterator(); it.hasNext(); ) {
                res = it.next();
                if(!woGroup) {
                    if (res.getRegister().getMark().equals(mark)) return res;
                }else if(res.getRegister().getUnitMark().equals(mark))return res;
            }
        return null;
    }

    public RegisteredValue getRegisteredValue(int index){
        RegisteredValue res = null;
        index++;
        if(index > size()|| index < 0)throw new ArrayIndexOutOfBoundsException("index >= size");
        for (Iterator<RegisteredValue> it = iterator(); index != 0; index-- )res = it.next();
        return res;
    }

    public boolean add(RegisteredValue r) {
        return r.getRegister() != null && super.add(r);
    }

    public boolean add(RegisteredValueSet set){
        boolean result = true;
        for(RegisteredValue registeredValue:set)
            result &= add(registeredValue);
        return result;
    }

    @Override
    public Iterator<RegisteredValue> iterator() {
        return super.iterator();
    }

    @Override
    public Iterator<RegisteredValue> descendingIterator() {
        return super.descendingIterator();
    }

    public void update(RegisteredValueSet rvs) {
        for (RegisteredValue rvDomestic : this) {
            for (RegisteredValue rvForeign : rvs) {
                if (rvDomestic.getRegister().getUnitMark().equals(rvForeign.getRegister().getUnitMark()))
                    rvDomestic.setValue(rvForeign.getValue());
            }
        }
    }

//    public RegisteredValueSet getDeviceIDKeys(){
//        RegisteredValue rv = getRegisteredValue("DeviceID");
//        if(rv == null)return null;
//        RegisteredValueSet rvs = new RegisteredValueSet();
//        rvs.add(rv);
//        return rvs;
//    }

    public RegisteredValueSet getDeviceIDKeys(Registers registers, int id){
        RegisteredValueSet rvs = new RegisteredValueSet();
        RegisteredValue rv = getRegisteredValue("DeviceID");
        if(rv == null){
            Register kk = registers.getRegister(getRegisteredValue(0).getRegister().getGroupMark()+":DeviceID");
            rvs.add(new RegisteredValue(kk, String.valueOf(id)));
        }else{
            rv.setValue(String.valueOf(id));
            rvs.add(rv);
        }
        return rvs;
    }

    public void delete(String mark) {
        RegisteredValue r = getRegisteredValue(mark);
        if(r != null)remove(r);
    }

    public Map<String, RegisteredValueSet> getGrouped(){
        Map<String, RegisteredValueSet> m = new HashMap<>();
        for(RegisteredValue r : this){
            if(!m.containsKey(r.getRegister().getGroupMark()))
                m.put(r.getRegister().getGroupMark(), new RegisteredValueSet());
            m.get(r.getRegister().getGroupMark()).add(r);
        }
        return m;
    }

    private RegisteredValueSet pickOut(String mark, boolean delete){
        String array[] = mark.split(",");
        RegisteredValueSet set = new RegisteredValueSet();
        for(String s : array){
            RegisteredValue rv = getRegisteredValue(s.trim());
            if(rv != null){
                set.add(rv);
                if(delete)remove(rv);
            }
        }
        return set;
    }



    public RegisteredValueSet pickOut(String mark){
        return pickOut(mark, false);
    }

    public RegisteredValueSet pickOutRemove(String mark){
        return pickOut(mark, true);
    }

    public void remove(Set<Register>set){
        for (Register register : set) {
            RegisteredValue rv = getRegisteredValue(register.getMark());
            if(rv != null)super.remove(rv);
        }
    }

    private void copyDiffRegs(RegisteredValueSet source, String marks){
        String[] exp = marks.split(",");
        for (String s : exp) {
            String[] pair = s.split("=");
            RegisteredValue src = source.getRegisteredValue(pair[1]);
            RegisteredValue r = getRegisteredValue(pair[0]);
            if(src == null || r == null)continue;
            r.setValue(src.getValue());
        }
    }

    public void copy(Set<Register>set, String marks){

    }

//    public void copy(RegisteredValueSet source, String marks){
//        if(marks.contains("=")){
//            copyDiffRegs(source, marks);
//        }else {
//            String[] stray = marks.split(",");
//            for (String s : stray) {
//                RegisteredValue src = source.getRegisteredValue(s.trim());
//                RegisteredValue dst = getRegisteredValue(s.trim());
//                if (dst == null) add(src);
//                else dst.setValue(src.getValue());
//            }
//        }
//    }

    public void copy(RegisteredValueSet source, String marks){
        String[] stray = marks.split(",");
        for (String s : stray) {
            if(s.contains("=")){
                String[] pair = s.split("=");
                RegisteredValue src = source.getRegisteredValue(pair[1]);
                RegisteredValue r = getRegisteredValue(pair[0]);
                if(src == null || r == null)continue;
                r.setValue(src.getValue());
            }else {
                RegisteredValue src = source.getRegisteredValue(s.trim());
                RegisteredValue dst = getRegisteredValue(s.trim());
                if (dst == null) add(src);
                else dst.setValue(src.getValue());
            }
        }

    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for(RegisteredValue r: this){
            sb.append(r.getRegister().getMark()+" = " + r.getValue() + '\n');
        }
        return sb.toString();
    }

    @Override
    public boolean addAll(Collection<? extends RegisteredValue> c) {
        if(c == null)return false;

        for(RegisteredValue crv : c){
            boolean toBeContinued = false;
            for(RegisteredValue rv: this){
                if(crv.compareTo(rv) == 0){
                    rv.setValue(crv.getValue());
                    toBeContinued = true;
                    break;
                }
            }
            if(toBeContinued)continue;
            this.add(crv);
        }
        return true;
    }

    //    public RegisteredValueSet getDeviceIDKeys(String group){
//        RegisteredValue rv = getRegisteredValue(group + ".DeviceID");
//        if(rv == null)return null;
//        RegisteredValueSet rvs = new RegisteredValueSet();
//        rvs.add(rv);
//        return rvs;
//    }

}
