package com.tess.modbus.register;


import com.tess.CfgRoot;
import com.tess.modbus.DeviceHead;
import com.tess.modbus.exceptions.RegistersException;

import java.io.*;
import java.lang.invoke.MethodHandles;
import java.nio.file.NoSuchFileException;
import java.util.*;
import java.util.logging.Logger;

/**
 * Created by TESS on 21.07.2016.
 * Collection for Registers
 */
public class RegistersFactory {
    static Logger log = Logger.getLogger(MethodHandles.lookup().toString());

    private static Registers common;
    private static Map<String, Registers>store = new HashMap<>();
    private static ParserTypeName parserTypeName;
    static{
        try {
            File p = new File(CfgRoot.PATH + "\\lib\\types");
            if(p.exists() == false)throw new NoSuchFileException("There is no LIB/TYPES directory!");
            common = new Registers("Common");
            store = new HashMap<>();
            store.put("Common", common);
            parserTypeName = new ParserTypeName() {
                @Override
                public String getReferenceType(String deviceType) throws RegistersException {
                    if (deviceType == null) return "Common";
                    String array[] = deviceType.split(" ");
                    if (array.length != 2) throw new RegistersException("No type found");
                    String device = array[0];
                    String version = array[1];
                    String[] str = version.split("\\.");
                    int vers;
                    try {
                        vers = Integer.parseInt(str[0]) * 1000 + Integer.parseInt(str[1]);
                    } catch (Exception e) {
                        throw new RegistersException("There is no device with such version: " + deviceType);
                    }

                    if ((2000 < vers && vers < 3000) && (device.equals("STU-1"))) {
                        return "STU2000";
                    } else if (4000 < vers && vers < 5000) {
                        if (version.contains(".i32") || version.contains(".i33")) return "STU4000";
                        if (version.contains(".i34") || version.contains(".i35")) return "URG4000";
                    } else if (5000 < vers && vers < 6000) {
                        return "URG5000";
                    }
                    throw new RegistersException("There is no device with such version: " + deviceType);
                }
            };
        }catch (Exception e){
            log.severe(e.getClass().getSimpleName() + " : " + e.getMessage());
        }
    }

    private RegistersFactory(){}

    public static void setParserTypeName(ParserTypeName parserTypeName) {
        RegistersFactory.parserTypeName = parserTypeName;
    }

    public static Registers getInstance(String device) throws RegistersException {
        String rs;
        rs = parserTypeName.getReferenceType(device);
        return load(rs);
    }

    public static Registers getInstance(){
        try {
            return getInstance(null);
        } catch (RegistersException e) {
            return null;
        }
    }

    public static Registers getCommon(){
        return common;
    }

    public static Registers load(String rs){
        Registers registers = store.get(rs);
        if(registers == null) {
            registers = new Registers(rs);
            store.put(rs, registers);
        }
        log.info("Registers \"" +  rs + "\" have been loaded");
        return registers;
    }






//    public static class VersionUniFactory implements VersionFactory<VersionUniSystem>{
//
//        @Override
//        public VersionUniClass getInstance(Properties prop) throws Exception {
//            return new VersionUniClass(prop);
//        }
//    }



}
