package com.tess.modbus.register.versionSystem;

import com.tess.modbus.DeviceHead;

/**
 * Created by Admin on 04.05.2019.
 */
public interface VersionSystem {
    int getCoincides(DeviceHead dh);
    String getRegistersName();
    String[] getServices();
    String getXLTName();
    String getCutName();
}
