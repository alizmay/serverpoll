package com.tess.modbus.register.versionSystem;

import com.tess.modbus.register.versionSystem.AbstractVersionSystem;
import com.tess.modbus.register.versionSystem.VersionSystem;

import java.util.Properties;

/**
 * Created by Admin on 04.05.2019.
 */
public class VersionSystemClass extends AbstractVersionSystem implements VersionSystem {

    String registersName = null;
    String[] services = null;
    String xltName = null;
    String cutName = null;
    private int version = 0;

    public VersionSystemClass(Properties prop){
        super(prop);
        registersName = prop.getProperty("registers");
        services = getVariants(prop.getProperty("services"));
        xltName = prop.getProperty("xlt");
        cutName = prop.getProperty("cut");
    }

    @Override
    public String getRegistersName() {
        return registersName;
    }

    @Override
    public String[] getServices() {
        return services;
    }

    @Override
    public String getXLTName() {
        return xltName;
    }

    @Override
    public String getCutName() {
        return cutName;
    }
}
