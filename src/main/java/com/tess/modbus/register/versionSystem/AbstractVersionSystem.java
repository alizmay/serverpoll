package com.tess.modbus.register.versionSystem;

import com.tess.modbus.DeviceHead;

import java.util.Properties;

/**
 * Created by Admin on 04.05.2019.
 */
public abstract class AbstractVersionSystem implements VersionSystem {
    private int startVersion = 0, endVersion = 0;
    private String[]hardwareVersion;
    private String[] options;
    private int coincides = 0;

    protected static String[] getVariants(String prop){
        if(prop == null)return null;
        String[] arry = prop.trim().split(",");
        String[] result = new String[arry.length];
        for(int i = 0; i < arry.length; i++)
            result[i] = arry[i].trim();
        return result;
    }

    public AbstractVersionSystem(Properties prop){
        try {
            startVersion = Integer.parseInt(prop.getProperty("startVersion"));
        }catch(Exception ignore){}
        try{
            endVersion = Integer.parseInt(prop.getProperty("endVersion"));
        }catch(Exception ignore){}

        hardwareVersion = getVariants(prop.getProperty("hardwareVersion"));
        options = getVariants(prop.getProperty("options"));
    }

    @Override
    public int getCoincides(DeviceHead dh) {
        int boundVersion;
        if(dh.getDevice() == null)throw new RuntimeException("Wrong answer");
        if(endVersion != 0 )
            boundVersion = endVersion;
        else {
            boundVersion = startVersion / 1000;
            boundVersion = (boundVersion + 1) * 1000 - 1;
        }

        if(startVersion != 0 && dh.getVersion() >= startVersion && dh.getVersion() <= boundVersion) {
            coincides++;
            if (hardwareVersion != null) {
                for (String s : hardwareVersion) {
                    if (dh.getHardwareVersion().contains(s.trim())) {
                        coincides++;
                        if (options != null) {
                            coincides--;
                            int optionCoincides = 0;
                            for (String ss : options) {
                                if (dh.containOptions(ss.trim())) {
                                    optionCoincides++;
                                }
                            }
                            if(optionCoincides == options.length)
                                coincides += 2;
                        }//else coincides++;
                    }
                }
            }
        }
        return coincides;
    }
}
