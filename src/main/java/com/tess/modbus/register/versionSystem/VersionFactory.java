package com.tess.modbus.register.versionSystem;

import java.util.Properties;

/**
 * Created by Admin on 04.05.2019.
 */
public interface VersionFactory<T extends VersionSystem> {
    T getInstance(Properties prop);
}
