package com.tess.modbus.register;

import com.sun.media.sound.InvalidFormatException;

/**
 * Created by TESS on 18.05.2016.
 * Definition class for access to parameters
 */

public class Register implements Comparable<Register>, Cloneable {
    private String mark;
    private int address = 0;
    private TypeMapping typeMapping;
    private String groupMark;
    private String unitMark;

    public Register(String mark, int addr, TypeMapping typeMapping) throws RuntimeException {
        if(mark==null || mark.isEmpty())throw new RuntimeException(mark + ":" + mark);
        this.mark = mark;
        this.address = addr;
        this.typeMapping = typeMapping;
        if(mark.contains(":"))
        {
            String[]ss = mark.split(":");
            if(ss.length !=2 )throw new RuntimeException(mark + ":" + mark);
            groupMark = ss[0];
            unitMark = ss[1];
        }else unitMark = groupMark = mark;
    }

    public String getMark() {
        return mark;
    }

    public int getAddress() {
        return address;
    }

    public TypeMapping getTypeMapping() {
        return typeMapping;
    }

    @Override
    public int compareTo(Register o) {
        if(o.equals(this))return 0;
        int res =  - o.getAddress() + getAddress();
        if(res == 0)res = getMark().compareTo(o.getMark());
        return res;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Register register = (Register) o;

        return mark.equals(register.mark);

    }

    @Override
    public int hashCode() {
        return mark.hashCode();
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        super.clone();
        Register newR = null;
        try {
            newR = new Register(mark, address, typeMapping);
        } catch (Exception ignored) {}
        return newR;
    }

/*    public RegisteredValue getRegisteredValueInstance(){
        RegisteredValue registeredValue = new RegisteredValue(register.this);
        return registeredValue;
    }*/

    public String getGroupMark() {
        return groupMark;
    }

    public String getUnitMark() {
        return unitMark;
    }

    @Override
    public String toString() {
        return String.format("%s, %d (0x%X), %s", getMark(), getAddress(), getAddress(), getTypeMapping());
    }
}

