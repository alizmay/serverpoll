package com.tess.modbus.register;

import com.tess.CfgRoot;
import com.tess.modbus.exceptions.RegistersException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import sun.reflect.generics.tree.Tree;

import java.io.File;
import java.lang.invoke.MethodHandles;
import java.util.*;
import java.util.logging.Logger;

/**
 * Container for registers
 * Created by TESS on 09.08.2016.
 */
public class Registers {
    static Logger log = Logger.getLogger(MethodHandles.lookup().toString());
    String setName;
    Set<Register> registersSet = new HashSet<>();

    public Registers(String fileName) throws RegistersException {
        load(fileName);
        setName = fileName;
    }

    public Registers() throws RegistersException {
        load("Common");
        setName = "Common";
    }


    public Register getRegister(String mark){
        mark = mark.trim();
        if(registersSet == null)return null;
        if(!mark.contains(":")){
            for(Register r:registersSet)
                if(r.getUnitMark().equals(mark)){
                    return r;
                }
        }else {
            for (Register r : registersSet)
                if (r.getMark().equals(mark)) {
                    return r;
                }
        }
        return null;
    }

    public TreeSet<Register> get(String ...marks){
        if(registersSet == null)return null;
        if(marks.length == 1)
            marks = marks[0].split(",");
        TreeSet<Register>set = new TreeSet<>();
        for(String s:marks) {
            if(s == null)continue;
            Register r = getRegister(s);
            if(r != null)set.add(r);
        }
        return set;
    }


    public RegisteredValueSet getRvs(String expression){
        if(registersSet == null)throw new NullPointerException("registers is empty");
        String[] array = expression.split("[=,]");
        if((array.length & 0x01) == 1) throw new NumberFormatException(expression);

        RegisteredValueSet rvs = new RegisteredValueSet();
        for (int i = 0; i < array.length; i += 2) {
            Register r = getRegister(array[i].trim());
            if(r == null)continue;
            rvs.add(new RegisteredValue(r, array[i + 1].trim()));
        }
       return rvs;
    }


    public Register getByUnitMark(String mark){
        throw new UnsupportedOperationException("Should use getRegister");
//        if(registersSet == null)return null;
//        for(Register r:registersSet)
//            if(r.getUnitMark().equals(mark))return r;
//        return null;
    }

    public Set<Register> getSetByUnitMark(String mark){
        throw new UnsupportedOperationException("Should use getRegister");
//        Set<Register>set = new HashSet<>();
//        if(mark.contains(":")){
//            Register r = getRegister(mark);
//            if(r != null)set.add(r);
//        }
//        for(Register r:registersSet)
//            if(r.getUnitMark().equals(mark))set.add(r);
//        return set;
    }

    public TreeSet<Register> getSetByGroup(String mark){
        if(registersSet == null)return null;
        TreeSet<Register>set = new TreeSet<>();
        for(Register r:registersSet){
            if(r.getGroupMark().equals(mark.trim()))set.add(r);
        }
        return set;
    }

    public TreeSet<Register> getFullSetByGroup(String mark){
        TreeSet<Register>s = getSetByGroup(mark);
        TreeSet<Register>upd = new TreeSet<>();
        for(Register r: s){
            TreeSet<Register>local = getSetByGroup(r.getUnitMark());
            upd.addAll(local);
        }
        s.addAll(upd);
        return s;
    }

    public RegisteredValueSet getRvsByGroup(String mark){
        assert(registersSet != null);
        RegisteredValueSet rvs = new RegisteredValueSet();
        for(Register r:registersSet){
            if(r.getGroupMark().equals(mark.trim()))rvs.add(new RegisteredValue(r, null));
        }
        return rvs;
    }

    public String getSetName() {
        return setName;
    }

    public void setSetName(String setName) {
        this.setName = setName;
    }

    public final static String REGISTERS_PATH = "\\Config\\DevsInfo\\Registers\\";

    public void load(String fileName) throws RegistersException {
        //File file =  new File(System.getProperty("user.dir") + "\\Config\\" + fileName + ".json");
        File file =  new File(CfgRoot.PATH + REGISTERS_PATH + fileName + ".json");
        int indx = 0;
        try(Scanner in = new Scanner(file)){
            StringBuilder sb = new StringBuilder();
            while(in.hasNext())
                sb.append(in.nextLine());
            in.close();
            JSONParser jsonParser = new JSONParser();
            JSONObject oo = (JSONObject)jsonParser.parse(sb.toString());
            JSONArray ja = (JSONArray) oo.get(fileName);
            for(indx = 0; indx < ja.size();indx++){
                oo = (JSONObject)ja.get(indx);
                Long adr = (Long)oo.get("addr");
                if(adr == null)adr = -1L;
                Register ror = new Register((String)oo.get("mark"), (int)(adr.longValue()),
                        new TypeMapping((String)oo.get("type")));
                        //TypeMapping.valueOf((String)oo.getRegister("type")));
                registersSet.add(ror);
            }
        }catch (Exception e){
            log.severe(e.getMessage());
            if(e.getCause()!=null)log.severe(e.getCause().getMessage());
            log.severe("Line: " + indx);
            throw new RegistersException(e.getMessage());
        }
    }

    public Set<String>getGroupNames(){
        Set<String>set = new HashSet<>();
        for(Register r:registersSet){
            if(set.contains(r.getGroupMark()))continue;
            set.add(r.getGroupMark());
        }
        return set;
    }

    public TreeSet<Register> getSetByAllIndex(String ... marks){
        TreeSet<Register>res = new TreeSet<>();
        for(int i = 1; i < 100; i++){
            for(String s : marks){
                Register r = getRegister(s + i);
                if(r == null)return res;
                res.add(r);
            }
        }
        throw new RuntimeException("Is it possible?");
    }

    public TreeSet<Register> getAll(){
        TreeSet<Register>set = new TreeSet<>();
        for(Register r: registersSet)set.add(r);
        return set;
    }

}
