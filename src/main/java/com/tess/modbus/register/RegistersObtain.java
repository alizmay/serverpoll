package com.tess.modbus.register;

/**
 * Created by TESS on 03.08.2016.
 * Objects with Registers
 */
public interface RegistersObtain {
    Registers getRegisters();
}
