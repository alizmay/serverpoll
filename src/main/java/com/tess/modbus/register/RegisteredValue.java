package com.tess.modbus.register;

/**
 * Created by TESS on 30.05.2016.
 * Register with value
 */
public class RegisteredValue implements Comparable<RegisteredValue> {
    protected Register origin;
    protected String value;

    public RegisteredValue(Register register) {
        origin = register;
    }

    public RegisteredValue(Register register, String value) {
        origin = register;
        this.value = value;
    }

    @Override
    public String toString() {
        return String.format("%s, %d (0x%X), %s: %s",
                origin.getMark(), origin.getAddress(), origin.getAddress(), origin.getTypeMapping(), value);
        //return origin.getMark() + "," + origin.getAddress() + "," + origin.getTypeMapping() + ":" + value;
    }

    public String getValue() {
        return value;
    }

    public Register getRegister() {
        return origin;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public boolean isEmpty(){
        if(value == null || value.isEmpty() || value.toLowerCase().equals("null"))return true;
        return false;
    }

    @Override
    public int compareTo(RegisteredValue o) {
        if (o.getRegister() == null || getRegister() == null) return 0;
        int res = o.getRegister().compareTo(getRegister());
        return -res;
    }
}
