package com.tess.modbus.register;

import com.tess.modbus.register.versionSystem.VersionSystem;

/**
 * Created by Admin on 04.05.2019.
 */
public interface VersionUniSystem extends VersionSystem {
    String getXLTName();
    String getCutName();
}



