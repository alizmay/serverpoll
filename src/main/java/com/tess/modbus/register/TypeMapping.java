package com.tess.modbus.register;

import com.tess.modbus.exceptions.RegistersException;
import com.tess.modbus.register.loader.Converter;
import com.tess.modbus.register.loader.ConverterClassBox;
import com.tess.modbus.types.Clock;

import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Locale;


/**
 * Created by TESS on 18.05.2016.
 * Interconnections for types between devices, databases
 */

public class TypeMapping {
    private Converter converter = null;
    private TypeMappingEnum typeCache = null;
    private final static ConverterClassBox CCB = new ConverterClassBox(ClassLoader.getSystemClassLoader());

    public TypeMapping(String type){
        try{
            typeCache = TypeMappingEnum.valueOf(type);
        }catch(Exception e){
            if(CCB.match(type)==null)throw new RegistersException("Unknown type of the register: " + type);
            converter = CCB.match(type);
        }

    }

    public int transformation(ByteBuffer byteBuffer, int address, String value) throws RegistersException{
        if(typeCache != null){
            return typeCache.transformation(byteBuffer, address, value);
        }else {
            converter.UniToBin(byteBuffer, address * 2, value);
            return address + converter.size();
        }
    }

    public String transformation(ByteBuffer byteBuffer, int address)throws RegistersException{
        if(typeCache != null)
            return typeCache.transformation(byteBuffer, address);
            else return converter.BinToUni(byteBuffer, address);
    }

    public TypeMappingEnum getCache() {
        return typeCache;
    }

    public Converter getConverter() {
        return converter;
    }

    public static enum TypeMappingEnum {
        FLOAT(2),
        INT(1),
        LONG(2),
        TOTAL(4),
        CLOCK(3),
        STR20(10),
        STRING(0),
        CHECK(2),
//        TEMP151(1),
        PRESS160(1),
        TCOLD(1),
//        TATMO(1),
        //    IPADR(2),
        ALARM(2),
        UNIQUEIDENTIFIER(16),
        INT_PRIMARYKEY(1),
//        BYTE186(93),
        UNKNOWN(0);

        TypeMappingEnum(int size) {
            this.size = size;
        }

        private int size;


        public int getSize() {
            return size;
        }

        private static void swap(ByteBuffer bb, int address, int size) {
            bb.position(address);
            while (size-- != 0) {
                byte tmp = bb.get(address);
                bb.put(bb.get(address + 1));
                bb.put(tmp);
                address += 2;
            }
        }

        private static long checkAsLong(String ch){
            long result = 0;
            String[] strs = ch.split(" ");
            for (int i = 0; i < strs.length; i++) {
                for (int j = 0; j < CHECK_CODES.length; j++) {
                    if(CHECK_CODES[j].equals(strs[i])){result |= (1<<j); break;}
                }
            }
            return result;
        }

        //There is address pointing on words
        public int transformation(ByteBuffer byteBuffer, int address, String value) throws RegistersException {
            switch (this) {
                case STR20:
                    if (value.length() > 19) throw new RegistersException("Length is more than 20");
                    byteBuffer.position(address * 2);
                    byte bytes[];
                    if ("\\0".equals(value)) bytes = new byte[0];
                    else bytes = value.getBytes();
                    byte area[] = new byte[20];
                    Arrays.fill(area, (byte) 0);
                    System.arraycopy(bytes, 0, area, 0, bytes.length);
                    for (int i = 0; i < area.length; i += 2) {
                        byteBuffer.put(area[i + 1]);
                        byteBuffer.put(area[i]);
                    }
                    break;
                case INT:
                    byteBuffer.putShort(address * 2, (short)Float.parseFloat(value));
                    swap(byteBuffer, address * 2, size);
                    break;
                case FLOAT:
                    byteBuffer.putFloat(address * 2, Float.parseFloat(value));
                    swap(byteBuffer, address * 2, size);
                    break;
                case LONG:
                    byteBuffer.putInt(address * 2, Integer.parseInt(value));
                    swap(byteBuffer, address * 2, size);
                    break;
                case CLOCK:
                    Clock.SRTClock rt = new Clock(value).getRtClock();
                    byteBuffer.position(address * 2);
                    byteBuffer.put((byte) rt.min);
                    byteBuffer.put((byte) rt.sec);
                    byteBuffer.put((byte) rt.day);
                    byteBuffer.put((byte) rt.hour);
                    byteBuffer.put((byte) rt.year);
                    byteBuffer.put((byte) rt.mon);
                    break;
                case PRESS160:
                    float f = Float.parseFloat(value);
                    if(f < 0 || f > 1.6)throw new RegistersException("The value of pressure is out of bounds PRESS160: " + value);
                    short s = (short)(f / 1.60f*65535.0f);
                    byteBuffer.putShort(address * 2, s);
                    swap(byteBuffer, address * 2, size);
                    //float p = byteBuffer.getShort(address);
                    //p = p / 65535.0f * 1.60f;
                    //value = String.format(Locale.ENGLISH, "%.2f", p);
                    break;
                case CHECK:
                    int ch = (int)checkAsLong(value);
                    byteBuffer.putInt(address * 2, ch);
                    swap(byteBuffer, address * 2, size);
                    break;
                case TCOLD:
                    float ff = Float.parseFloat(value);
                    if(ff < 0 || ff> 25.5)throw new RegistersException("The value of cold temp is out of bounds TCOLD (>25.5)");
                    short tcold = (short)(ff * 10);
                    byteBuffer.putShort(address * 2, tcold);
                    swap(byteBuffer, address * 2, size);
                    break;
//            case IPADR: String str[] = value.split("\\.");
//                if(str.length != 4)throw new RegistersException("Wrong IPADR");
//                byteBuffer.position(address * 2);
//                try {
//                    byteBuffer.put((byte)Short.parseShort(str[1]));
//                    byteBuffer.put((byte)Short.parseShort(str[0]));
//                    byteBuffer.put((byte)Short.parseShort(str[3]));
//                    byteBuffer.put((byte)Short.parseShort(str[2]));
//                } catch (NumberFormatException e) {throw new RegistersException("Wrong IPADR");}
//                break;
                case ALARM:
                    String st[] = value.split(":");
                    if (st.length != 4) throw new RegistersException("Wrong ALARM");
                    byteBuffer.position(address * 2);
                    try {
                        byteBuffer.put((byte) Short.parseShort(st[1]));
                        byteBuffer.put((byte) Short.parseShort(st[0]));
                        byteBuffer.put((byte) Short.parseShort(st[3]));
                        byteBuffer.put((byte) Short.parseShort(st[2]));
                    } catch (NumberFormatException e) {
                        throw new RegistersException("Wrong ALARM");
                    }
                    break;
                default:
//                    Converter converter = ccb.match(this.toString());
//                    if (converter == null) throw new RegistersException("Unknown type of the register");
//                    converter.UniToBin(byteBuffer, address * 2, value);
//                    return address + converter.size();
                throw new RegistersException("Unknown type map enum : " + this.name());
            }
            return address + size;
        }

        //There is address pointing on bytes
        @SuppressWarnings("MagicConstant")
        public String transformation(ByteBuffer byteBuffer, int address) {
            String value;
            switch (this) {
                case STR20:
                    byte bytes[] = new byte[20];
                    for (int i = 0; i < bytes.length; i += 2) {
                        short sh = byteBuffer.getShort(address + i);
                        bytes[i + 1] = (byte) (sh >> 8);
                        bytes[i] = (byte) (sh & 0x0FF);
                    }
                    int len = 0;
                    while (bytes[len] != 0 ){
                        if(len++ >= 19){len = 0; break;}
                    }
                    if (len == 0) value = "\\0";
                    else value = new String(bytes, 0, len);
                    break;
                case FLOAT:
                    float flo = byteBuffer.getFloat(address);
                    if (Float.isNaN(flo)) value = "NaN";
                    else {
                        if(flo != 0 && Math.abs(flo) < 0.001)value = String.format(Locale.ENGLISH, "%e", flo);
                        else value = String.format(Locale.ENGLISH, "%5.3f", flo);
                    }
                    break;
                //value = String.format(Float.toString(byteBuffer.getFloat(address));break;
                //Float.toString(byteBuffer.getFloat(address));break;
                case INT:
                    value = Integer.toString(byteBuffer.getShort(address) & 0xFFFF);
                    break;
                case LONG:
                    value = Long.toString(byteBuffer.getInt(address));
                    break;
                case TOTAL:
                    value = Long.toString(byteBuffer.getInt(address));
                    StringBuilder ss = new StringBuilder(Float.toString(byteBuffer.getFloat(address + 4)));
                    value = value.concat(ss.deleteCharAt(0).toString());
                    break;
                case CLOCK:
                    try {
                        Calendar cl = Calendar.getInstance();
                        int year = byteBuffer.get(address+5)+2000;
                        if(year < 2010)throw new Exception();
                        int mon = byteBuffer.get(address + 4) - 1;
                        if(mon < 0 || mon > 11)throw new Exception();
                        int day = byteBuffer.get(address + 3);
                        if(day < 1 || day > 31)throw new Exception();
                        int hour = byteBuffer.get(address + 2);
                        if(hour < 0 || hour > 24)throw new Exception();
                        int min = byteBuffer.get(address + 1);
                        if(min < 0 || min > 59)throw new Exception();
                        int sec = byteBuffer.get(address);
                        if(sec < 0 || sec > 59)throw new Exception();
                        cl.set(year, mon, day, hour, min, sec);
                        //cl.set(byteBuffer.get(address + 5) + 2000, byteBuffer.get(address + 4) - 1, byteBuffer.get(address + 3),
                        //        byteBuffer.get(address + 2), byteBuffer.get(address + 1), byteBuffer.get(address));
                        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
                        value = sdf.format(cl.getTime());
                    } catch (Exception e) {
                        value = null;
                    }
                    break;
                case CHECK:
                    long check = byteBuffer.getInt(address);
                    value = asCheck(check);
                    break;
//                case TEMP151:
//                    float d = byteBuffer.getShort(address) &0xFFFF;
//                    d = d / 65535.0f * 151.0f;
//                    value = Float.toString(d);
//                    break;
                case PRESS160:
                    float p = ((int)byteBuffer.getShort(address)) &0xFFFF;
                    p = p / 65535.0f * 1.60f;
                    value = String.format(Locale.ENGLISH, "%.2f", p);
                    break;
                case TCOLD:
                    int tcold = byteBuffer.getShort(address) & 0xFF;
                    value = Double.toString(((double) tcold) * 10.0);
                    break;
//                case TATMO:
//                    int tatmo = byteBuffer.get(address + 1 & 0xFF);
//                    value = Double.toString(((double) tatmo) * 10.0);
//                    break;
//            case IPADR:byteBuffer.position(address);
//                short b[] = new short[4];
//                b[0] = (short)Byte.toUnsignedInt(byteBuffer.getRegister());
//                b[1] = (short)Byte.toUnsignedInt(byteBuffer.getRegister());
//                b[2] = (short)Byte.toUnsignedInt(byteBuffer.getRegister());
//                b[3] = (short)Byte.toUnsignedInt(byteBuffer.getRegister());
//                value = String.format("%d.%d.%d.%d", b[0], b[1], b[2], b[3]);
//                break;
                case ALARM:
                    byteBuffer.position(address);
                    short bb[] = new short[4];
                    bb[0] = (short) (byteBuffer.get() & 0xFF);
                    bb[1] = (short) (byteBuffer.get() & 0xFF);
                    bb[2] = (short) (byteBuffer.get() & 0xFF);
                    bb[3] = (short) (byteBuffer.get() & 0xFF);
                    value = String.format("%d:%d:%d:%d", bb[0], bb[1], bb[2], bb[3]);
                    break;
//                case BYTE186:
//                    byteBuffer.position(address);
//                    StringBuilder sb = new StringBuilder();
//                    for(int i = 0; i < 186; i++){
//                        sb.append(String.valueOf((int)byteBuffer.getRegister() & 0xFF) + " ");
//                    }
//                    value = sb.toString();
//                    break;

                default: throw new RegistersException("Unknown type map enum : " + this.name());
            }
            return value;
        }
    }

    private final static String[] CHECK_CODES = {
            "P1B", "P1H", "P2B", "P2H", "M12", "P1", "P2", "P5",
            "P3B", "P3H", "P4B", "P4H", "M34", "P3", "P4", "P6",
            "P5B", "P5H", "P6B", "P6H", "T1", "T2", "T3", "T4",
            "T1H", "T2B", "T3H", "T4B", "D1", "D2", "SYS", "OFF"};

    private static String asCheck(long val) {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < 32; i++) {
            if ((val & 0x01) == 1) {
                result.append(CHECK_CODES[i]);
                result.append(" ");
            }
            val >>= 1;
        }
        if (result.length() == 0) result.append(" ");
        return result.toString();
    }

    public int getSize(){
        if(typeCache != null)return typeCache.getSize();
        return converter.size();
    }

    public String[] getPossibleValues(){
        if(typeCache != null)return null;
        return converter.getPossibleValues();
    }

    @Override
    public String toString() {
        if(typeCache != null)return typeCache.toString();
        String nc = converter.getClass().getName();
        String n  = nc.substring(0, nc.length() - 2).toUpperCase();
        return n + "\\" + nc + ".class";
    }
}