package com.tess.modbus.register.loader;

import com.tess.modbus.exceptions.RegistersException;

import java.nio.ByteBuffer;

/**
 * Created by Admin on 12.01.2018.
 */
public interface Converter {
    /**
     * Convert String value (from RegisteredValue for example) to Modbus STU formats for a writing operation
     * @param byteBuffer Buffer that going to be write to device
     * @param address byteBuffer offset in bytes
     * @param value Source value
     * @throws RegistersException
     */
    void UniToBin(ByteBuffer byteBuffer, int address, String value) throws RegistersException;

    /**
     * Convert received bin data from Modbus device to string
     * @param byteBuffer received data buffer
     * @param address byteBuffer offset in bytes
     * @return Ready for use string value
     * @throws RegistersException
     */
    String BinToUni(ByteBuffer byteBuffer, int address) throws RegistersException;
    String UniToDao(String className, String value) throws RegistersException;
    String DaoToUni(String className, Object o) throws RegistersException;
    String getDaoType(String className)throws RegistersException;
    int size();
    String[] getPossibleValues();
}
