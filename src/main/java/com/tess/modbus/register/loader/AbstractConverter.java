package com.tess.modbus.register.loader;

import com.tess.modbus.exceptions.RegistersException;

import java.nio.ByteBuffer;
import java.util.Locale;

/**
 * Created by Admin on 12.01.2018.
 */
abstract public class AbstractConverter implements Converter {
    protected int SIZE = 0;

    public static void swap(ByteBuffer bb, int address, int size){
//        address <<= 1;
        bb.position(address);
        while(size-- != 0){
            byte tmp = bb.get(address);
            bb.put(bb.get(address + 1));
            bb.put(tmp);
            address+=2;
        }
    }

    @Override
    public int size() {
        return SIZE;
    }

    @Override
    public String[] getPossibleValues() {
        return null;
    }

    @Override
    public void UniToBin(ByteBuffer byteBuffer, int address, String value) throws RegistersException {
    }

    @Override
    public String BinToUni(ByteBuffer byteBuffer, int address) throws RegistersException {
        return null;
    }

    @Override
    public String UniToDao(String className, String value) throws RegistersException {
        if(className.contains("MsSQL"))return  String.format(Locale.ENGLISH, "N'%s'", value);
        if("SQLite".equals(className) || "MsExcel".equals(className))return  String.format(Locale.ENGLISH,"'%s'", value);
        throw new RegistersException("Unknown type of SQL class");
    }

    @Override
    public String DaoToUni(String className, Object o) throws RegistersException {
        if(o == null)return null;
        if(o.toString().toUpperCase().equals("NULL"))return null;
        if(className.contains("MsSQL")
                || "SQLite".equals(className)
                || "MsExcel".equals(className))return o.toString();

        throw new RegistersException("Unknown type of SQL class");
    }

    @Override
    public String getDaoType(String className) throws RegistersException {
        if("MsSQLServer2000".equals(className))return "NVARCHAR(30)";
        if("SQLite".equals(className))return "TEXT";
        if("MsExcel".equals(className))return "TEXT";
        throw new RegistersException("Unknown type of SQL class");
    }
}
