package com.tess.modbus.register.loader;

import com.tess.CfgRoot;

import java.io.*;
import java.util.HashMap;

/**
 * Created by Admin on 12.01.2018.
 */
public class ConverterClassBox extends ClassLoader {
    private HashMap<String, Converter> box = new HashMap<>();
    public ConverterClassBox(ClassLoader parent) {
        super(parent);//move parent here
        File dir = new File(CfgRoot.PATH + "\\lib\\types\\");
        String[] converters = dir.list();
        for(String c:converters){
            if(c.contains(".class")){
                c = c.split("\\.")[0];
                try {
                    Class cl = loadClass(c);
                    Converter cnv = (Converter)cl.newInstance();
                    c = c.substring(0, c.length()-2).toUpperCase();
                    box.put(c, cnv);
                } catch (Exception ignored) {}
            }
        }

    }

    public Converter match(String type){
        return box.get(type);
    }
    @Override
    protected Class<?> findClass(String className) throws ClassNotFoundException {
        try {
            String path = CfgRoot.PATH + "\\lib\\types\\" + className + ".class";
            byte b[] = fetchClassFromFS(path);
            return defineClass(className, b, 0, b.length);
        } catch (FileNotFoundException ex) {
            return super.findClass(className);
        } catch (IOException ex) {
            return super.findClass(className);
        }
    }

    private byte[] fetchClassFromFS(String path) throws FileNotFoundException, IOException {
        InputStream is = new FileInputStream(new File(path));
        long length = new File(path).length();
        if (length > Integer.MAX_VALUE) {
            throw new IOException("File is too much large");
        }
        byte[] bytes = new byte[(int)length];
        int offset = 0;
        int numRead = 0;
        while (offset < bytes.length
                && (numRead=is.read(bytes, offset, bytes.length-offset)) >= 0) {
            offset += numRead;
        }
        if (offset < bytes.length) {
            throw new IOException("Could not completely read file "+path);
        }
        is.close();
        return bytes;
    }

}
