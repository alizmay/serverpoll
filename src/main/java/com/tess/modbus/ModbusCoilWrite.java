package com.tess.modbus;

import com.tess.dataLinks.Portal;
import com.tess.modbus.register.RegisteredValue;
import com.tess.modbus.register.RegisteredValueSet;

import java.io.IOException;
import java.nio.ByteBuffer;

/**
 * Created by Admin on 25.01.2018.
 */
public class ModbusCoilWrite extends ModbusBasic {
    public ModbusCoilWrite(Portal portal, int netAddress) {
        super(portal, netAddress);
    }

    public void writeCoil(RegisteredValueSet in) throws IOException {
        if(in == null || in.size() == 0)throw new IOException();
        byte header[] = new byte[]{netAddress, 0x05, 0, 0};
        byte payload[] = new byte[]{0, 0};
        ByteBuffer bb = ByteBuffer.wrap(payload);
        for(RegisteredValue rv: in){
            rv.getRegister().getTypeMapping().transformation(bb, 0, rv.getValue());
            header[2] = (byte)(rv.getRegister().getAddress() >> 8);
            header[3] = (byte)(rv.getRegister().getAddress() & 0xFF);
            trade(header, payload);
        }
    }
}
