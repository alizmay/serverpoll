package com.tess.modbus.exceptions;

import java.io.IOException;

/**
 * Created by TESS on 08.06.2017.
 */
public class ModbusException extends IOException {
    public ModbusException() {
        super();
    }

    public ModbusException(String message) {
        super(message);
    }

    public ModbusException(String message, Throwable cause) {
        super(message, cause);
    }

    public ModbusException(Throwable cause) {
        super(cause);
    }

}

