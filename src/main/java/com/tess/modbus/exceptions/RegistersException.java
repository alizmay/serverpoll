package com.tess.modbus.exceptions;

/**
 * Exception for Registers
 * Created by TESS on 15.06.2017.
 */
public class RegistersException extends RuntimeException {
    public RegistersException() {
    }

    public RegistersException(String message) {
        super(message);
    }

    public RegistersException(String message, Throwable cause) {
        super(message, cause);
    }

    public RegistersException(Throwable cause) {
        super(cause);
    }

    public RegistersException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
