package com.tess.modbus;

import com.tess.dataLinks.Portal;
import com.tess.modbus.register.Register;
import com.tess.modbus.register.RegisteredValue;
import com.tess.modbus.register.RegisteredValueSet;
import com.tess.modbus.register.TypeMapping;
import com.tess.modbus.types.ArchiveClock;
import com.tess.modbus.types.Clock;
import com.tess.modbus.types.ProgressMonitor;

import java.io.IOException;
import java.util.*;

/**
 * Created by TESS on 01.06.2016.
 * Implementation for the 6x function of device
 */
public class ModbusArchive extends ModbusRegisterRead{
    public static final String NoDataString = "NULL";
    private Set<Register>archiveRegisters = null;
    private ProgressMonitor progressMonitor = null;

    public ModbusArchive(Portal portal, int netAddress) {
        super(portal, netAddress);
    }

//    public void setArchiveRegisters(Set<Register> archiveRegisters) {
//        Iterator<Register>it = archiveRegisters.iterator();
//        while(it.hasNext()){
//            Register r = it.next();
//            if(r.getAddress() < 0)it.remove();
//        }
//        this.archiveRegisters = archiveRegisters;
//    }
//
//    public RegisteredValueSet readArchive(ArchiveClock date)throws IOException{
//        if(portal == null)throw new IOException("There is no opened port");
//        if(date == null)throw new IOException("Incoming data are invalid");
//        if(archiveRegisters == null)throw new IOException("Didn't detect the registers set");
//        Clock.SRTClock rt = date.getRtClock();
//        byte[]b = {netAddress,(byte)(65 + date.getArchiveMode().ordinal()),
//                (byte)rt.min,(byte)rt.hour,(byte)rt.day,(byte)rt.mon,(byte)rt.year};
//        RegisteredValueSet record = readData(b, archiveRegisters);
//        if(record.getRegisteredValue(0).getValue().equals("NaN")){
//            for(RegisteredValue rv:record)
//                if(!rv.getRegister().getMark().contains("stateDuration"))rv.setValue(NoDataString);
//        }
//        return record;
//    }
//
//    public Map<ArchiveClock, RegisteredValueSet>readArchive(Set<ArchiveClock>clockSet)throws IOException{
//        if(portal == null)throw new IOException("There is no opened port");
//        if(clockSet == null)throw new IOException("Incoming data ara invalid");
//        Map<ArchiveClock, RegisteredValueSet>result = new HashMap<>();
//        for(ArchiveClock ac : clockSet){
//            RegisteredValueSet rvs = readArchive(ac);
//            result.put(ac, rvs);
//        }
//        return result;
//    }
    private int getRegisterSetLength(Set<Register>areg){
        int sum = 0;
        for (Register register : areg) {
            if(register.getAddress() >=0) sum+= register.getTypeMapping().getSize();
        }
        return sum << 1;
    }
    public RegisteredValueSet readArchive(ArchiveClock date, Set<Register>areg)throws IOException{
    //    if(portal == null)throw new IOException("There is no opened port");
    //    if(date == null)throw new IOException("Incoming data ara invalid");
    //    if(archiveRegisters == null)throw new IOException("Didn't detect the registers set");
        Clock.SRTClock rt = date.getRtClock();
        expectedOperator = (byte)(65 + date.getArchiveMode().ordinal());
        expectedBytes =getRegisterSetLength(areg);
        byte[]b = {netAddress, expectedOperator,
                (byte)rt.min,(byte)rt.hour,(byte)rt.day,(byte)rt.mon,(byte)rt.year};
        RegisteredValueSet record = readData(b, areg);
        if(record.getRegisteredValue(0).getValue().equals("NaN")){
            for(RegisteredValue rv:record)
                if(!rv.getRegister().getMark().contains("stateDuration"))rv.setValue(NoDataString);
//                else rv.setValue(String.valueOf(date.getIntervalMinutes()));
        }
        for(Register r: areg){
            if(r.getAddress() < 0 && r.getTypeMapping().getCache().equals(TypeMapping.TypeMappingEnum.CLOCK))
                            record.add(new RegisteredValue(r, date.toString()));
        }
        return record;
    }

    public void setProgressMonitor(ProgressMonitor progressMonitor){
        this.progressMonitor = progressMonitor;
    }

    public Map<ArchiveClock, RegisteredValueSet>readArchive(ArchiveClock from, ArchiveClock to, Set<Register>regset){
//        if(portal == null)throw new IOException("There is no opened port");
//        if(clockSet == null)throw new IOException("Incoming data ara invalid");
        Map<ArchiveClock, RegisteredValueSet>res = new HashMap<>();

        ArchiveClock f = to, t = from;
        boolean direction = false;
        if(from.asLong() < to.asLong()){
            f = from; t= to; direction = true;
        }
        ArchiveClock i = f;
        if(progressMonitor != null)progressMonitor.init(0, (int)(t.asLong() - f.asLong()));
        try {
            do{
                RegisteredValueSet rvs = readArchive(i, regset);
                res.put(new ArchiveClock(i), rvs);
                if(i.asLong() == t.asLong())break;
                i.increment(direction);
                if(progressMonitor != null)progressMonitor.step(i.toString());
            }while(true);
        } catch (IOException ifnored) {}
        return res;
    }
}
