package com.tess.modbus;

import com.tess.dataLinks.InetPortal;
import com.tess.dataLinks.Portal;

import java.io.*;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by Admin on 16.11.2018.
 */
public class Terminal{

    protected boolean run;
    protected final InputStream is;
    protected final OutputStream os;
    protected final OutputStream strings;
    protected AtomicBoolean updated = new AtomicBoolean(false);
    public Terminal(InetPortal portal, BufferedOutputStream sb) {
        is = portal.getInputStream();
        os = portal.getOutputStream();
        run = true;
        strings = sb;
    }

    public void run() throws InterruptedException {
        incoming.start();
        readKeyboard.start();
        readKeyboard.join();
        incoming.interrupt();
        incoming.join();

    }

    Thread readKeyboard = new Thread("read KB"){
        @Override
        public void run() {
            System.out.println("Please");
            while(true){
                try {
//                    strings.write("BOSOON!".getBytes());
                    System.out.println("BOSOON!");
                    os.write("BOSOON!\r".getBytes());
                    os.flush();
                    sleep(3000);
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

        }
    };

    Thread incoming = new Thread("incoming"){
        @Override
        public void run() {
            while(!isInterrupted()){
                try {
                    while(is.available() > 0){
                        int b = is.read();
                        System.out.print(String.valueOf((char)b));
                        updated.set(true);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    };

}
