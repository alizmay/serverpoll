package com.tess.modbus.wrappers;

import java.util.Arrays;

/**
 * Created by Admin on 26.06.2018.
 */
public class WrapperSRTU extends WrapperRTU {
    private short crc;
    @Override
    public byte[] wrapForWrite(byte[] buffer) {
        byte[] res = super.wrapForWrite(buffer);
        crc =(short) ((res[res.length-2] << 8) + res[res.length-1]);
        if(!(res[1]==65 && (res[5] >=10 || res[5] <7)))crc = 0;
        return res;
    }

    @Override
    public byte[] wrapForRead(byte[] buffer, int offset, int len) {
        return wrapForRead(buffer);
    }

    protected byte[] wrapForRead(byte[] buffer) {
        byte[] res = super.wrapForRead(buffer);
        if(res == null)return null;
        if((crc & 0xA5) != 0){
            for(int i = 3; i < 11; i++)res[i] = 0;
            return super.wrapForWrite(Arrays.copyOf(res, res.length - 2));
        }
        return res;
    }
}
