package com.tess.modbus.wrappers;

import java.util.Arrays;

/**
 * Created by ali on 19.11.2017.
 */
public class WrapperBootloader implements Wrapper {
    @Override
    public byte[] wrapForWrite(byte[] buffer) {
        byte[] res = buffer.clone();
        return res;
    }

//    @Override
//    public byte[] wrapForRead(byte[] buffer) {
//        return Arrays.copyOf(buffer, buffer.length);
//    }

    @Override
    public byte[] wrapForRead(byte[] buffer, int offset, int len) {
        byte[] reply = Arrays.copyOf(buffer, len);
        return reply;
    }

    @Override
    public int getPacketSize() {
        return 1024;
    }
}
