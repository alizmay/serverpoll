package com.tess.modbus.wrappers;

/**
 * Created by TESS on 31.05.2016.
 * Wrapper class for using in portals as type of ModBus. RTU, ASCII, TCP
 */
public interface Wrapper {
    byte[]wrapForWrite(byte[]buffer);
    //byte[]wrapForRead(byte[]buffer);
    byte[]wrapForRead(byte[]buffer, int offset, int len);
    int getPacketSize();
}
