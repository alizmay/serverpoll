package com.tess.modbus.wrappers;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Admin on 16.10.2019.
 */
public class WrapperASCII implements Wrapper {
    @Override
    public byte[] wrapForWrite(byte[] buffer) {
        if(buffer == null || buffer.length == 0)return buffer;
        byte sum = lrc(buffer);
        byte[]res = Arrays.copyOf(buffer, buffer.length + 1);
        res[res.length - 1] = sum;
        return encoding(res);

    }

//    @Override
//    public byte[] wrapForRead(byte[] buffer) {
//        return wrapForRead(buffer, 0, buffer.length);
//    }

    @Override
    public byte[] wrapForRead(byte[] buffer, int offset, int len) {
        int startPack = -1, endPack = -1;
        for (int i = offset; i < len ; i++) {
            if(buffer[i] == ':')startPack = i;
        }
        if((startPack < 0)||(startPack == buffer.length - 1))return null;
        for (int i = startPack + 1; i < len ; i++) {
            if((buffer[i] == 0x0A) && (buffer[i - 1] == 0xD))endPack = i - 1;
        }
        if(endPack < 0)return null;
        byte res[] = decoding(buffer, startPack+1, endPack-1);
        if(res == null)return null;
        if( res[res.length - 1] + lrc(res) == 0) return res;
        return null;
    }

    @Override
    public int getPacketSize() {
        return 124;
    }

    public static byte lrc(byte[] buf){
        byte sum=0;
        for (int i = 0; i < buf.length; i++) {
            sum += buf[i];
        }
        return (byte)(- sum);
    }

    protected byte[] encoding(byte[] in){
        byte[] out = new byte[in.length*2 + 3];
        out[0] = ':';
        int y = 1;
        for (int i = 0; i < in.length; i++) {
            char mn;
            mn = (char)((in[i] & 0xF0) >> 4);
            mn = (char)( (mn <= 9)? mn + '0': mn - 10 + 'A');
            out[y++] = (byte)mn;
            mn = (char)(in[i] & 0x0F);
            mn = (char)( (mn <= 9)? mn + '0': mn - 10 + 'A');
            out[y++] = (byte)mn;
        }
        out[y++] = 0x0D;
        out[y++] = 0x0A;
        return out;
    }

    protected byte[] decoding(byte[] buf, int start, int end){
        if(((end - start)&0x01) == 0)return null;
        byte[] res = new byte[end - start];

        for (int i = start, y = 0; i < end ; i+=2, y++) {
            res[y] = (byte)((buf[i] > '9'? buf[i] - 'A' + 10: buf[i] - '0') << 4);
            res[y] |= (byte)((buf[i+1] > '9'? buf[i+1] - 'A' + 10: buf[i+1] - '0'));
        }
        return res;
    }

}
