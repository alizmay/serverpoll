package com.tess.modbus;

import com.tess.dataLinks.Portal;
import com.tess.modbus.register.RegisteredValue;
import com.tess.modbus.register.RegisteredValueSet;
import com.tess.modbus.register.Registers;
import com.tess.modbus.register.RegistersFactory;

import java.io.IOException;

/**
 * Created by TESS on 18.05.2016.
 * Implementation of the 17 ModBus function
 */
public class ModbusDeviceID  {
    private byte netAddress;
    private Portal portal;

    public ModbusDeviceID(Portal portal, int netAddress) {
        this.portal = portal;
        setNetAddress(netAddress);
    }

    public void setNetAddress(int netAddress) {
        this.netAddress = (byte)(netAddress & 0xFF);
    }

    public RegisteredValueSet readID() throws IOException {
        byte[] buffer = new byte[2];
        buffer[0] = netAddress;
        buffer[1] = 17;
        buffer = portal.request(buffer);
        RegisteredValueSet registeredValues = new RegisteredValueSet();
        Registers registers = RegistersFactory.getCommon();//getInstance(null);
        if (buffer != null) {
            StringBuilder sb = new StringBuilder();
            int indx = 0;
            for (int i = 3; i < buffer.length; i++) {
                if (buffer[i] != 0) sb.append((char) buffer[i]);
                else {
                    if(sb.length() != 0){
                        registeredValues.add(new RegisteredValue(registers.getRegister(DeviceHead.PARAMETERS_NAMES[indx++]),sb.toString()));
                        if (indx > 3) break;
                        sb.setLength(0);
                    }
                }
            }
        }
        return registeredValues;
    }
}
