package com.tess.modbus;

import com.tess.dataLinks.Portal;
import com.tess.modbus.ModbusBasic;
import com.tess.modbus.register.RegisteredValue;
import com.tess.modbus.register.RegisteredValueSet;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;						  
import java.util.Arrays;
import java.util.Iterator;
import java.util.Set;

/**
 * Created by TESS on 08.06.2017.
 */
public class ModbusRegisterWrite extends ModbusBasic {
    public ModbusRegisterWrite(Portal portal, int netAddress) {
        super(portal, netAddress);
    }

    private byte[] getHeader(int base, int length){
        byte len = (byte)length;
        return  new byte[]{getNetAddress(), 0x10, (byte) (base >> 8), (byte) (base & 0xFF),0, len, (byte)(len * 2)};
    }
//    public void writeRegister(Set<RegisteredValue> in) throws IOException{
//        if(in == null || in.size() == 0)throw new IOException();
//        byte buffer[] = new byte[256];
//        ByteBuffer byteBuffer = ByteBuffer.wrap(buffer);
//        Iterator<RegisteredValue>it = in.iterator();
//        RegisteredValue rv = null;
//        int baseAdr = 0;
//        int length = 0;
//        byte header[] = getHeader(baseAdr, length);
//        while(it.hasNext()){
//            rv = it.next();
//            if(length == 0){
//                baseAdr = rv.getRegister().getAddress();
//                length = rv.getRegister().getTypeMapping().transformation(byteBuffer, length, rv.getValue());
//                continue;
//            }
//            int adr = rv.getRegister().getAddress();
//            int le = rv.getRegister().getTypeMapping().getSize() + length;
//            if(adr == baseAdr + length && le < (248 - header.length)/2){
//                length = rv.getRegister().getTypeMapping().transformation(byteBuffer, length, rv.getValue());
//            }else {
////                byte data[] = Arrays.copyOf(byteBuffer.array(), len);
//                trade(getHeader(baseAdr, length) , Arrays.copyOf(byteBuffer.array(), length*2));
//                length = 0;
//            }
//        }
//        if(length > 0)trade(getHeader(baseAdr, length), Arrays.copyOf(byteBuffer.array(), length*2));
//    }

    public void writeRegister(RegisteredValueSet in) throws IOException{
        if(in == null || in.size() == 0)throw new IOException();
        byte buffer[] = new byte[256];
        ByteBuffer byteBuffer = ByteBuffer.wrap(buffer);
	    byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
        Iterator<RegisteredValue>it = in.iterator();
        RegisteredValue rv = it.next();
        int baseAdr = rv.getRegister().getAddress();
        int length = rv.getRegister().getTypeMapping().transformation(byteBuffer, 0, rv.getValue());
        byte header[] = getHeader(baseAdr, length);
        while(it.hasNext()){
            rv = it.next();
            int adr = rv.getRegister().getAddress();
            int le = rv.getRegister().getTypeMapping().getSize() + length;
            if(adr == baseAdr + length && le < (portal.getWrapper().getPacketSize() - header.length)/2){
                length = rv.getRegister().getTypeMapping().transformation(byteBuffer, length, rv.getValue());
            }else {
//                    byte data[] = Arrays.copyOf(byteBuffer.array(), len);
                trade(getHeader(baseAdr, length) , Arrays.copyOf(byteBuffer.array(), length*2));
                baseAdr = rv.getRegister().getAddress();
                length = rv.getRegister().getTypeMapping().transformation(byteBuffer, 0, rv.getValue());
            }
        }
        if(length > 0)trade(getHeader(baseAdr, length), Arrays.copyOf(byteBuffer.array(), length*2));
    }

}
