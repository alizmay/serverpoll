package com.tess.modbus;

import com.tess.dataLinks.Portal;
import com.tess.modbus.exceptions.ModbusException;
import com.tess.modbus.register.Register;
import com.tess.modbus.register.RegisteredValue;
import com.tess.modbus.register.RegisteredValueSet;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Iterator;
import java.util.Set;

/**
 * Created by TESS on 02.06.2016.
 * Basic class for ModBus registers
 */
public abstract class ModbusBasic implements Modbus {

    Portal portal = null;
    byte netAddress;


    public ModbusBasic(){
    }
    public ModbusBasic(Portal portal, int netAddress) {
        this.portal = portal;
        this.netAddress = (byte)(netAddress & 0xFF);
    }

//    public RegisteredValueSet readData(byte[]buffer, Set<Register> in) throws IOException { //TODO move this method to ModbusRegisterRead
//        if(buffer == null || buffer.length == 0 || in == null || in.size() == 0)return new RegisteredValueSet();
//        buffer = portal.request(buffer);
//        if(buffer == null)throw new IOException("No answer");
//        if((buffer[1] & 0x80) == 0x80)throw new IOException("Exceptional situation: " +buffer[2]);
//        for(int i = 3; i < buffer.length;i+=2)
//        {byte tmp = buffer[i]; buffer[i] = buffer[i+1]; buffer[i+1] = tmp;}
//        ByteBuffer byteBuffer = ByteBuffer.wrap(buffer);
//        byteBuffer = byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
//        RegisteredValueSet result = new RegisteredValueSet();
//
//        Iterator<Register> it = in.iterator();
//        Register r = it.next();
//        while(r.getAddress() < 0 && it.hasNext()){r = it.next();}
//        if(r.getAddress() < 0)return result;
//        int offset = r.getAddress()*2 - 3;
//
//        do{
//            int adr = r.getAddress()*2 - offset;
//            if(adr >= 0) {
//                adr = r.getAddress() * 2 - offset;
//                String value = r.getTypeMapping().transformation(byteBuffer, adr);
//                result.add(new RegisteredValue(r, value));
//            }
//            if(!it.hasNext())break;
//            r = it.next();
//        }while(true);
//        return result;
//    }


    public byte[] trade(byte header[], byte data[])throws IOException{
        if(portal == null)throw new IOException("There is no opened port");
        if(data == null)data = new byte[0];
        byte buffer[] = new byte[header.length + data.length];
        int i;
        for(i = 0;i < header.length;i++)buffer[i] = header[i];
        System.arraycopy(data, 0, buffer, i + 0, data.length);
        buffer = portal.request(buffer);
        if(buffer == null)throw new IOException("No answer");
        if((buffer[1] & 0x80) == 0x80){
            switch(buffer[2]){
                case 1: throw new ModbusException("Modbus: ILLEGAL FUNCTION");
                case 2: throw new ModbusException("Modbus: ILLEGAL DATA ADDRESS");
                case 3: throw new ModbusException("Modbus: ILLEGAL DATA VALUE");
                case 4: throw new ModbusException("Modbus: FAILURE IN ASSOCIATED DEVICE");
                case 5: throw new ModbusException("Modbus: ACKNOWLEDGE");
                case 6: throw new ModbusException("Modbus: BUSY, REJECTED MESSAGE");
                case 7: throw new ModbusException("Modbus: NAK-NEGATIVE ACKNOWLEDGMENT");
            }
            throw new ModbusException("Modbus: UNKNOWN EXCEPTION");
        }
        return buffer;
    }

    public void setNetAddress(byte netAddress) {
        this.netAddress = netAddress;
    }

    public byte getNetAddress() {
        return netAddress;
    }

    public void setPortal(Portal portal) {
        this.portal = portal;
    }
}
