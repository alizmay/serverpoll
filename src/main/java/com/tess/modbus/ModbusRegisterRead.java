package com.tess.modbus;

import com.tess.dataLinks.Portal;
import com.tess.modbus.exceptions.ModbusException;
import com.tess.modbus.register.Register;
import com.tess.modbus.register.RegisteredValue;
import com.tess.modbus.register.RegisteredValueSet;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.*;

/**
 * Created by TESS on 18.05.2016.
 * Implementation of the 3 ModBus function and the 16
 */
public class ModbusRegisterRead extends ModbusBasic {
    public final static int MODBUS_READ_REGISTER_OPERATION = 3;
    public ModbusRegisterRead(Portal portal, int netAddress){
        super(portal, netAddress);
    }
    protected int expectedBytes;
    protected byte expectedOperator;

    private ModbusRegisterRead(){
    }

    protected byte[] getHeader(int baseAdr, int length){
        byte[]buffer = new byte[6];
        buffer[0] = super.netAddress;
        buffer[1] = MODBUS_READ_REGISTER_OPERATION;
        buffer[2] = (byte)((baseAdr >> 8) & 0xFF);
        buffer[3] = (byte)(baseAdr & 0xFF);
        buffer[4] = (byte)((length >> 8) & 0xFF);
        buffer[5] = (byte)(length & 0xFF);
        expectedBytes = length << 1;
        expectedOperator = buffer[1];
        return buffer;
    }

    //TODO DEBUG
    String error;

    protected boolean isLengthAsExpected(byte buffer[]){

        if(buffer.length < expectedBytes)return false;
        if(!((buffer[2] & 0xFF) == expectedBytes)){
            error = String.format("[2]=%x expected=%x", buffer[2], expectedBytes);
        }
        return( buffer[2] & 0xFF) == expectedBytes;
    }

    protected boolean isOperatorAsExpected(byte buffer[]){
        if(!((buffer[1] & 0xFF) == expectedOperator)){
            error = String.format("[1]=%x expected=%x", buffer[1], expectedBytes);
        }
        return (buffer[1] & 0xFF) == expectedOperator;
    }

    boolean isNetAddressAsExpected(byte buffer[]){
        if(!((buffer[0] & 0xFF) == getNetAddress())){
            error = String.format("[0]=%x NA = %x", buffer[0], getNetAddress());
        }
        return (buffer[0] & 0xFF) == getNetAddress();
    }

    protected byte[] swapBuffer(byte[] buffer){
        byte[]nb = new byte[buffer.length - 3];
        for(int i = 3, y = 0; i < buffer.length; i += 2, y +=2 ){
            nb[y + 1] = buffer[i]; nb[y] = buffer[i+1];
        }
        return nb;
    }

    RegisteredValueSet readData(byte[] header, Set<Register> in) throws IOException { //TODO move this method to ModbusRegisterRead
        byte buffer[];
        try {
            buffer = trade(header, null);

        } catch (ModbusException e) {
            throw new IOException(e.getMessage());
        }
        //header = portal.request(header);
        if(buffer == null)throw new IOException("No answer");
        if(!isLengthAsExpected(buffer) || !isOperatorAsExpected(buffer) || !isNetAddressAsExpected(buffer))
            throw new IOException("Incorrect answer: " + error);
        buffer = swapBuffer(buffer);
        //for(int i = 3; i < buffer.length; i+=2){byte tmp = buffer[i]; buffer[i] = buffer[i+1]; buffer[i+1] = tmp;}
        ByteBuffer byteBuffer = ByteBuffer.wrap(buffer);
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
        RegisteredValueSet result = new RegisteredValueSet();

        Iterator<Register> it = in.iterator();
        Register r = it.next();
        while(r.getAddress() < 0 && it.hasNext()){r = it.next();}
        if(r.getAddress() < 0)return result;
        int offset = r.getAddress()*2;// - 3;

        do{
            int adr = r.getAddress()*2 - offset;
            if(adr >= 0) {
                adr = r.getAddress() * 2 - offset;
                String value = r.getTypeMapping().transformation(byteBuffer, adr);
                result.add(new RegisteredValue(r, value));
            }
            if(!it.hasNext())break;
            r = it.next();
        }while(true);
        return result;
    }

    public RegisteredValueSet readRegister(TreeSet<Register>set) throws IOException {
        RegisteredValueSet result = new RegisteredValueSet();

        if(set == null || set.size() == 0 || netAddress < 1)throw new IOException("Incoming data are invalid");
        Set<Register>localSet = new TreeSet<>();
        int startAddress = -1, length = 0;
        int headerLength = getHeader(0, 0).length;
        boolean ini = true;
        for(Register r:set){
            if(r.getGroupMark().equals("Archive"))continue;
            if(ini){
                startAddress = r.getAddress();
                if(startAddress < 0){result.add(new RegisteredValue(r, null)); continue;}
                ini = false;
                length = r.getTypeMapping().getSize();
                localSet.add(r);
                continue;
            }
            int adr = r.getAddress();
            if(adr < 0)continue;
            int newLength = adr - startAddress + r.getTypeMapping().getSize();
            if(newLength < (portal.getWrapper().getPacketSize() - headerLength) / 2){
                length = newLength;
                localSet.add(r);
            }else {
                for(RegisteredValue rv:readData(getHeader(startAddress, length), localSet))result.add(rv);
//                startAddress = -1;
//                length = 0;
                localSet.clear();
                startAddress = r.getAddress();
                length = r.getTypeMapping().getSize();
                localSet.add(r);
            }
        }
        if(length > 0) for(RegisteredValue rv:readData(getHeader(startAddress, length), localSet))result.add(rv);
        return result;
    }


    public List<RegisteredValueSet> readRegistersRepeat(final TreeSet<Register>set, int repNumber, int addressOffset) throws IOException {
        if(repNumber == 1)return new ArrayList<RegisteredValueSet>(1){{add(readRegister(set));}};
//        TreeSet<Register> ns = new TreeSet<>();
        int startAddress = set.first().getAddress();
        int endAddress = set.last().getAddress() + set.last().getTypeMapping().getSize();
        int len = endAddress - startAddress;
        int repNumberShadow = repNumber;
//        startAddress += addressOffset;
        TreeSet<Register>cloneSet  = new TreeSet<>();
        do {
            repNumber--;
            addressOffset += len;

            for(Register r : set){
                Register rn = new Register(r.getMark()+"#"+repNumber, r.getAddress() + addressOffset, r.getTypeMapping());
                cloneSet.add(rn);
            }
        }while(repNumber > 0);
        RegisteredValueSet re = readRegister(cloneSet);
        List<RegisteredValueSet> l = new ArrayList<>();
        while(repNumberShadow-- > 0 && re.size() > 0){
            RegisteredValueSet lir = new RegisteredValueSet();
            for(Register r : set){
                Iterator<RegisteredValue>it = re.iterator();
                while(it.hasNext()) {
                    RegisteredValue rvo = it.next();
                    if ((r.getMark() + '#' + repNumberShadow).equals(rvo.getRegister().getMark())){
                        lir.add(new RegisteredValue(r, rvo.getValue()));
                        it.remove();
                        break;
                    }
                }
            }
            l.add(lir);
        }
        return l;
    }
}
