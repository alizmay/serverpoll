package com.tess.modbus;

import com.tess.dataLinks.Portal;

import java.io.IOException;

/**
 * Created by TESS on 30.11.2017.
 */
public class ModbusSystemReset {
    private Portal portal;

    public ModbusSystemReset(Portal portal) {
        this.portal = portal;
    }

    public void reset(byte netAddress) throws IOException {
        byte[] bytes = {netAddress, 0x47, (byte)0x81, (byte)0xD2 };
        portal.request(bytes);
    }
}
