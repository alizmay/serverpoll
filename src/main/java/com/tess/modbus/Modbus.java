package com.tess.modbus;

import com.tess.dataLinks.Portal;
import com.tess.modbus.exceptions.ModbusException;
import com.tess.modbus.register.Register;
import com.tess.modbus.register.RegisteredValueSet;

import java.io.IOException;
import java.util.Set;

/**
 * Created by TESS on 18.05.2016.
 * Interface for ModBus classes
 */
public interface Modbus {
    void setNetAddress(byte netAddress);
    byte getNetAddress();
    void setPortal(Portal portal);
    byte[] trade(byte header[], byte data[])throws IOException;
    //RegisteredValueSet readData(byte[]buffer, Set<Register> in) throws IOException;
}
