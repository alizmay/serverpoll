package com.tess.modbus;

import com.tess.CfgRoot;
import com.tess.modbus.exceptions.RegistersException;
import com.tess.modbus.register.*;
import com.tess.modbus.register.versionSystem.VersionFactory;
import com.tess.modbus.register.versionSystem.VersionSystem;
import com.tess.modbus.register.versionSystem.VersionSystemClass;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URI;
import java.util.*;

/**
 * Created by TESS on 06.06.2016.
 * Definition class of device common parameters
 */
public class DeviceHead implements ParserTypeName {
    public static final String[] PARAMETERS_NAMES = {"DeviceHead:Vendor", "DeviceHead:Device", "DeviceHead:SerNo", "DeviceHead:Version"};
    private String vendor = "TESS-Engineering";
    private String device = null;
    private String subTypeDevice = null;
    private String hardwareVersion = null;
    private String optionsVersion[] = null;
    private int version = 0;
    private URI uri;
    private String serialNumber = "00000";
//CONTRUCTORS----------------------------------------------------------------------------------------------------
    public DeviceHead(){}

    public DeviceHead(URI uri) {
        this.uri = uri;
    }

    public DeviceHead(RegisteredValueSet set){
        parseDeviceRvs(set);
    }

    public DeviceHead(URI uri, String device, int version) {
        this.uri = uri;
        this.device = device;
        this.version = version;
        throw new RuntimeException("What about recognize?");
    }
//---------------------------------------------------------------------------------------------------------------

    public void parseDeviceRvs(RegisteredValueSet set){
        if (set == null || set.size() == 0) return;
        try {
            vendor = set.getRegisteredValue("DeviceHead:Vendor").getValue();
        } catch (Exception ignored) {
        }
        try {
            device = set.getRegisteredValue("DeviceHead:Device").getValue();
        } catch (Exception ignored) {
        }
        try {
            serialNumber = set.getRegisteredValue("DeviceHead:SerNo").getValue();
        } catch (Exception ignored) {
        }
        try {
            version = Integer.parseInt(set.getRegisteredValue("DeviceHead:Version").getValue());
        } catch (Exception e) {
            version = 0;
        }
        if (version == 0) {
            String array[] = device.split("[\\. ]");
            if (array.length == 0) return;
            Map<Integer, File> versions = new TreeMap<>();
            subTypeDevice = array[0];
            version = Integer.parseInt(array[1] + array[2]);
        }
        int space = device.indexOf(' ');
        if (space != -1) device = device.substring(space);
        String[] parts = device.split("\\.");
        try {
            hardwareVersion = parts[2];
        } catch (Exception e) {
            hardwareVersion = "";
        }
        try {
            ArrayList<String>l = new ArrayList();
            StringBuilder sb = new StringBuilder();
            sb.append(parts[3].charAt(0));
            for(int i = 1; i < parts[3].length(); i++){
                char ch = parts[3].charAt(i);
                if(ch < 'a'){
                    l.add(sb.toString());
                    sb.setLength(0);
                }
                sb.append(ch);
            }
            if(sb.length() != 0)l.add(sb.toString());
            optionsVersion = l.toArray(new String[l.size()]);
        } catch (Exception e) {
            optionsVersion = null;
        }
    }

    public URI getURI() {
        return uri;
    }

    public void setUri(URI uri) {
        this.uri = uri;
    }

    public byte getChannel() {
        return (byte) (Integer.parseInt(uri.getQuery()) & 0xFF);
    }

    public String getDevice() {
        return device;
    }

    public int getVersion() {
        return version;
    }

    public String getHardwareVersion() {
        return hardwareVersion;
    }


    public String[] getOptionsVersion() {
        return optionsVersion;
    }

    public boolean containOptions(String str){
        if(optionsVersion == null)return false;
        for(String s : optionsVersion)
            if(s.equals(str))return true;
        return false;
    }



    public String getSubTypeDevice() {
        return subTypeDevice;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    private static final String PATH = "\\Config\\Registers";

    private String parserDeterminator(String ignored) throws RegistersException {
        if(device == null)return "Common";
        String array[] = device.split("[\\. ]");
        if(array.length == 0)return "Common";
        File f = new File(CfgRoot.PATH + PATH);
        File[] list = f.listFiles();
        if(list == null)return "Common";
        Map<Integer, File> versions = new TreeMap<>();

        if(containOptions("Ttr")) {
            for (File ff:list)
                if (ff.isFile() && ff.getName().contains(".json") && ff.getName().contains("TTR")){
                    Integer integer = Integer.parseInt(ff.getName().substring(ff.getName().length()-9,
                            ff.getName().length()-5));
                    versions.put(integer, ff);
                }
        }else if("URG2KM".equals(subTypeDevice)) {
            for (File ff:list)
                if (ff.isFile() && ff.getName().contains(".json") && ff.getName().contains("URG")){
                    Integer integer = Integer.parseInt(ff.getName().substring(ff.getName().length()-9,
                            ff.getName().length()-5));
                    versions.put(integer, ff);
                }
        }else if("STU-1".equals(subTypeDevice)) {
            for (File ff : list)
                if (ff.isFile() && ff.getName().contains(".json") && ff.getName().contains("STU")) {
                    Integer integer = Integer.parseInt(ff.getName().substring(ff.getName().length() - 9,
                            ff.getName().length() - 5));
                    versions.put(integer, ff);
                }
        }
        File file = null;
        for(Map.Entry<Integer, File>pair: versions.entrySet())
            if(version >= pair.getKey())file = pair.getValue();
        if(file == null)return "Common";
        return file.getName().substring(0, file.getName().length()-5);
    }

    public String[] getServices(){
        throw new RuntimeException("Need to use getDeviceHead().getVersionSystem().getServices()");
//        int v = deviceHead.getVersion() / 1000;
//        if(v == 4)return new String[]{"syncTime_service"};
//        if(v == 5)return new String[]{"syncTime_service","wipeArchive_service", "flowrateCalibration_service",
//                "flowrateTrialCalibration_service","resetDevice5000_service"};
//        return null;
    }

    @Override
    public String getReferenceType(String ignored) throws RegistersException {
//        return parserDeterminator(ignored);
        return versionSystem.getRegistersName();
    }

    protected final static String DEVICES_INFO_PATH = "\\Config\\DevsInfo";
    protected VersionSystem versionSystem = null;
    public VersionSystem getVersionSystem() {
        return versionSystem;
    }
    public void recognize() throws IOException {
        versionSystem = loadDevInfo(new VersionFactory<VersionSystem>() {
            @Override
            public VersionSystem getInstance(Properties prop){
                return new VersionSystemClass(prop);
            }
        },  this);
    }

    protected final static <T extends VersionSystem> T loadDevInfo(VersionFactory<? extends VersionSystem> versionFactory, DeviceHead dh) throws IOException {
        File f = new File(CfgRoot.PATH + DEVICES_INFO_PATH);
        ArrayList<? super VersionSystem> infos = new ArrayList<>();
        ArrayList<String>fileNames = new ArrayList<>();
        int maxCoincides = 0;
        File[] list = f.listFiles();
        for(File file :list){
            if(!(file.isFile() && file.getName().endsWith(".dprop")))continue;
            FileInputStream fis = new FileInputStream(CfgRoot.PATH + DEVICES_INFO_PATH + '\\' +  file.getName());
            Properties prop = new Properties();
            prop.load(fis);
            T versionSystem = (T) versionFactory.getInstance(prop);
            int coincides = versionSystem.getCoincides(dh);
            if( coincides > maxCoincides){
                maxCoincides = coincides;
                infos.clear();
                fileNames.clear();
            }
            if(coincides == maxCoincides) {
                infos.add(versionSystem);
                fileNames.add(file.getName());
            }
            fis.close();
        }

        if(infos.size() > 1){
                String message = "More than 1 coincides: ";
                for (String s : fileNames)
                    message += '[' + s + ']';
                throw new RuntimeException("More than 1 coincides: " + message);
        }
        if(infos.size() == 0)throw new RuntimeException("No devices found for: " + dh.getDevice());
        return (T)infos.get(0);
    }
}
