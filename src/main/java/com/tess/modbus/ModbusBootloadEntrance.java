package com.tess.modbus;

import com.tess.dataLinks.Portal;

import java.io.IOException;

/**
 * Created by ali on 18.11.2017.
 */
public class ModbusBootloadEntrance {
    private Portal portal;
    private int bootloaderVersion;

    public ModbusBootloadEntrance(Portal portal) {
        this.portal = portal; bootloaderVersion = 0;
    }

    public int bootloaderEntrance(byte netAddress) throws IOException {
        byte[] bytes = {netAddress, 0x46};
        bytes = portal.request(bytes);
        bootloaderVersion = ((((int)bytes[2]) & 0xFF)<<8) + (((int)bytes[3])&0xFF);
        return bootloaderVersion;
    }

}
