package com.tess.modbus.types;

import java.util.Calendar;

/**
 * Created by TESS on 01.06.2016.
 * Clock class compatible with STU Clock parameters
 */
public class Clock implements Comparable<Clock>{

    long date;
    public static class SRTClock{
        public short sec, min, hour, day, mon, year;

        public SRTClock(int sec, int min, int hour, int day, int mon, int year) {
            this.sec = (short)sec;
            this.min = (short)min;
            this.hour = (short)hour;
            this.day = (short)day;
            this.mon = (short)mon;
            this.year = (short)year;
        }

        public SRTClock(SRTClock srtClock){
            sec = srtClock.sec; min = srtClock.min; hour = srtClock.hour; day = srtClock.day; mon = srtClock.mon;
            year = srtClock.year;
        }

        public SRTClock(Calendar cl){
            sec = (short)cl.get(Calendar.SECOND);
            min = (short)cl.get(Calendar.MINUTE);
            hour= (short)cl.get(Calendar.HOUR_OF_DAY);
            day = (short)cl.get(Calendar.DAY_OF_MONTH);
            mon = (short)(cl.get(Calendar.MONTH) + 1);
            year= (short)(cl.get(Calendar.YEAR) - 2000);
        }

        public SRTClock() {
            this(Calendar.getInstance());
        }
    }
    SRTClock rtClock;

    public Clock() {
        rtClock  = new SRTClock();
        date = jday(rtClock);
    }

    public Clock(Calendar r){
        rtClock = new SRTClock(r);
        date = jday(rtClock);
    }

    public Clock(String str){
        String[]arr = str.split("[\\s:\\.]+");
        rtClock = new SRTClock();
        try{
            if(arr.length == 3 && str.contains(".")){
                rtClock.day = Short.parseShort(arr[0]);
                rtClock.mon = Short.parseShort(arr[1]);
                rtClock.year= Short.parseShort(arr[2]);
                rtClock.min = 0;
                rtClock.hour= 0;
            }else
            if(arr.length == 5 || arr.length == 6){
                if(arr.length == 6)rtClock.sec = Short.parseShort(arr[5]);
                rtClock.min = Short.parseShort(arr[4]);
                rtClock.hour= Short.parseShort(arr[3]);
                rtClock.day = Short.parseShort(arr[0]);
                rtClock.mon = Short.parseShort(arr[1]);
                rtClock.year= Short.parseShort(arr[2]);
                if(rtClock.year > 2000)rtClock.year -= 2000;
            }
        }catch(Exception e){
            throw new NumberFormatException("Wrong Clock format: " + str);
        }
        date = jday(rtClock);
    }

    public Clock(long value){
        date = value;
        rtClock = jdate(date);
    }

    public Clock(Clock clock){
        rtClock = new SRTClock(clock.getRtClock());
        date = jday(rtClock);
    }




    public SRTClock getRtClock() {
        return new SRTClock(rtClock);
    }


    public void setRtClock(SRTClock rtClock) {
        this.rtClock = new SRTClock(rtClock);
        date = jday(rtClock);
    }

    static SRTClock jdate(long ju)
    {
        SRTClock rt = new SRTClock();
        long du,mu,yu;
        rt.min = (short)(ju%60);
        ju-=rt.min; ju/=60;
        rt.hour = (short)(ju%24);
        ju-=rt.hour;ju/=24;
        ju=ju+2414989-1721119;
        yu=(4*ju-1)/146097; du=(4*ju-1-146097*yu)/4;
        ju=(4*du+3)/1461; du=(4*du+7-1461*ju)/4;
        mu=(5*du-3)/153;  du=(5*du+2-153*mu)/5;
        yu=100*yu+ju;
        if(mu<10) mu=mu+3; else { mu=mu-9; yu=yu+1; }
        yu=yu-1900;
        if(yu>99) yu=yu-100;
        rt.day=(short)du; rt.mon=(short)mu; rt.year=(short)yu; rt.sec = 0;
        return rt;
    }

    static long jday(SRTClock rt)
    {
        int year,day,mon;
        long c,ya;
        day=rt.day;
        mon=rt.mon;
        year=rt.year;
        if(mon>2) mon=mon-3; else { mon=mon+9; year=year-1; }
        if(year<52) year=year+100;
        c=(year+1900)/100; ya=(year+1900)-100*c;
        c=((146097*c)/4+(1461*ya)/4+(153*mon+2)/5+day+1721119-2414989);//*(RTHours*RTMinutes)+rt.Hour*(RTMinutes)+rt.Min*RTSeconds;
        c=(c*24+rt.hour)*60+rt.min;
        return c;
    }

    public long asLong() {
        return date;
    }

    public long getAsLinuxEpochMs(){
        Calendar c = Calendar.getInstance();
        c.set(rtClock.year+2000, rtClock.mon - 1, rtClock.day, rtClock.hour, rtClock.min, rtClock.sec);
        long ms= c.getTimeInMillis();
        return ms;
    }

    public void setLinuxEpochMs(long ms){
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(ms);
        rtClock = new SRTClock(c);
        date = jday(rtClock);
    }

    public Clock addMinutes(long offset){
        date += offset;
        rtClock = jdate(date);
        return this;
    }

    @Override
    public String toString() {
        return String.format("%02d.%02d.%02d %02d:%02d:%02d",
                rtClock.day, rtClock.mon, rtClock.year, rtClock.hour, rtClock.min, rtClock.sec);
    }

    @Override
    public int compareTo(Clock o) {
//        if(date == o.date)return 0;
//        if(date > o.date)return 1;
//        return -1;
        long diff = date - o.date;
        if(diff > Integer.MAX_VALUE)return Integer.MAX_VALUE;
        if(diff < Integer.MIN_VALUE)return Integer.MIN_VALUE;
        return (int)(date - o.date);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Clock)) return false;

        Clock clock = (Clock) o;

        return date == clock.date && rtClock.equals(clock.rtClock);

    }

    @Override
    public int hashCode() {
        int result = (int) (date ^ (date >>> 32));
        result = 31 * result + rtClock.hashCode();
        return result;
    }
}
