package com.tess.modbus.types;

import com.sun.javaws.exceptions.InvalidArgumentException;

/**
 * Created by TESS on 01.06.2016.
 * Clock with enhancements. Being used for extract data from archive. Divided on four types: the two minutes interval
 * clock, the hour interval clock, the day interval clock, and the month interval clock (HOUR DAY MONTH TWOMIN)
 */
public class ArchiveClock extends Clock {
    public enum ArchiveMode{
        HOUR,
        DAY,
        MONTH,
        TWOMIN
    }
    private ArchiveMode archiveMode;
    private short amdDay = 1, amdHour = 0;

    public ArchiveClock(ArchiveMode archiveMode, Clock clock){
        super(clock);
        this.archiveMode = archiveMode;
        normalizeClock();
    }

    public ArchiveClock(ArchiveMode archiveMode, int amdDay, int amdHour ) {
        super();
        this.archiveMode = archiveMode;
        this.amdDay = (short)amdDay;
        this.amdHour = (short)amdHour;
    }

    public ArchiveClock(ArchiveMode archiveMode, String str){
        super(str);
        this.archiveMode = archiveMode;
        normalizeClock();
//        increment(false);
    }

    public ArchiveClock(ArchiveMode archiveMode) {
        super();
        this.archiveMode = archiveMode;
        normalizeClock();
//        increment(false);
    }

    public ArchiveClock(ArchiveClock ac){
        rtClock = ac.getRtClock();
        date = ac.date;
        archiveMode = ac.archiveMode;
        amdDay = ac.amdDay;
        amdHour = ac.amdHour;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        ArchiveClock ar = new ArchiveClock(this);
        return ar;
    }

    private void normalizeClock(){
        switch (archiveMode){
            case HOUR: date -= (date % 60);break;
            case DAY:  date -= (date % 1440)+ amdHour;break;
            case MONTH:
                if(rtClock.day < amdDay){
                    if(rtClock.mon == 1){rtClock.mon = 12; rtClock.year--;}else rtClock.mon--;
                }
                rtClock.day = amdDay;
                date = jday(rtClock);
                return;
            case TWOMIN:date -= (date %2);break;
        }
        rtClock = jdate(date);
    }
    @Override
    public void setRtClock(SRTClock rtClock) {
        super.setRtClock(rtClock);
        normalizeClock();
    }
    @Override
    public ArchiveClock addMinutes(long offset){
//        date += offset;
//        rtClock = jdate(date);
        super.addMinutes(offset);
        normalizeClock();
        return this;
    }

    public ArchiveClock addUnits(int offset){
        switch(archiveMode){
            case HOUR:addMinutes(offset * 60);break;
            case DAY:addMinutes(offset * 60 *24);break;
            case TWOMIN:addMinutes(offset * 2);break;
            case MONTH:
                boolean direction = offset >=0;
                offset = Math.abs(offset);
                while(offset-- !=0)increment(direction);break;
        }
        return this;
    }

    /**
     * Add or diminish one unit of time
     * @param direction false - diminish, true - add
     * @return itself
     */
    public ArchiveClock increment(boolean direction){
        switch(archiveMode){
            case HOUR:  date += direction?60:-60;break;
            case DAY:   date += direction?1440:-1440;break;
            case MONTH: rtClock.mon = (short)(direction?rtClock.mon + 1: rtClock.mon - 1);
                if(rtClock.mon == 0){rtClock.mon = 12; rtClock.year--;}
                if(rtClock.mon == 13){rtClock.mon = 1; rtClock.year++;}
                date = jday(rtClock);
                return this;
            case TWOMIN:date += direction?2:-2;break;
        }
        rtClock = jdate(date);
        return this;
    }

    public ArchiveMode getArchiveMode() {
        return archiveMode;
    }

    public String toShortedString() {
        String frmt;
        switch (archiveMode){
            case TWOMIN:
            case HOUR:frmt = "%02d.%02d.%02d %02d:%02d";break;
            case DAY:
            case MONTH:frmt = "%02d.%02d.%02d";break;
            default:frmt = "%02d.%02d.%02d %02d:%02d:%02d";
        }
        return String.format(frmt,rtClock.day, rtClock.mon, rtClock.year, rtClock.hour, rtClock.min, rtClock.sec);
    }

    public int getIntervalMinutes(){
        switch(archiveMode){
            case HOUR:  return 60;
            case DAY:   return 1440;
            case TWOMIN:return 2;
            case MONTH:
                ArchiveClock next = this.increment(true);
                return (int)(next.asLong() - asLong());
        }
        return 0;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ArchiveClock)) return false;
        if (!super.equals(o)) return false;

        ArchiveClock that = (ArchiveClock) o;

        return amdDay == that.amdDay && amdHour == that.amdHour && archiveMode == that.archiveMode;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + archiveMode.hashCode();
        result = 31 * result + (int) amdDay;
        result = 31 * result + (int) amdHour;
        return result;
    }

    public void setAmd(int amdDay, int amdHour){
        if(amdDay  > 31 || amdDay < 1)throw new IllegalArgumentException("amdDay: " + amdDay);
        if(amdHour > 23 || amdHour <0)throw new IllegalArgumentException("amdHour:" + amdHour);
        this.amdDay = (short)amdDay;
        this.amdHour = (short)amdHour;
    }
}
