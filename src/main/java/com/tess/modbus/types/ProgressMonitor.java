package com.tess.modbus.types;

/**
 * Created by Admin on 08.08.2018.
 */
public interface ProgressMonitor {
    void init(int start, int end);
    void step(String info);
}
