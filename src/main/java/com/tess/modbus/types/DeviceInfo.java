package com.tess.modbus.types;

import com.tess.modbus.register.RegisteredValueSet;

import java.util.ArrayList;

/**
 * Created by TESS on 01.12.2017.
 * Process Strings such as URG2KM 4.045.i345.RmA#00012 (USB DeviceDescription)
 */
public class DeviceInfo {
    private String type = "Unknown";
    private int softwareVersion = 0;
    private String hardwareVersion = "";
    private String[] details = null;

    public String getRawDeviceDescription() {
        return rawDeviceDescription;
    }

    private String rawDeviceDescription;

    public String getSerialNumberString() {
        return serialNumberString;
    }

    public int getSerialNumber() {
        return serialNumber;
    }

    private String serialNumberString = "";
    private int serialNumber = 0;

    public DeviceInfo(RegisteredValueSet head){
        this(head.getRegisteredValue("DeviceHead:Device").getValue());
        serialNumberString = head.getRegisteredValue("DeviceHead:SerNo").getValue();
        try{serialNumber = Integer.parseInt(getSerialNumberString());}catch(NumberFormatException e){serialNumber = 1;}
    }
    public DeviceInfo(String info){
        if(info == null)return;
        int dies = info.indexOf('#');
        if(dies != -1){
            serialNumberString = info.substring(dies + 1);
            try{serialNumber = Integer.parseInt(getSerialNumberString());}catch(NumberFormatException e){serialNumber = 1;}
            info = info.substring(0, dies);
        }
        rawDeviceDescription = info;
        String[] array = info.split("[\\. ]");
        if(array.length == 0 || array.length > 5)return;
        type = array[0];
        try {
            softwareVersion = Integer.parseInt(array[1] + array[2]);
        } catch (NumberFormatException ignored) {}
        hardwareVersion = array[3];
        StringBuilder sb = new StringBuilder();
        ArrayList<String> arl = new ArrayList<>();
        if(array.length == 5){
            for(char ch: array[4].toCharArray()){
                if(Character.isUpperCase(ch)) {
                    if(sb.length() != 0)arl.add(sb.toString());
                    sb.setLength(0);
                }
                sb.append(ch);
            }
        }
        if(sb.length() != 0)arl.add(sb.toString());
        details = new String[arl.size()];
        for(int i = 0; i < arl.size(); i++)
            details[i] = arl.get(i);
    }

    public String getType() {
        return type;
    }

    public int getSoftwareVersion() {
        return softwareVersion;
    }

    public String getHardwareVersion() {
        return hardwareVersion;
    }

    public String[] getDetails() {
        return details;
    }

    public String getUSBDescription() {
        return rawDeviceDescription + "#" + serialNumberString;
    }
}
