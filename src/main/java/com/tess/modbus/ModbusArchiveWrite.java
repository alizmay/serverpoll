package com.tess.modbus;


import com.tess.dataLinks.Portal;
import com.tess.modbus.register.RegisteredValue;
import com.tess.modbus.register.RegisteredValueSet;
import com.tess.modbus.register.TypeMapping;
import com.tess.modbus.types.ArchiveClock;
import com.tess.modbus.types.Clock;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;

/**
 * Created by Admin on 19.04.2019.
 */
public class ModbusArchiveWrite extends ModbusBasic{

    public ModbusArchiveWrite(Portal portal, int netAddress) {
        super(portal, netAddress);
    }

    private byte[] getHeader(ArchiveClock date){
        Clock.SRTClock rt = date.getRtClock();
        byte expectedOperator = (byte)(65 + date.getArchiveMode().ordinal());
        byte[]b = {netAddress, expectedOperator,
                (byte)rt.min,(byte)rt.hour,(byte)rt.day,(byte)(rt.mon | 0x80),(byte)rt.year};
        return  b;
    }

    public void writeArchive(ArchiveClock clock, RegisteredValueSet in) throws IOException {
        if(in == null || in.size() == 0)throw new RuntimeException("No incoming data");
        byte[] header = getHeader(clock);
        byte[] buffer = new byte[256];
        ByteBuffer byteBuffer = ByteBuffer.wrap(buffer);
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
        int length = 0;
        for(RegisteredValue rv : in){
            int adr = rv.getRegister().getAddress();
            if(adr < 0)continue;
            length += rv.getRegister().getTypeMapping().getSize();
            String value = rv.getValue();
            if(value == null || value.isEmpty()){
                for(int i = 0; i < rv.getRegister().getTypeMapping().getSize() * 2; i++){
                    byteBuffer.put(adr*2 + i, (byte)0xFF);
                }
            }else {
                rv.getRegister().getTypeMapping().transformation(byteBuffer, adr, value);
            }
        }
        if(length > 0)trade(header, Arrays.copyOf(byteBuffer.array(), length*2));
    }
}
