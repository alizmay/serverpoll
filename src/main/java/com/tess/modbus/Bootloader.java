package com.tess.modbus;

import com.tess.dataLinks.Portal;

import java.io.IOException;

/**
 * Created by ali on 19.11.2017.
 */
public class Bootloader {
    private final Portal portal;
    public Bootloader(Portal portal){
        this.portal = portal;
    }
    public static byte[] ascii2Bin(byte[] bt){
        byte[]bytes = new byte[bt.length>>1];
        for(int i = 0;i < bt.length;i+=2){
            String s = ""+(char)bt[i]+(char)bt[i+1];
            int ii = Integer.parseInt(s, 16);
            bytes[i>>1]=(byte)ii;
        }
        return bytes;
    }
    public int load(byte[] bytes, int index) throws IOException {
        int frameSize = (((int) bytes[index] & 0xFF) << 8) | ((int) bytes[index + 1] & 0xFF) + 2;
        byte[] buffer = new byte[frameSize];
        for (int i = 0; i < frameSize; i++) buffer[i] = bytes[index + i];
        byte[] reply = portal.request(buffer);
        if (reply[0] != 0x11) throw new IOException("Wrong reply");
        return index + frameSize;
    }

//    public void    load(byte[] bt) throws IOException {
//        byte[]bytes = new byte[bt.length>>1];
//        for(int i = 0;i < bt.length;i+=2){
//            String s = ""+(char)bt[i]+(char)bt[i+1];
//            int ii = Integer.parseInt(s, 16);
//            bytes[i>>1]=(byte)ii;
//        }
//        int frameSize, bufferSize = bytes.length >>1;
//        for(int index = 0; index < bufferSize; index+=frameSize){
//            frameSize = (((int)bytes[index] & 0xFF) << 8) | ((int)bytes[index + 1] & 0xFF) + 2;
//
//            byte[]buffer = new byte[frameSize];
//            for(int i = 0; i < frameSize; i++)buffer[i] = bytes[index+i];
//            byte[]reply = portal.request(buffer);
//            if(reply[0] != 0x11)throw new IOException("Wrong reply");
//            System.out.println(""+(double)index/bufferSize*100 + "%");
//        }
//
//    }
}
