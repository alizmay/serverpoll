package com.tess.modbus;

import com.tess.dataLinks.Portal;
import com.tess.modbus.exceptions.ModbusException;
import com.tess.modbus.register.Register;
import com.tess.modbus.register.RegisteredValue;
import com.tess.modbus.register.RegisteredValueSet;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created by Admin on 02.04.2019.
 */
public class ModbusCoilRead extends ModbusRegisterRead {
    public final static int MODBUS_READ_COIL_OPERATION = 1;

    public ModbusCoilRead(Portal portal, int netAddress) {
        super(portal, netAddress);
    }

    protected byte[] getHeader(int baseAdr, int length){
        byte[]buffer = new byte[6];
        buffer[0] = super.netAddress;
        buffer[1] = MODBUS_READ_COIL_OPERATION;
        buffer[2] = (byte)((baseAdr >> 8) & 0xFF);
        buffer[3] = (byte)(baseAdr & 0xFF);
        buffer[4] = (byte)((length >> 8) & 0xFF);
        buffer[5] = (byte)(length & 0xFF);
        //expectedBytes = length << 1;
        expectedOperator = buffer[1];
        return buffer;
    }

    @Override
    protected boolean isLengthAsExpected(byte[] buffer) {
        return true;
    }

    @Override
    protected byte[] swapBuffer(byte[] buffer) {
        return Arrays.copyOfRange(buffer, 3, buffer.length);
    }

    public RegisteredValueSet readCoil(TreeSet<Register> set) throws IOException {
//        Iterator<Register>it = set.iterator();
//        while(it.hasNext()){
//            Register r = it.next();
//            if(!r.getTypeMapping().toString().contains("COIL"))it.remove();
//        }
        return readRegister(set);
    }

//    public final static int MODBUS_READ_COIL_OPERATION = 1;
//    public ModbusCoilRead(Portal portal, int netAddress) {
//        super(portal, netAddress);
//    }
//
//
//
//    RegisteredValueSet readData(byte[] header, Set<Register> in) throws IOException {
//        byte buffer[];
//        try {
//            buffer = trade(header, null);
//        } catch (ModbusException e) {
//            throw new IOException(e.getFlow1StringMessage());
//        }
//        if(buffer == null)throw new IOException("No answer");
//        ByteBuffer byteBuffer = ByteBuffer.wrap(buffer);
//        byteBuffer = byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
//        RegisteredValueSet result = new RegisteredValueSet();
//
//        Iterator<Register> it = in.iterator();
//        Register r = it.next();
//        while(r.getAddress() < 0 && it.hasNext()){r = it.next();}
//        if(r.getAddress() < 0)return result;
//        int offset = r.getAddress()*2 - 3;
//
//        do{
//            int adr = r.getAddress()*2 - offset;
//            if(adr >= 0) {
//                adr = r.getAddress() * 2 - offset;
//                String value = r.getTypeMapping().transformation(byteBuffer, adr);
//                result.add(new RegisteredValue(r, value));
//            }
//            if(!it.hasNext())break;
//            r = it.next();
//        }while(true);
//        return result;
//    }
//
//    public void readCoil(TreeSet<Register> set) throws IOException {
//        if(set == null || set.size() == 0 || netAddress < 1)throw new IOException("Incoming data are invalid");
//        Set<Register> localSet = new TreeSet<>();
//        RegisteredValueSet result = new RegisteredValueSet();
//        int startAddress = -1, length = 0;
//        byte[] header = new byte[]{netAddress, MODBUS_READ_COIL_OPERATION, 0, 0, 0, 0};
//        int headerLength = header.length;
//        boolean ini = true;
//        for(Register r:set){
//            if(r.getGroupMark().equals("Archive"))continue;
//            if(!r.getTypeMapping().toString().contains("COIL"))continue;
//            if(ini){
//                startAddress = r.getAddress();
//                if(startAddress < 0){result.add(new RegisteredValue(r, null)); continue;}
//                ini = false;
//                length = r.getTypeMapping().getSize();
//                localSet.add(r);
//                continue;
//            }
//            int adr = r.getAddress();
//            if(adr < 0)continue;
//            int newLength = adr - startAddress + r.getTypeMapping().getSize();
//            if(newLength < (248 - headerLength) / 2){
//                length = newLength;
//                localSet.add(r);
//            }else {
//                for(RegisteredValue rv:readData(getHeader(startAddress, length), localSet))result.add(rv);
////                startAddress = -1;
////                length = 0;
//                localSet.clear();
//                startAddress = r.getAddress();
//                length = r.getTypeMapping().getSize();
//                localSet.add(r);
//            }
//        }
//        if(length > 0) for(RegisteredValue rv:readData(getHeader(startAddress, length), localSet))result.add(rv);
//        return result;
//    }
}
