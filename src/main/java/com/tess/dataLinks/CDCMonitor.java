package com.tess.dataLinks;

import com.sun.jna.Memory;
import com.sun.jna.platform.win32.Guid;
import com.sun.jna.platform.win32.SetupApi;
import com.sun.jna.platform.win32.WinError;
import com.sun.jna.platform.win32.WinNT;
import com.sun.jna.ptr.IntByReference;
import com.tess.modbus.ModbusDeviceID;
import com.tess.modbus.register.RegisteredValueSet;

import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Logger;

import static com.sun.jna.Native.getLastError;
import static com.sun.jna.platform.win32.SetupApi.DIGCF_DEVICEINTERFACE;
import static com.sun.jna.platform.win32.SetupApi.DIGCF_PRESENT;
import static com.sun.jna.platform.win32.SetupApi.GUID_DEVINTERFACE_COMPORT;
//import static com.sun.jna.Native.getLastError;;

/**
 * Created by Admin on 19.11.2018.
 */
public class CDCMonitor {
    static Logger log = Logger.getLogger(MethodHandles.lookup().toString());
    public static final int SPDRP_HARDWAREID = 0x00000001;
    public static final int SPDRP_FRIENDLYNAME = 0x0000000C;
    final static Guid.GUID interfaceClassGuid = new Guid.GUID(new byte[]{(byte)0xa5,(byte)0xdc,(byte)0xbf,0x10, 0x65,0x30, 0x11,
            (byte)0xd2, (byte)0x90, 0x1f, 0x00, (byte)0xc0, 0x4f, (byte)0xb9, 0x51, (byte)0xed});
    private static int errorStatus;
    private static MonitorUSBAttachmentThread musat = new MonitorUSBAttachmentThread(0, 0);
    private static AtomicBoolean connected = new AtomicBoolean(false);
    private static ComRTU comRTU = null;

    public synchronized ComRTU getComRTU() {
        return comRTU;
    }


    public boolean isConnected() {
        return connected.get();
    }

//    private static class DEVPROPKEY{
//        public Guid.GUID fmtid = new Guid.GUID(new byte[]{(byte)0x8c, 0x7e, (byte)0xd2, 0x06, 0x3f,(byte) 0x8a, 0x48, 0x27,
//                (byte)0xb3, (byte)0xab,(byte)0xae, (byte)0x9e, 0x1f, (byte)0xae, (byte)0xfc, 0x6c});
//        public int pid = 2;
//    }
//    final static DEVPROPKEY devPropKeyContainerID = new DEVPROPKEY();
//
//    public static void main(String args[]) {
//        //System.out.println(checkIfPresentAnfGetCOMString((short)0x04D8, (short)0x000A));
//
//    }



    static int getErrorStatus() {
        return errorStatus;
    }

    private static class ComPortName{
        String devicePort;
        String friendlyName;
    }


    public static String checkUsbDeviceAttachment(int USBVendorID, int USBProductID){
        String comString = null;
        String deviceIDToFind = String.format("VID_%04X&PID_%04X", USBVendorID, USBProductID);
        List<ComPortName> res = makeUpComPortNames(interfaceClassGuid);
        for(ComPortName cpn : res)
            if(cpn.devicePort.contains(deviceIDToFind)){
                int indexComStringStart = cpn.friendlyName.indexOf("COM");
                int indexComStringEnd   = cpn.friendlyName.indexOf(")");
                if(indexComStringStart < indexComStringEnd && indexComStringEnd <= cpn.friendlyName.length()) {
                    comString = cpn.friendlyName.substring(indexComStringStart, indexComStringEnd);
                    return comString;
                }
            }
        return comString;
    }

    public static List<ComPortName> getComPortsNames(){
        return makeUpComPortNames(GUID_DEVINTERFACE_COMPORT);
    }

    //public static String checkIfPresentAnfGetCOMString(short USBVendorID, short USBProductID){
    public static List<ComPortName> makeUpComPortNames(Guid.GUID guid){
//        String deviceIDToFind = String.format("VID_%04X&PID_%04X", USBVendorID, USBProductID);
        String COMString = null;
        int interfaceIndex = 0;
        IntByReference dwRegType = new IntByReference();
        IntByReference dwRegSize = new IntByReference();
        SetupApi.SP_DEVICE_INTERFACE_DATA interfaceDataStructure = new SetupApi.SP_DEVICE_INTERFACE_DATA();
        SetupApi.SP_DEVINFO_DATA devInfoData = new SetupApi.SP_DEVICE_INTERFACE_DATA.ByReference();
        List<ComPortName>result = new ArrayList<>();
        WinNT.HANDLE deviceInfoTable = SetupApi.INSTANCE.SetupDiGetClassDevs(guid, null, null,
                DIGCF_PRESENT | DIGCF_DEVICEINTERFACE);
        errorStatus = getLastError();

        while(true){
            SetupApi.INSTANCE.SetupDiEnumDeviceInterfaces(deviceInfoTable, null, guid, interfaceIndex, interfaceDataStructure);
            errorStatus = getLastError();
            if(errorStatus != 0) {
                if (errorStatus == WinError.ERROR_NO_MORE_ITEMS) {
                    SetupApi.INSTANCE.SetupDiDestroyDeviceInfoList(deviceInfoTable);
                    break;
                } else throw new RuntimeException("WinError: " + errorStatus);
            }
            SetupApi.INSTANCE.SetupDiEnumDeviceInfo(deviceInfoTable, interfaceIndex, devInfoData);
            SetupApi.INSTANCE.SetupDiGetDeviceRegistryProperty(deviceInfoTable, devInfoData, SPDRP_HARDWAREID, dwRegType,
                    null, 0, dwRegSize);
            Memory propertyValueBuffer = new Memory(dwRegSize.getValue());
            SetupApi.INSTANCE.SetupDiGetDeviceRegistryProperty(deviceInfoTable, devInfoData, SPDRP_HARDWAREID, dwRegType,
                    propertyValueBuffer, dwRegSize.getValue(), null);
            String deviceIDFromRegistry = propertyValueBuffer.getWideString(0);
            ComPortName cpn = new ComPortName();
            cpn.devicePort = deviceIDFromRegistry;
            SetupApi.INSTANCE.SetupDiGetDeviceRegistryProperty(deviceInfoTable, devInfoData, SPDRP_FRIENDLYNAME,
                    dwRegType, null, 0, dwRegSize);
            propertyValueBuffer = new Memory(dwRegSize.getValue());
            SetupApi.INSTANCE.SetupDiGetDeviceRegistryProperty(deviceInfoTable, devInfoData, SPDRP_FRIENDLYNAME,
                    dwRegType, propertyValueBuffer, dwRegSize.getValue(), null);
            String friendlyNameString = propertyValueBuffer.getWideString(0);
            if(friendlyNameString.contains("(COM")){
                cpn.friendlyName = friendlyNameString;
                result.add(cpn);
            }
//            System.out.println(deviceIDFromRegistry);
            //propertyValueBuffer.finalize();?

//            boolean matchFound = deviceIDFromRegistry.contains(deviceIDToFind);
//            if(true/*matchFound*/){
//                SetupApi.INSTANCE.SetupDiGetDeviceRegistryProperty(deviceInfoTable, devInfoData, SPDRP_FRIENDLYNAME,
//                        dwRegType, null, 0, dwRegSize);
//
//                propertyValueBuffer = new Memory(dwRegSize.getValue());
//                SetupApi.INSTANCE.SetupDiGetDeviceRegistryProperty(deviceInfoTable, devInfoData, SPDRP_FRIENDLYNAME,
//                        dwRegType, propertyValueBuffer, dwRegSize.getValue(), null);
//                String friendlyNameString = propertyValueBuffer.getWideString(0);
////                for(int i =0; i < propertyValueBuffer.size(); i++)
////                    System.out.print(Character.valueOf((char)propertyValueBuffer.getByte(i)));
//                System.out.println(friendlyNameString);
//                if(friendlyNameString.contains("(COM") && friendlyNameString.contains(")")){
//                    int indexComStringStart = friendlyNameString.indexOf("COM");
//                    int indexComStringEnd   = friendlyNameString.indexOf(")");
//                    if(indexComStringStart < indexComStringEnd && indexComStringEnd <= friendlyNameString.length()){
//                        COMString = friendlyNameString.substring(indexComStringStart, indexComStringEnd);
//                        SetupApi.INSTANCE.SetupDiDestroyDeviceInfoList(deviceInfoTable);
//                        return COMString;
//                    }
//                }
//            }
            interfaceIndex++;
        }

        return result;
    }

    public static void start(){
        musat.start();
    }

    public static void stop() throws InterruptedException {
        musat.interrupt();
        musat.join();
    }

    private static class MonitorUSBAttachmentThread extends Thread{

        public int usbVendorID = 0x4D8;
        public int usbProductID = 0xA;
        private int delay = 1;

        public MonitorUSBAttachmentThread(int vendorId, int productId) {
            super("USB Attach Monitor");
            if((vendorId != 0) && (productId != 0)){
                usbProductID = productId; usbVendorID = vendorId;
            }
        }

        public MonitorUSBAttachmentThread(int delay) {
            super("USB Attach Monitor");
            this.delay = delay;
        }

        private RegisteredValueSet validation(String comPort) {
            log.info(String.format("New USB device(VID_%04X&PID_%04X) is discovered", usbVendorID, usbProductID));
            ComRTU cr = new ComRTU();
            RegisteredValueSet rvs = null;
            try {
                cr.open(comPort, "tries=1,ms=2000");
                ModbusDeviceID mid = new ModbusDeviceID(cr, 1);
                while(true) {
                    try {
                        rvs = mid.readID();
                        System.out.println(rvs.getRegisteredValue("Device:Device"));

                    } catch (IOException e) {

                    }
                }
            } catch (IOException e) {
                cr.close();
                log.info("Device not support the modbus protocol or has NetAddress differ from 1");
                throw new RuntimeException("Device not support the modbus protocol or has NetAddress differ from 1");
            }
//            if(rvs == null)log.info("Device not support the modbus protocol or has NetAddress differ from 1");
//            else{
//                String no = rvs.getRegisteredValue("DeviceHead:SerNo").getValue();
//                String device = rvs.getRegisteredValue("DeviceHead:Device").getValue();
//                log.fine(device + " #" + no);
//                comRTU = cr;
//                connected.set(true);
//                interrupt();
//            }
//            return rvs;
        }

        @Override
        public void run() {
            String comPort;
            log.info("USB attachment monitor is launched");
            try {
                while(!isInterrupted()){
                    comPort = checkUsbDeviceAttachment(usbVendorID, usbProductID);
                    if(comPort != null){
                        log.fine(String.format("COM port (%s)", comPort));
                        validation(comPort); continue;
                    }
                    Thread.sleep(delay);
                }
            } catch (Exception e) {
                //TODO printStack to Log
                e.printStackTrace();
            }finally {

            }
        }
    }
}
