package com.tess.dataLinks;

import com.tess.modbus.wrappers.Wrapper;
import jssc.SerialPortException;

import java.io.IOException;

/**
 * Created by TESS on 18.05.2016.
 * Interface for extracting data by physical interfaces from actual devices
 */
public interface Portal extends AutoCloseable, PortalListener, ILogBuilder{
    void open(String path, String parameters) throws IOException;
    void close();
    byte[] request(byte[]buffer) throws IOException;
    Wrapper getWrapper();
}
