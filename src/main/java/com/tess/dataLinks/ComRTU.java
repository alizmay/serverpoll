package com.tess.dataLinks;

import com.tess.CfgRoot;
import com.tess.modbus.wrappers.Wrapper;
import com.tess.modbus.wrappers.WrapperASCII;
import com.tess.modbus.wrappers.WrapperRTU;
import jssc.SerialPort;
import jssc.SerialPortException;
import jssc.SerialPortTimeoutException;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * Created by TESS on 09.06.2016.
 * Portal for COMMUNICATIONS ports of PC with RTU type
 */
public class ComRTU extends AbstractPortal implements PortalListener {
    protected static String PROPERTY_PATH = "\\Config\\SerialPortConfig.properties";;
    protected SerialPort serialPort;

    public ComRTU() {
        super(new WrapperRTU());
    }

    public ComRTU(Wrapper wrapper){
        super(wrapper);
    }

    @Override
    public void open(String path, String parameters) throws IOException {
        if(serialPort != null)close();
        int baudrate;
        Map<String, String> settings;

        if (parameters != null && !parameters.isEmpty() && path != null)settings = parseFragment(parameters);
            else settings = parseComProperties();//(System.getProperty("user.dir")+PROPERTY_PATH);
        try {baudrate = Integer.parseInt(settings.get("baud")); } catch (Exception e) {baudrate = 9600;}
        try {timeout = Integer.parseInt(settings.get("ms"));}catch(Exception e){timeout = 1000;}
        try {attempts = Integer.parseInt(settings.get("tries"));}catch(Exception e){attempts = 1;}
        if(path == null || path.isEmpty()) path = settings.get("path");

        try {
            localTimeout = 2;
            serialPort = new SerialPort(path);
            serialPort.openPort();
            serialPort.setParams(baudrate,
                SerialPort.DATABITS_8,
                SerialPort.STOPBITS_1,
                SerialPort.PARITY_NONE);
            serialPort.setFlowControlMode(SerialPort.FLOWCONTROL_NONE);
            if(logBuilder!= null)logBuilder.append("Последовательный порт " + path + " открыт успешно\n");
        } catch (SerialPortException e) {
            throw new IOException(e.getMessage());
        }

    }

    @Override
    public void close(){
        try {serialPort.closePort();} catch (SerialPortException ignored) {}
    }

    @Override
    protected void write(byte[] buffer) throws IOException {
        try {
			serialPort.purgePort(SerialPort.PURGE_RXCLEAR | SerialPort.PURGE_TXCLEAR);
            serialPort.writeBytes(buffer);
        } catch (SerialPortException e) {
            throw new IOException(e.getMessage());
        }
    }

    @SuppressWarnings("InfiniteLoopStatement")
    @Override
    protected int append(byte[] buffer, int length) throws IOException {
        try {
            Thread.sleep(localTimeout);
            while(true) {
                byte[] bytes = serialPort.readBytes(1, 1);
                buffer[length] = bytes[0];
                length++;
            }

        } catch (SerialPortException e) {
            throw new IOException(e.getMessage());
        } catch (SerialPortTimeoutException ignored) {
        } catch (InterruptedException igniored) {
        }
        return length;
    }

    public static Map<String, String> parseComProperties() {
        Map<String, String> prop;
        try {
            prop = parseProperties(CfgRoot.PATH+PROPERTY_PATH);
        } catch (IOException e) {prop = new HashMap<>();        }
        if(prop.get("path")==null)prop.put("path","COM1");
        if(!isInteger(prop.get("baud")))prop.put("baud","9600");
        if(!isInteger(prop.get("tries")))prop.put("tries","2");
        if(!isInteger(prop.get("ms")))prop.put("ms","2000");
        return prop;
    }


    public static void storeComProperties(Map<String, String>m){
        if(m == null)return;
        Properties prop = new Properties();
        if(m.get("path")!=null)prop.setProperty("path",m.get("path"));
        else prop.setProperty("path", "COM1");
        if(isInteger(m.get("baud")))prop.setProperty("baud",m.get("baud"));
        else prop.setProperty("baud", "9600");
        if(isInteger(m.get("tries")))prop.setProperty("tries", m.get("tries"));
        else prop.setProperty("tries", "2");
        if(isInteger(m.get("ms")))prop.setProperty("ms", m.get("ms"));
        else prop.setProperty("ms", "2000");
        try(OutputStream os = new FileOutputStream(CfgRoot.PATH+PROPERTY_PATH)) {
            prop.store(os, PROPERTY_PATH);
        } catch (Exception e) {
            log.severe(getTrackStackString(e));
        }

    }

    @Override
    public int bytesReady() throws IOException {
        try {
            if(serialPort.isOpened()== false)throw new IOException("Serial port is not opened");
            return serialPort.getInputBufferBytesCount();
        } catch (SerialPortException e) {
            throw new IOException(e);
        }
    }

    @Override
    public byte[] read(int bytes) throws IOException{
        try{
            if(serialPort.isOpened()== false)throw new IOException("Serial port is not opened");
            byte[] result = serialPort.readBytes();
            return result;
        } catch (SerialPortException e) {
            throw new IOException(e);
        }
    }
}
