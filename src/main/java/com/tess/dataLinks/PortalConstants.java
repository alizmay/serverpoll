package com.tess.dataLinks;


/**
 * Created by TESS on 07.08.2017.
 */
public class PortalConstants {
    public final static String COM = "com";
    public final static String MODEM = "modem";
    public final static String TELNET = "tcp";
    public final static String TCPACCEPTED = "tcpaccepted";
    public final static String USBACCEPTED = "usbaccepted";
	
    //Modbus modes
    public final static String RTU = "rtu";
    public final static String ASCII = "ascii";
    public final static String TCP = "tcp";


}
