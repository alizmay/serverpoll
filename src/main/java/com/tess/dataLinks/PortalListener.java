package com.tess.dataLinks;

import jssc.SerialPortException;

import java.io.IOException;

/**
 * Created by Admin on 03.04.2019.
 */
public interface PortalListener {
    int bytesReady() throws IOException;
    byte[] read(int bytes)throws IOException;
}
