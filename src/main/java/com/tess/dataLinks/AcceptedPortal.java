package com.tess.dataLinks;

import com.tess.dataLinks.InetPortal;

import java.io.IOException;
import java.net.Socket;

/**
 * Former ServerPortal. Used when ServerSocket create Socket
 * Created by TESS on 06.06.2017.
 */
public class AcceptedPortal extends InetPortal {
    public AcceptedPortal(String wrapper, Socket s) throws IOException {
        super(wrapper);
        socket = s;
        inputStream = socket.getInputStream();
        outputStream = socket.getOutputStream();
        socket.setSoTimeout(1);
        loadDefaultParams();
    }

    @Override
    public void open(String path, String parameters) throws IOException {
    }
}
