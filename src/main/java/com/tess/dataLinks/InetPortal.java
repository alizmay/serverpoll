package com.tess.dataLinks;

import com.tess.CfgRoot;
import com.tess.modbus.wrappers.Wrapper;
import com.tess.modbus.wrappers.WrapperASCII;
import com.tess.modbus.wrappers.WrapperRTU;
import com.tess.modbus.wrappers.WrapperSRTU;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * Created by TESS on 10.06.2016.
 * Extracting data from devices through internet socket
 */
public class InetPortal extends AbstractPortal implements PortalListener {
    static final String PROPERTY_PATH = "\\Config\\SocketPortConfig.properties" ;
    Socket socket;
    InputStream inputStream;
    OutputStream outputStream;
    String inetPath;
    public InetPortal(){
        super(new WrapperRTU());
    } //TODO CONTROLLED BUG
    public InetPortal(String wrapper) {
        super(wrapper.equals("rtu")?new WrapperRTU():wrapper.equals("ascii")?new WrapperASCII():null);
    }//TODO CONTROLLED BUG
    public InetPortal(Wrapper wrapper){
        super(wrapper);
    }

    public InputStream getInputStream() {
        return inputStream;
    }

    public OutputStream getOutputStream() {
        return outputStream;
    }

    void loadDefaultParams() throws IOException {
        Map<String, String> settings;
        settings = parseProperties();
        timeout = Integer.parseInt(settings.get("ms"));
        attempts = Integer.parseInt(settings.get("tries"));
        inetPath = settings.get("path");
    }
    @Override
    public void open(String path, String parameters) throws IOException {
        if(socket != null)close();
        Map<String, String> settings;
        try {
            if (parameters != null && !parameters.isEmpty()) {
                settings = parseFragment(parameters);
                timeout = Integer.parseInt(settings.get("ms"));
                attempts = Integer.parseInt(settings.get("tries"));
            } else {
                loadDefaultParams();
//                settings = parseProperties(System.getProperty("user.dir") + PROPERTY_PATH);
//                timeout = Integer.parseInt(settings.getRegister("timeout"));
//                attempts = Integer.parseInt(settings.getRegister("attempts"));
            }
        }catch(NumberFormatException e){ timeout = 5000; attempts = 2;}
        if(path != null) {
            try {
                String host = path.split(":")[0];
                int port = Integer.parseInt(path.split(":")[1]);
                socket = new Socket();
                InetSocketAddress isa = new InetSocketAddress(host, port);
                socket.connect(isa/*new InetSocketAddress(host, port)*/, timeout);
                inputStream = socket.getInputStream();
                outputStream = socket.getOutputStream();
                socket.setSoTimeout(1);
                Thread.sleep(2000);
            } catch (Exception e) {
                if (socket != null) socket.close();
                throw new IOException(e.getMessage());
            }
        }


    }

    @Override
    public void close(){
        try {
            if (outputStream != null) {
                outputStream.write(new String("CLOSED").getBytes());
                outputStream.close();
            }
            if (inputStream != null) inputStream.close();
            if (socket != null) socket.close();
        }catch(Exception ignored){}
    }

    @Override
    protected void write(byte[] buffer) throws IOException {
        //inputStream.reset();
        //while(inputStream.available()!=0)inputStream.read();
        outputStream.write(buffer);
    }

    @Override
    protected int append(byte[] buffer, int length) throws IOException {
        int len;
        try{
//            Thread.sleep(1);
            len = inputStream.read(buffer, length, 256 );
  //          len = inputStream.available();
            if(len != 0){
                //inputStream.readRegister(buffer, length, len);
                length += len;
            }
        }
        catch (IOException ignored){}
        return length;
    }

    public Socket getSocket(){
        return socket;
    }

    public static Map<String, String> parseProperties() {
        Map<String, String> prop;
        try {
            prop = parseProperties(CfgRoot.PATH+PROPERTY_PATH);
        } catch (IOException e) { log.fine("Property missed:" + e);prop = new HashMap<>();}
        if(prop.get("path")==null)prop.put("path","127.0.0.1:502");
        if(!isInteger(prop.get("tries")))prop.put("tries","2");
        if(!isInteger(prop.get("ms")))prop.put("ms","2000");
        return prop;
    }

    public static void storeProperties(Map<String, String> m){
        if(m == null)return;
        Properties prop = new Properties();
        if(m.get("path")==null)prop.setProperty("path","127.0.0.1:502");
        else prop.setProperty("path",m.get("path"));
        if(!isInteger(m.get("tries")))prop.setProperty("tries","2");
        else prop.setProperty("tries", m.get("tries"));
        if(!isInteger(m.get("ms")))prop.setProperty("ms","2000");
        else prop.setProperty("ms", m.get("ms"));
        try(OutputStream os = new FileOutputStream(CfgRoot.PATH+PROPERTY_PATH)) {
            prop.store(os, "InetPortal settings");
        } catch (Exception e) {
            log.severe(getTrackStackString(e));
        }
    }

    @Override
    public int bytesReady() throws IOException {
        return inputStream.available();
    }

    @Override
    public byte[] read(int bytes) throws IOException {
        return new byte[0];
    }
}
