package com.tess.dataLinks;

import com.tess.modbus.wrappers.Wrapper;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.invoke.MethodHandles;
import java.net.URI;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.*;
import java.util.logging.Logger;

/**
 * Created by TESS on 09.06.2016.
 * Base class for portals
 */
public abstract class AbstractPortal implements Portal {
    static Logger log = Logger.getLogger(MethodHandles.lookup().toString());
    protected Wrapper wrapper;
    protected int attempts;
    protected int timeout;
    protected int localTimeout;
    URI uri;
    protected StringBuilder logBuilder = null;

//    private static String getHostLocal(URI uri){
//        String path = uri.toString();
//        int start = path.indexOf("//");
//        int finish = path.indexOf("?");
//        try {
//           URI u = new URI("com.rtu:/10.0.0.2:502?1");
//            String pp = u.getPath();
//            System.out.println(u.toString()+ " " +pp);
//            u = new URI("com.rtu:/+79176615999?1");
//            System.out.println(u.toString());
//        } catch (URISyntaxException e) {
//            e.printStackTrace();
//        }
//        return path.substring(start,finish);
//    }


    /**
     * com.rtu:\\COM2?1#baud=9600,tries=2,ms=2000
     * Transport = com, Type = rtu, Path = COM2, NetAddress(Byte) = 1, bauds=9600, tries=2, ms=2000
     * @param uri URI (as I see it)
     * @return HashMap with netaddress, transport, type, path
     * @throws URISyntaxException
     */
    public static Map<String, String> parseUri(URI uri) throws URISyntaxException{
        if(uri == null)throw new NullPointerException("URI is empty");
        if(!uri.toString().contains("?"))throw new URISyntaxException(uri.toString(),"NetAddress required");
        Map<String, String>m = new HashMap<>();
        String netAddress;
        String scheme, type, pathLocal, parameters;
        scheme = uri.getScheme().split("\\.")[0].toUpperCase();
        type = uri.getScheme().split("\\.")[1].toUpperCase();
        pathLocal = uri.getPath();
        if(pathLocal!=null && !pathLocal.isEmpty())pathLocal = pathLocal.substring(1);
        else {
            pathLocal = uri.getHost();
            if(pathLocal == null)throw new URISyntaxException(pathLocal, "Address is not available");
        }
        try{
            netAddress =uri.getQuery();
            parameters = uri.getFragment();
        }catch (Exception e){throw new URISyntaxException(uri.getFragment(), e.getMessage());}
        m.put("netaddress", netAddress);
        m.put("transport", scheme);
        m.put("type", type);
        m.put("path", pathLocal);
        if(parameters != null)m.putAll(parseFragment(parameters));
        return m;
    }


//    static Map<String, String> parseFragment (String parameters){
//        Map<String, String> m = new HashMap<>();
//        String[] sss = parameters.split(",");
//        for(String s:sss){
//            int i;
//            if(s.isEmpty())continue;
//            for(i = 0;i < s.length();i++)
//                if(s.charAt(i)<'0' || s.charAt(i)>'9' || s.charAt(i) == ' ')break;
//            String value = s.substring(0, i);
//            String key   = s.substring(i, s.length());
//            if(i != 0)m.put(key, value);
//        }
//        return m;
//    }

    protected static Map<String, String>parseFragment(String parameters){
        Map<String, String> m = new HashMap<>();
        String[]sss = parameters.split(",");
        if(sss.length == 0)return m;
        for(String s:sss){
            String[] couple = s.split("=");
            if(couple.length == 1)m.put(couple[0].trim(), null);
                else m.put(couple[0].trim(), couple[1].trim());
        }
        return m;
    }

    protected static Map<String, String> parseProperties(String path) throws IOException {
        Map<String, String>m = new HashMap<>();
        FileInputStream fis = new FileInputStream(path);
            Properties prop = new Properties();
            prop.load(fis);
            for(String name:prop.stringPropertyNames())m.put(name, (String)prop.get(name));
        return m;
    }

    static boolean isInteger(String num){
        if(num == null)return false;
        try{
            Integer.parseInt(num);
            return true;
        }catch(NumberFormatException e){return false;}
    }



    public AbstractPortal(Wrapper wrapper) {
        this.wrapper = wrapper;
        localTimeout = 1;
    }

    @Override
    public byte[] request(byte[] buffer) throws IOException {
        if(buffer == null || buffer.length < 2)throw new IOException("No incoming data");
        buffer = wrapper.wrapForWrite(buffer);
        int i = attempts;
        byte[]total = new byte[1500];
        int lenTotal = 0;
        while(i-- > 0){
            try{
                int to = timeout;
                write(buffer);
                log.fine/*Console.out*/("WR: " + deepByteArray(buffer));
                if(logBuilder != null)logBuilder.append("WR: " + deepByteArray(buffer) + '\n');
                int len;
                while(to > 0 ){
                    to -= localTimeout;
                    len = append(total, lenTotal);
                    if(len < 0)throw new IOException("Portal is closed by outer side");
                    if(len == lenTotal)continue;
                    lenTotal = len;
                    byte[]resultBuffer = wrapper.wrapForRead(total, 0, lenTotal);
                    //There was an occasion when CRC16 == 0 but the not whole packet has come. expectedByte > lenTotal condition is to add
                    if(resultBuffer != null)
                    {
                        if((resultBuffer.length & 0x01) == 0 )resultBuffer = Arrays.copyOf(resultBuffer,resultBuffer.length + 1);
                        log.fine("RD: " + deepByteArray(total, 0, lenTotal));
                        if(logBuilder != null)logBuilder.append("RD: " + deepByteArray(total, 0, lenTotal)+ '\n');
                        return resultBuffer;
                    }
                }
            }catch(Exception e){
                throw new IOException(e.getMessage());
            }
            if(lenTotal > 0){
                log.fine("RD: " + deepByteArray(total, 0, lenTotal));
                if(logBuilder != null)logBuilder.append("RD: " + deepByteArray(total, 0, lenTotal)+ '\n');
            }else{
                log.fine("No answer");
                if(logBuilder != null)logBuilder.append("No answer"+ '\n');
            }
            lenTotal = 0;
        }
        throw new IOException("No answer");
    }

    @Override
    public Wrapper getWrapper() {
        return wrapper;
    }

    protected void write(byte[]buffer) throws IOException {
        throw new IOException("write method are not defined");
    }

    protected int append(byte[]buffer, int length) throws IOException{
        throw new IOException("append method are not defined");
    }

    protected static String deepByteArray(byte[]buff) {
        StringBuilder res = new StringBuilder();
        for (byte b : buff)
            res.append(String.format("%02X ", b));
        return res.toString();
    }

    protected static String deepByteArray(byte[]buffer, int offset, int len){
        byte[]subBuffer = new byte[len];
        for(int i = offset, y = 0;len-- > 0;i++, y++)subBuffer[y] = buffer[i];
        return deepByteArray(subBuffer);
    }


    public void setWrapper(Wrapper wrapper) {
        this.wrapper = wrapper;
    }

    @Override
    public void setLogBuilder(StringBuilder logBuilder) {
        this.logBuilder = logBuilder;
    }

    public static String getTrackStackString(Exception e){
        StringWriter sw = new StringWriter();
        e.printStackTrace(new PrintWriter(sw));
        return sw.toString();
    }

}
