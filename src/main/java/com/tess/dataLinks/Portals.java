package com.tess.dataLinks;

import com.tess.modbus.wrappers.Wrapper;
import com.tess.modbus.wrappers.WrapperASCII;
import com.tess.modbus.wrappers.WrapperRTU;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import static com.tess.dataLinks.PortalConstants.*;

/**
 * Created by TESS on 28.11.2017.
 */
public class Portals {

    public static Portal portalFactory(URI uri) throws IOException {
        return portalFactory(uri, null);
    }

    public static Portal portalFactory(URI uri, StringBuilder sb) throws IOException {
        Portal portal = null;
        Map<String, String> m;
        try {
            m = AbstractPortal.parseUri(uri);
            if(m == null || m.isEmpty())throw new IOException("URI is empty or wrong");
        } catch (URISyntaxException e) {
            throw new IOException("Wrong URI", e);
        }
        //this.uri = uri;
        try{
            Byte na = Byte.parseByte(m.get("netaddress"));
            //netAddress = (byte)(na & 0xFF);
            String scheme, type, pathLocal;
            scheme = m.get("transport").toLowerCase();
            type   = m.get("type").toLowerCase();
            pathLocal = m.get("path").toLowerCase();
            Wrapper wrapper;
            switch(type){
                case RTU: wrapper = new WrapperRTU(); break;
                case ASCII: wrapper = new WrapperASCII(); break;
                case TCP: throw new RuntimeException("Not yet implemented");
                default: throw new RuntimeException("Unknown type of modbus");
            }
            switch(scheme){
                case COM:
                    portal = new ComRTU(wrapper);
                    portal.setLogBuilder(sb);
                    portal.open(pathLocal, uri.getFragment());
                    break;
                case MODEM:
                    portal = new ModemPortal(wrapper);
                    portal.setLogBuilder(sb);
                    portal.open(pathLocal, uri.getFragment());
                    break;
                case TELNET:
                    //TODO TCP
                    portal = new InetPortal(wrapper);
                    portal.setLogBuilder(sb);
                    portal.open(pathLocal+ ":" + uri.getPort(), uri.getFragment());
                    break;
                case TCPACCEPTED:  //Here already socket is exist and opened
                    //TODO RTU ASCII TCP
                    break;
                default:throw new IOException("Unknown type of URI has found");
            }
            sb.append(scheme + "\n");
        }catch (Exception e){throw new IOException(e.getLocalizedMessage(), e);}
        return portal;
    }

    public static Byte getNetAddress(URI uri) {
        try {
            String na = AbstractPortal.parseUri(uri).get("netaddress");
            return Byte.parseByte(na);
        } catch (Exception e) {
            return null;
        }
    }

    public static boolean isEqual(URI a1, URI a2){
        try {
            Map<String, String>m1 = AbstractPortal.parseUri(a1);
            Map<String, String>m2 = AbstractPortal.parseUri(a2);
            String p1 = m1.get("transport")+m1.get("type")+m1.get("path");
            String p2 = m2.get("transport")+m2.get("type")+m2.get("path");
            return p1.equals(p2);
        } catch (URISyntaxException e) {
            return false;
        }
    }
}
