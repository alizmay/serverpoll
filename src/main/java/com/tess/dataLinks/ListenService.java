package com.tess.dataLinks;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by Admin on 02.04.2019.
 */
public class ListenService implements Runnable{

    protected Thread thread;
    protected ArrayList<Byte> readBytes = new ArrayList<>();
    protected PortalListener portalListener;
    protected int msSeparator;
    protected Deque<byte[]>messages = new LinkedBlockingDeque<>();

    public ListenService(PortalListener portalListener, int msSeparator) {
        this.portalListener = portalListener;
        this.msSeparator = msSeparator;
        thread = new Thread(this, "ListenService");
    }

    public void start(){
        thread.start();
    }

    public void stop(){
        thread.interrupt();
        while(thread.isAlive());
    }

    private void saveMessage(List<Byte> l){
        byte[] m = new byte[l.size()];
        for (int i = 0; i < l.size(); i++) m[i] = l.get(i);
        l.clear();
        messages.add(m);
    }

    public byte[] getMessages(){
        byte[] r = messages.pollFirst();
        return r;
    }

    public boolean isDataReady(){
        return messages.size() > 0;
    }

    @Override
    public void run() {
        try {
            long timestamp = 0;
            while(!Thread.currentThread().isInterrupted()){
                int incnt = portalListener.bytesReady();
                if(incnt > 0){
                    timestamp = System.currentTimeMillis();
                    for(byte b : portalListener.read(incnt))
                        readBytes.add(b);
                }else{
                    if(timestamp != 0){
                        long now = System.currentTimeMillis();
                        if(now - timestamp > msSeparator){
                            timestamp = 0;
                            saveMessage(readBytes);
                        }
                    }
                }

            }
        } catch (IOException e) {
            System.out.println("Listen thread:");
            e.printStackTrace();
        }
    }

}
