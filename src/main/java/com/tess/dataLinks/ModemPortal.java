package com.tess.dataLinks;

import com.tess.CfgRoot;
import com.tess.modbus.wrappers.Wrapper;
import jssc.SerialPortException;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

/**
 * Created by Admin on 11.11.2019.
 */
public class ModemPortal extends ComRTU {
    public final static String PROPERTY_PATH = "\\Config\\ModemConfig.properties";
    static final String[] negativeConnect = {
        "NO DIALTONE", "6", "BUSY", "7", "NO CARRIER", "3"
    };
    public final static String defaultInitString = "ATQ0V1E0&D2";
    private static String initialModemString = defaultInitString;
    final static int simpleTimeoutMs = 5000;
    private String telephoneNumber;
    private boolean connected = false;

    public ModemPortal(Wrapper wrapper){
        super(wrapper);
    }

    @Override
    public void open(String path, String parameters) throws IOException {
        Map<String, String> prop;
        if (parameters != null && !parameters.isEmpty() && path != null) prop = parseFragment(parameters);
        else prop = parseModProperties();//CfgRoot.PATH + PROPERTY_PATH);
        initialModemString = prop.get("is");
        telephoneNumber = prop.get("num");
        super.open(path, parameters);
        try {
            if (//(!writeCommand("AT")) ||
                    (!writeCommand("AT&F")) ||
                    (!writeCommand(initialModemString)) ||
                    (!connect())) {
                close();
                throw new IOException("Modem doesn't answer or an unknown command was applied");
            }
        }catch(SerialPortException e){
            close();
            throw new IOException(e);
        }
    }

    public static Map<String, String> parseModProperties(){
        Map<String, String> prop;
        try {
            prop = parseProperties(CfgRoot.PATH+PROPERTY_PATH);
        } catch (IOException e) {prop = new HashMap<>();        }
        if(prop.get("path")==null)prop.put("path","COM1");
        if(!isInteger(prop.get("baud")))prop.put("baud","115200");
        if(!isInteger(prop.get("tries")))prop.put("tries","2");
        if(!isInteger(prop.get("ms")))prop.put("ms","2000");
        if(prop.get("is")== null)prop.put("is",initialModemString);
        return prop;
    }


    public boolean writeCommand(String comm) throws SerialPortException {
        return writeCommand(comm, simpleTimeoutMs, new String[]{"OK","0"}, new String[]{"ERROR","4"});
    }


    public boolean connect() throws SerialPortException {
        if(writeCommand("ATD" + telephoneNumber, timeout * 2,
                new String[]{"CONNECT*"},
                negativeConnect) == true){
            connected = true;
        }else connected = false;
        return connected;
    }

    public boolean writeCommand(String comm, int msTo, String[]positive, String[] negative)
            throws SerialPortException {
        assert(serialPort.isOpened() == true);
        boolean resultReady = false;
        boolean result = false;
        comm =comm + "\r\n";
        serialPort.writeBytes(comm.getBytes());
        if(logBuilder != null)logBuilder.append(comm);
        StringBuilder astr = new StringBuilder();
        while(msTo-- > 0) {
            byte[] bb = serialPort.readBytes();
            if(bb != null) {
                if (logBuilder != null) logBuilder.append(new String(bb));
                astr.append(new String(bb));
                if(!resultReady ) {
                    String[] answers = astr.toString().split("[\r\n]");

                    for (String a : answers) {
                        if (a.isEmpty()) continue;
                        for (String n : positive) {
                            if (n.contains("*")) {
                                n = n.replace('*', ' ').trim();
                                if (a.toUpperCase().contains(n)) {
                                    result = true;
                                    msTo = 10;
                                    resultReady = true;
                                    break;
                                }
                            } else if (a.toUpperCase().equals(n)) {
                                result = true;
                                resultReady = true;
                                msTo = 10;
                                break;
                            }
                        }
                        if(!resultReady) {
                            for (String n : negative) {
                                if (n.contains("*")) {
                                    n.replace('*', ' ');
                                    if (a.toUpperCase().contains(n)) {
                                        result = false;
                                        resultReady = true;
                                        msTo = 10;
                                        break;
                                    }
                                } else if (a.toUpperCase().equals(n)) {
                                    result = false;
                                    resultReady = true;
                                    msTo = 10;
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            try { TimeUnit.MILLISECONDS.sleep(1);} catch (InterruptedException e) {return false;}
        }
        if(!resultReady)return false;
        return result;
    }

//    public boolean connectCommand(int msTo) throws SerialPortException {
//        assert(serialPort.isOpened() == true);
//
//        String comm = "ATD" + telephoneNumber + "\r\n";
//        if(logBuilder != null)logBuilder.append(comm.getBytes());
//        serialPort.writeBytes(comm.getBytes());
//        StringBuilder astr = new StringBuilder();
//        while(msTo-- > 0) {
//            byte[] bb = serialPort.readBytes();
//            if(logBuilder != null)logBuilder.append(bb);
//            astr.append(bb);
//            String[] answers = astr.toString().split("[\r\n]");
//            for (String a : answers) {
//                for(String n : negativeConnect)
//                    if(a.toUpperCase().equals(n))return false;
//                if(a.toUpperCase().contains("CONNECT"))return true;
//            }
//            try { TimeUnit.MILLISECONDS.sleep(msTo);} catch (InterruptedException e) {return false;}
//        }
//        return true;
//    }

    @Override
    public void close() {
        try{
            TimeUnit.MILLISECONDS.sleep(1250);
            serialPort.writeBytes("+++".getBytes());
            TimeUnit.MILLISECONDS.sleep(1250);
            if(writeCommand("ATH0") == true) connected = false;
            else connected = true;
            super.close();
        }catch(Exception e){
            return;
        }
    }

    public static void storeModProperties(Map<String, String>m){
        if(m == null)return;
        Properties prop = new Properties();
        if(m.get("path")!=null)prop.setProperty("path",m.get("path"));
        else prop.setProperty("path", "COM1");
        if(isInteger(m.get("baud")))prop.setProperty("baud",m.get("baud"));
        else prop.setProperty("baud", "115200");
        if(isInteger(m.get("tries")))prop.setProperty("tries", m.get("tries"));
        else prop.setProperty("tries", "2");
        if(isInteger(m.get("ms")))prop.setProperty("ms", m.get("ms"));
        else prop.setProperty("ms", "2000");
        if(isInteger(m.get("is")))prop.setProperty("is", m.get("is"));
        else prop.setProperty("is", defaultInitString);
        if(isInteger(m.get("num")))prop.setProperty("num", m.get("num"));
        else prop.setProperty("num", "0");
        try(OutputStream os = new FileOutputStream(CfgRoot.PATH+PROPERTY_PATH)) {
            prop.store(os, PROPERTY_PATH);
        } catch (Exception e) {
            log.severe(getTrackStackString(e));
        }

    }


}
