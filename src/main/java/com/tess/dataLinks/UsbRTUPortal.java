package com.tess.dataLinks;

import com.ftdichip.ftd2xx.*;

import com.tess.CfgRoot;
import com.tess.modbus.ModbusDeviceID;
import com.tess.modbus.wrappers.WrapperRTU;
import javafx.scene.paint.Color;


import javax.sound.sampled.LineUnavailableException;
import java.io.IOException;

import java.util.Map;

/**
 * Created by ali on 18.11.2017.
 */
public class UsbRTUPortal extends AbstractPortal {
    private final static String PROPERTY_PATH = "\\Config\\SerialPortConfig.properties";

    private Device device = null;


    private static boolean isOneConnectionExist = false;
    public static UsbRTUPortal getUsbRTUPortal(){
        try {
            if(isOneConnectionExist)throw new RuntimeException("Just one USB connection is being supported yet");
            Device[] devices = com.ftdichip.ftd2xx.Service.listDevices();
            if(devices.length == 0)return null;
            if(devices.length > 1)throw new RuntimeException("Two USB connection is detected");
            isOneConnectionExist = true;
            return new UsbRTUPortal(devices[0]);
        } catch (FTD2xxException e) {
            throw new RuntimeException("FTDI Driver problem", e);
        }
    }

    public static boolean isConnected() throws InterruptedException {
        while(true) {
            try {
                Device[] devices;
                devices = Service.listDevices();
                if (devices.length == 0) return false;
                return true;
            }catch (FTD2xxException e) {
                e.printStackTrace();
                return false;
            }
            catch(IllegalStateException ignored) {
                Thread.sleep(1000);
            }


//                if(e instanceof FTD2xxException){
//                    e.getFlow1StringMessage().equals("Device is not open");
//                }else {
//                    e.printStackTrace();
//                    return false;
        }
    }


    UsbRTUPortal(Device device) {
        super(new WrapperRTU());
        this.device = device;
        localTimeout = 2;
    }



    @Override
    public void open(String path, String parameters) throws IOException {
        if(!device.isOpen())device.open();
        Map<String, String> settings;
        int baudrate = 9600;timeout = 3000; attempts = 1;
        try {
            if (parameters != null && !parameters.isEmpty()) {
                settings = parseFragment(parameters);
                try{timeout = Integer.parseInt(settings.get("ms"));}catch(Exception ignored){}
                try{attempts = Integer.parseInt(settings.get("tries"));}catch(Exception ignored){}
                try{baudrate = Integer.parseInt(settings.get("baud"));}catch(Exception ignored){}
            } else {
                settings = parseProperties(CfgRoot.PATH+PROPERTY_PATH);
                timeout = Integer.parseInt(settings.get("ms"));
                attempts = Integer.parseInt(settings.get("tries"));
                baudrate = Integer.parseInt(settings.get("baud"));
            }
        }catch(NumberFormatException ignored){}

        device.reset();
        device.purgeReceiveBuffer();
        device.purgeTransmitBuffer();
        device.setReadTimeout(300/*localTimeout*/);
        device.setWriteTimeout(300/*localTimeout*/);
//        Port port = device.getPort();
//        DeviceDescriptor ee = device.getDeviceDescriptor();

        //TODO configDevice
        device.getPort().reset();
        device.getPort().setBaudRate(baudrate);
        device.getPort().setDataCharacteristics(DataBits.DATA_BITS_8, StopBits.STOP_BITS_1, Parity.NONE);
    }

    @Override
    public void close() {
        try {
            if(device!=null)device.close();
        } catch (FTD2xxException ignored) {}
        device = null;
        isOneConnectionExist = false;
    }

    @Override
    protected void write(byte[] buffer) throws IOException {
        try {
            device.write(buffer);
        } catch (FTD2xxException e) {
            throw new IOException(e.getMessage());
        }
    }

    @Override
    protected int append(byte[] buffer, int length) throws IOException {
        try {
            while(device.getReceiveQueueStatus()!=0){
                int bytes = device.read();
                buffer[length] = (byte) bytes;
                length++;
            }
            Thread.sleep(localTimeout);
            return length;
        } catch (FTD2xxException e) {
            log.severe(e.getLocalizedMessage());
        } catch (InterruptedException ignored) {}
        return length;
    }

    public DeviceDescriptor getDeviceDescriptor() {
        try {
            return device.getDeviceDescriptor();
        } catch (FTD2xxException e) {
            log.severe(e.getLocalizedMessage());
            return null;
        }
    }

    public String getProductDescription(){
        try {
            return device.getDeviceDescriptor().getProductDescription();
        } catch (FTD2xxException e) {
            log.severe(e.getLocalizedMessage());
            return null;
        }
    }

    public void setProductDescription(String pd){
        try {
            EEPROM eeprom = device.getEEPROM();
            DeviceDescriptor dd = eeprom.readDeviceDescriptor();
            dd.setProductDescription(pd);
            eeprom.writeDeviceDescriptor(dd);
        } catch (FTD2xxException e) {
            log.severe(e.getLocalizedMessage());
        }
    }

    @Override
    public int bytesReady() throws IOException {
        return device.getInputStream().available();
    }

    @Override
    public byte[] read(int bytes) throws IOException {
        byte buffer[] = new byte[bytes];
        device.read(buffer);
        return buffer;
    }

    public static class FTDIMonitor extends Thread{
        int netAddress = 0;
        UsbRTUPortal usbPortal = null;

        public FTDIMonitor() {
            super("Launch monitor FTDI USB");
        }

        public synchronized int getNetAddress() {
            return netAddress;
        }

        public synchronized UsbRTUPortal getUsbPortal() {
            return usbPortal;
        }

        private int seekingNetAddress(int limit) throws IOException {
            int na;
            limit &= 0xFF;
            log.fine("Start net address enumeration");
            UsbRTUPortal usb = UsbRTUPortal.getUsbRTUPortal();
            usb.open(null, "ms=5000");
            try {
                ModbusDeviceID idReader;
                for (na = 1; na < limit; na++) {
                    idReader = new ModbusDeviceID(usb, na);
                    idReader.readID();
                    break;
                }
                if (na == limit) na = 0;
                return na;
            } catch (Exception e) {throw e;}
            finally {usb.close();}
        }

        private void awaitingConnection() throws InterruptedException, IOException {
            log.info("FTDI USB Monitor is launched and expecting USB device attachment...");
            while (!isInterrupted()) {
                if (UsbRTUPortal.isConnected() == true) break;
                Thread.sleep(1000);
            }
            if(isInterrupted()){
                log.fine("Monitor is interrupted...");
                netAddress = 0;
                throw new InterruptedException();
            }
            log.info("Device is found");
            usbPortal = UsbRTUPortal.getUsbRTUPortal();
        }



        private void awaitingDisconnect() throws InterruptedException, LineUnavailableException, IOException {
            //UsbRTUPortal usb = UsbRTUPortal.getUsbRTUPortal();
            while (usbPortal.isConnected() == true && !isInterrupted()) {
                Thread.sleep(500);
            }
            if(UsbRTUPortal.isConnected() == false){
                log.info("Usb device has been detached");
                usbPortal = null;
                netAddress = 0;
            }else {
                throw new InterruptedException();
            }
        }

        @Override
        public void run() {
            try {
                while(!isInterrupted()) {
                    awaitingConnection();
                    awaitingDisconnect();
                }
            } catch (Exception ignored) {}
        }


    }
    static final FTDIMonitor ftdiMonitor = new FTDIMonitor();

    public static FTDIMonitor getFtdiMonitor() {
        return ftdiMonitor;
    }

    public static void startFTDIMonitor(){
        ftdiMonitor.start();
    }

    public static int netAddressEnumeration(int naLimit ) throws IOException {
        int netAddress = ftdiMonitor.seekingNetAddress(naLimit);
        if (netAddress <= 0 || netAddress > 249) {
            log.info("Wrong net address");
            netAddress = 0;
        } else log.info("Net address is " + netAddress);
        return netAddress;
    }

    public static void stopFTDIMonitor(){
        ftdiMonitor.interrupt();
        while(ftdiMonitor.isAlive());
    }




}
