package com.tess;

import com.tess.dataLinks.UsbRTUPortal;
import com.tess.modbus.ModbusDeviceID;
import com.tess.modbus.register.RegisteredValue;
import com.tess.modbus.register.RegisteredValueSet;
import com.tess.modbus.register.Registers;
import com.tess.modbus.register.RegistersFactory;

import java.io.*;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

/**
 * Created by TESS on 19.05.2016.
 * Console helper
 */
public class Console {
    static Logger log = Logger.getLogger(ServerPollManager.class.getName());
    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
    public static void outt(String str){
        System.out.println(str);
    }
    public static void logg(String str){
        try(PrintStream printStream = new PrintStream(new FileOutputStream(CfgRoot.PATH + "\\Config\\Logger.txt", true), true)){
            str = "" + new Date().toString()+": "+str;
            printStream.println(str);
        }catch(Exception ignored){}
    }

    public static String deepByteArray(byte[]buff) {
        StringBuilder res = new StringBuilder();
        for (byte b : buff)
            res.append(String.format("%02X ", b));
        return res.toString();
    }

    public static String deepByteArray(byte[]buffer, int offset, int len){
        byte[]subBuffer = new byte[len];
        for(int i = offset, y = 0;len-- > 0;i++, y++)subBuffer[y] = buffer[i];
        return deepByteArray(subBuffer);
    }

    public static void outList(List<RegisteredValueSet> registeredValueListSet){
        for(RegisteredValueSet r: registeredValueListSet){
            System.out.println("Next record:");
            for (RegisteredValue rv : r) {
                System.out.println(rv);
            }
        }
    }

    public String read(){
        String line;
        try {
            line = bufferedReader.readLine();
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
        return line;
    }

    public void close(){
        try {
            bufferedReader.close();
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    public static void main(String args[]) throws Exception{
        CfgRoot.initLogger();
        log.info("Version: 2.2.");

        ServerPollManager.consoleWindow( args[0], args.length == 2? args[1] : null);
//        Registers registers = RegistersFactory.getCommon();
//        registers.load("STU5001");
//        DaoModel daoModel = new DaoModel("MsSQLServer2000", RegistersFactory.load("POLLER"));
//        daoModel.connect();
//        daoModel.createTable(daoModel.getRegisters().getSetByGroup("UrgGroups"));
//        daoModel.close();
    }
}
