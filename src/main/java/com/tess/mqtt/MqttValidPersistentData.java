package com.tess.mqtt;

import org.eclipse.paho.client.mqttv3.MqttPersistable;

/**
 * Created by Admin on 26.08.2019.
 */
public class MqttValidPersistentData implements MqttPersistable {
    // Message key
    private String    key  = null;

    // Message header
    private byte[] header  = null;
    private int    hOffset = 0;
    private int    hLength = 0;

    // Message payload
    private byte[] payload = null;
    private int    pOffset = 0;
    private int    pLength = 0;

    /**
     * Construct a data object to pass across the MQTT client persistence interface.
     *
     * When this Object is passed to the persistence implementation the key is
     * used by the client to identify the persisted data to which further
     * update or deletion requests are targeted.<BR>
     * When this Object is created for returning to the client when it is
     * recovering its state from persistence the key is not required to be set.
     * The client can determine the key from the data.
     * @param key     The key which identifies this data
     * @param header  The message header
     * @param hOffset The start offset of the header bytes in header.
     * @param hLength The length of the header in the header bytes array.
     * @param payload The message payload
     * @param pOffset The start offset of the payload bytes in payload.
     * @param pLength The length of the payload in the payload bytes array
     * when persisting the message.
     */
    public MqttValidPersistentData ( String key,
                               byte[] header,
                               int    hOffset,
                               int    hLength,
                               byte[] payload,
                               int    pOffset,
                               int    pLength) {
        this.key     = key;
        this.header  = header.clone();
        this.hOffset = hOffset;
        this.hLength = hLength;
        this.payload = (payload !=null)?payload.clone(): null;
        this.pOffset = pOffset;
        this.pLength = pLength;
    }

    public String getKey() {
        return key;
    }

    public byte[] getHeaderBytes() {
        return header;
    }

    public int getHeaderLength() {
        return hLength;
    }

    public int getHeaderOffset() {
        return hOffset;
    }

    public byte[] getPayloadBytes() {
        return payload;
    }

    public int getPayloadLength() {
        if ( payload == null ) {
            return 0;
        }
        return pLength;
    }

    public int getPayloadOffset() {
        return pOffset;
    }

}
