package com.tess.mqtt;

import com.tess.host.client.RequestOwner;
import com.tess.modbus.register.RegisteredValueSet;
import com.tess.modbus.types.ArchiveClock;

/**
 * Created by Admin on 26.08.2019.
 */
public class DataMM {
    RequestOwner owner;
    String topic;
    RegisteredValueSet rvs;

    public DataMM(RequestOwner owner, String topic, RegisteredValueSet rvs) {
        this.owner = owner;
        this.topic = topic;
        this.rvs = rvs;
    }
}
