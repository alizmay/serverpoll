package com.tess.mqtt;

import com.tess.CfgRoot;
import com.tess.ServerPollManager;
import com.tess.host.client.OutputClient;
import com.tess.host.client.RequestOwner;
import com.tess.host.queuemessages.QueueMessages;
import com.tess.modbus.register.Register;
import com.tess.modbus.register.RegisteredValue;
import com.tess.modbus.register.RegisteredValueSet;
import com.tess.modbus.types.ArchiveClock;
import com.tess.multiserver.MultiModbusServer;
import org.eclipse.paho.client.mqttv3.*;


import java.io.*;
import java.lang.invoke.MethodHandles;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.UUID;
import java.util.logging.Logger;

/**
 * Created by Admin on 26.08.2019.
 */
public abstract class MqttClientStu implements OutputClient{
    final static Logger log = Logger.getLogger(MethodHandles.lookup().toString());
    final static String MQTT_PROPERTIES = "\\Config\\mqtt.properties";
    public final static String ClientID = UUID.randomUUID().toString();
    IMqttClient imc;
    QueueMessages<DataMM> mqttMessagesThread = null;
    String mqttServer = null;
    int qos = 0;
    boolean retained = true;

    private Properties loadProperties() throws IOException {
        Properties prop = new Properties();
        try (InputStream is = new FileInputStream(CfgRoot.PATH + MQTT_PROPERTIES)) {
            prop.load(is);
            mqttServer = prop.getProperty("server");
            qos = Integer.parseInt(prop.getProperty("qos"));
            retained = Boolean.parseBoolean(prop.getProperty("retained"));
            StringWriter sw = new StringWriter();
            prop.list(new PrintWriter(sw));
            log.fine(sw.toString());
            log.fine("MQTT config was loaded successfully");
        }
        return prop;
    }

    public MqttClientStu() throws IOException, MqttException {
        loadProperties();
        init(mqttServer);
    }

    public MqttClientStu(String mqttServer) throws MqttException {
        this.mqttServer = mqttServer;
        init(mqttServer);
    }

    private void init(String mqqtServer) throws MqttException {
        if(mqttServer == null)throw new MqttException(new Exception("Undefined MQTT server address"));
        imc = new MqttClient(mqqtServer, ClientID,
                new MqttValidFilePersistence(System.getProperty("user.dir")+"\\tmp\\"));//new MqttValidFilePersistence());
        connect();
        if(imc.isConnected())log.info("MQTT client is connected");
        else {
            log.info("MQTT broker is not available");
            throw new RuntimeException("MQTT broker is not available");
        }
        mqttMessagesThread = new QueueMessages<DataMM>("MqttMessagesThread") {
            @Override
            public void action() {
                DataMM dmm = getQueue().poll();
                if(imc.isConnected()== false){
                    dmm.owner.log("MQTT Broker is not available");
                    return;
                }

                String tittle = dmm.topic;
                dmm.owner.log("Mqtt published: ");
                for(RegisteredValue rv : dmm.rvs){
                    if(rv.getValue() == null)continue;
                    String topic  = tittle + rv.getRegister().getUnitMark();
                    try {
                        MqttMessage mm = new MqttMessage(rv.getValue().getBytes());
                        mm.setQos(qos);
                        mm.setRetained(retained);
                        imc.publish(topic, mm);
                        dmm.owner.log(topic + ' '+rv.getValue());
                    } catch (MqttException e) {
                        dmm.owner.log(topic + ' ' + ServerPollManager.getTrackStackString(e));
                    }
                }

            }
        };
        mqttMessagesThread.start();
    }

    private void connect() throws MqttException {
        MqttConnectOptions options = new MqttConnectOptions();
        options.setAutomaticReconnect(true);
        options.setCleanSession(true);
        options.setConnectionTimeout(10);
        imc.connect(options);
    }

//    public void sendMessage(RequestOwner owner, ArchiveClock clock, RegisteredValueSet rvs){
//        mqttMessagesThread.add(new DataMM(owner, clock, rvs));
//    }

    public void close(){
        mqttMessagesThread.interrupt();
        try {
            mqttMessagesThread.join();
            imc.disconnect();
            imc.close();
            log.fine("MQTT conection is closed");
            System.out.println("mqqt closed, is Alive?" + mqttMessagesThread.isAlive());
            System.out.println("imc connected?" + imc.isConnected());
        } catch (InterruptedException ignored) {} catch (MqttException ignored) {}
    };

    public boolean isDone(){
        if(mqttMessagesThread.isAlive())return false;
        return true;
    }

    protected void sendMessage(RequestOwner owner, String topic, RegisteredValueSet rvs) {
        mqttMessagesThread.add(new DataMM(owner, topic, rvs));
    }

    public boolean isConnected(){
        return imc.isConnected();
    }
}
