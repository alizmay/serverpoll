package com.tess;

import com.tess.dao.DBase;
import com.tess.dao.DataBase;
import com.tess.dao.MsSQLServer2000;
import com.tess.dao.SQLite;
import com.tess.modbus.register.Register;
import com.tess.modbus.register.RegisteredValue;
import com.tess.modbus.register.RegisteredValueSet;
import com.tess.modbus.register.Registers;


import java.io.Closeable;
import java.io.IOException;
import java.sql.SQLException;
import java.util.*;
import java.util.logging.Logger;

/**
 * Created by Admin on 07.06.2018.
 */
public class DaoModel implements Closeable{
    static Logger log = Logger.getLogger(DaoModel.class.getName());
    private DataBase dbase;
    private String classNameDBase;
    //private boolean autoCreateTables = false;
    private Registers registers;

    public Registers getRegisters() {
        return registers;

    }

    public DaoModel(String type, Registers rs) {
        assert(type != null);
        this.classNameDBase = type;
        if(rs == null)throw new RuntimeException("Registers are empty");
        registers = rs;
        switch(classNameDBase) {
            case "MsSQLServer2000":
                dbase = new MsSQLServer2000();
                break;
            case "SQLite":
                dbase = new SQLite();
                break;
            default:
                throw new NoSuchElementException(classNameDBase + " is unknown");
        }
    }

    public void connect() throws IOException {
        dbase.init();
    }

    @Override
    public void close() throws IOException {
        try {
            dbase.close();
        } catch (Exception e) {
            throw new IOException(e.getCause());
        }
    }


    public void createTableIfNotExist(String nameTable, Set<Register>table) throws SQLException {
        Map<String, Set<Register>> m = RegisteredValueSet.getSetGrouped(table);
        for(Map.Entry<String, Set<Register>> pair: m.entrySet()) {
            String query = String.format("SELECT COUNT(*) FROM " + nameTable + pair.getKey());
            try {
                dbase.rawQuery(query);
            } catch (SQLException e) {
                dbase.createTable(nameTable, pair.getValue());
            }

        }
    }

    public void createTable(Set<Register>table) throws SQLException {
        Map<String, Set<Register>> m = RegisteredValueSet.getSetGrouped(table);
        for(Map.Entry<String, Set<Register>> pair: m.entrySet())
           dbase.createTable(registers.getSetName(), pair.getValue());
    }

    public String write(RegisteredValueSet rvs) {
        return dbase.write(registers.getSetName(), rvs, null);
    }

    public String write(RegisteredValueSet rvs, RegisteredValueSet keys){
        return dbase.write(registers.getSetName(), rvs, keys);
    }

    public synchronized String read(List<RegisteredValueSet>list, Set<Register>set) {
        return (dbase.read(list, registers.getSetName(), set, null));
    }

    public  synchronized  String read(List<RegisteredValueSet>list, Set<Register>set, RegisteredValueSet keys){
        return dbase.read(list, registers.getSetName(), set, keys);
    }
}
