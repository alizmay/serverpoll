package com.tess.dao;

import com.tess.modbus.register.Register;
import com.tess.modbus.register.RegisteredValueSet;

import java.io.Closeable;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Set;

/**
 * Created by Admin on 22.06.2018.
 */
public interface DataBase extends Closeable {

    void init() throws IOException;

    String read(List<RegisteredValueSet> resultList, String setName, Set<Register> lookup, RegisteredValueSet keys);

    String write(String setName, RegisteredValueSet toInsert, RegisteredValueSet keys);

    boolean createTable(String setName, Set<Register> set) throws SQLException;

    Object rawQuery(String query) throws SQLException;
}
