package com.tess.dao;

import com.tess.CfgRoot;
import com.tess.modbus.register.Register;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by TESS on 22.07.2016.
 * Microsoft SQL Server 2000 implementation
 */
public class MsSQLServer2000 extends SQLDataBase {

    private static final String CREATE_DEVICEHEAD = "CREATE TABLE [Common.DeviceHead](" +
            "DeviceID INT PRIMARY KEY IDENTITY(1,1) NOT NULL," +
            "Device varchar(32) NOT NULL, " +
            "SerNo  varchar(16) NOT NULL, " +
            "Vendor varchar(32) NULL, " +
            "URI varchar(642) NULL, " +
            "LastSession datetime NULL, " +
            "LastAttempt datetime NULL" +
            ")";
    public MsSQLServer2000(){
        createDeviceHead = CREATE_DEVICEHEAD;
    }

    protected String path = CfgRoot.PATH + "//Config//MSSQLSERVER2000.properties";

    @Override
    public void init() throws IOException {
        if (connection != null)return;

        String connectionUrl;
        try (FileInputStream fis = new FileInputStream(path)) {
            Properties prop = new Properties();
            prop.load(fis);
            connectionUrl = String.format(
                    prop.getProperty("SQLdriver"),
//                    "jdbc:sqlserver://%s:%s;databaseName=%s;user=%s;password=%s;",
//                    "jdbc:sqlserver://%s:%s;databaseName=%s;user=%s;password=%s;"
                    prop.getProperty("IP"),
                    prop.getProperty("port"),
                    prop.getProperty("databasename"),
                    prop.getProperty("login"),
                    prop.getProperty("password")
            );
            log.fine("SQL Url:" + connectionUrl);// Console.out("SQL Url:" + connectionUrl);
            connection = DriverManager.getConnection(connectionUrl);
            statement = connection.createStatement();
        } catch (Exception e) {
            throw new IOException(e.getMessage());
        }
        log.fine("SQLManager has connected successfully");//Console.out("SQLManager has connected successfully");
    }

    @SuppressWarnings("UnusedAssignment")
    public String convertToDBType(Register register, String value) {

        String result = "NULL";
        if (register == null) return null;
        if(register.getTypeMapping().getCache() != null)
            switch (register.getTypeMapping().getCache()) {
                case CHECK:
                case STRING:
                    if(value != null)result = String.format("N'%s'", value);
                    break;
                case CLOCK:
                    if(value != null) {
                        String ts = new Timestamp(new SimpleDateFormat("dd.MM.yy HH:mm:ss").parse(value, new ParsePosition(0))
                                .getTime()).toString();
                        result = "'" + ts + "'";
                    }
                    break;
                case UNIQUEIDENTIFIER:
                    if(value != null)result = "NEWID()";
                default:
                    if(value != null)result = value;
        }else{
            result = register.getTypeMapping().getConverter().UniToDao(this.getClass().getSimpleName(), value);
        }
        return result;
    }

    @Override
    public String getDBType(Register register) {
        String type;
        if(register.getTypeMapping().getCache() != null)
            switch (register.getTypeMapping().getCache()) {
                case FLOAT:
                    type = "FLOAT";
                    break;
                case INT:
                    type = "INT";
                    break;
                case INT_PRIMARYKEY:
                    type = "INT PRIMARY KEY IDENTITY(1,1) NOT NULL";
                    break;
                case CHECK:
                case STRING:
                    type = "[nvarchar](30)";
                    break;
                case CLOCK:
                    type = "[datetime]";
                    break;
                case UNIQUEIDENTIFIER:
                    type = "[uniqueidentifier]";
                    break;
                default:
                    type = "[varchar](32)";
            }else{
            type = register.getTypeMapping().getConverter().getDaoType(this.getClass().getSimpleName());
        }
        return type;
    }

    @Override
    public String convertFromDBType(Register r, Object o){
        if(o == null)return null;
        if(o.toString().toUpperCase().equals("NULL"))return null;
        String result;
        if(r.getTypeMapping().getCache() != null)
            switch(r.getTypeMapping().getCache()){
                case CLOCK:
                    if(o instanceof Timestamp) {
                        Timestamp timestamp = (Timestamp) o;
                        result = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss").format(timestamp);
                    }else result = null;
                    break;
                default:result = o.toString();
            }
        else{
            result = r.getTypeMapping().getConverter().DaoToUni(this.getClass().getSimpleName(), o);
        }
        return result;
    }


    @Override
    public boolean isExist(String nameTable) {
        try {
            if (connection == null || statement == null) throw new SQLException("No connection detected");
            String querry = String.format("SELECT OBJECT_ID('[%s]') AS 'Object ID';", nameTable);
            log.config(querry);//Console.out(querry);
            ResultSet rs = statement.executeQuery(querry);
            rs.next();
            return rs.getInt("Object ID") != 0;
        }catch(SQLException e){return false;}
    }

    /*@Override
    public void createTable(String setName, Set<register>set) throws SQLException {
        setName = setName + "." + set.iterator().next().getGroupMark();
        if(isExist(setName)== true)return;
        if(set.size() == 0)return;
        String createQuerry = String.format("CREATE TABLE [%s](", setName);
        Iterator<register>it = set.iterator();
        while(true){
            createQuerry += convertToDBType(it.next(), null)[1];
            if(it.hasNext() == false)break;
            createQuerry +=",";
        }
        createQuerry +=")";
        Console.out(createQuerry);
        statement.execute(createQuerry);
    }*/
/*    private String toWhereString(RegisteredValueSet keys){
        StringBuilder sb = new StringBuilder();
        sb.append(" WHERE ");
        Iterator<RegisteredValue> it = keys.iterator();
        while(true){
            RegisteredValue registeredValue = it.next();
//            if(!table.equals(registeredValue.getRegister().getGroupMark()))continue;
            String value = registeredValue.getValue();
            if(value == null)continue;
            sb.append(String.format("%s = '%s'",registeredValue.getRegister().getUnitMark(), value));
            if(!it.hasNext())break;
            sb.append(" AND ");
        }
        return sb.toString();
    }*/

 /*   @Override
    public List<RegisteredValueSet> select(String setName, Set<register> lookup, RegisteredValueSet keySet) throws SQLException {
        if(connection == null || statement == null)throw  new SQLException("No connection detected");
        if(lookup == null || lookup.size() == 0)throw  new SQLException("No sets of registers detected");
        String table  = keySet.getRegisteredValue(0).getRegister().getGroupMark();
        String fullNameTable = setName + "." + table;
        String query;
        if(keySet == null)query = String.format("SELECT * FROM [%s]", fullNameTable);
        else query = String.format("SELECT * FROM [%s]", fullNameTable) + toWhereString(keySet);
        Console.out(query);
        resultSet  = statement.executeQuery(query);
        List<RegisteredValueSet>resultList = new ArrayList<>();
        while (resultSet.next()) {
            ResultSetMetaData rsmd = resultSet.getMetaData();
            RegisteredValueSet regList = new RegisteredValueSet();
            for (int x = 0; x < rsmd.getColumnCount(); x++) {
                String mark = table + ":" + rsmd.getColumnLabel(x + 1);
                register r = null;
                for(register reg: lookup)if(reg.getMark().equals(mark)){r = reg;break;}
                if (r == null) continue;
                Object o = resultSet.getObject(x + 1);
                String val = convertFromDBType(r, o);
                regList.add(new RegisteredValue(r, val));
            }
            if(regList.size() > 0)resultList.add(regList);
        }
        return resultList;
    }
    @Override
    public void insert(Map<String, RegisteredValueSet> map) throws SQLException {
        if(connection == null || statement == null)throw  new SQLException("No connection detected");
        for(Map.Entry<String, RegisteredValueSet> srvs :map.entrySet()) {
            StringBuilder clmn = new StringBuilder();
            StringBuilder vals = new StringBuilder();
            vals.append("VALUES(");
            RegisteredValueSet registeredValueSet = srvs.getValue();
            String table = srvs.getKey() + "." + registeredValueSet.getRegisteredValue(0).getRegister().getGroupMark();
            for (Iterator<RegisteredValue> i = registeredValueSet.iterator(); i.hasNext(); ) {
                RegisteredValue r = i.next();
                String val = convertToDBType(r.getRegister(), r.getValue())[0];
                clmn.append(r.getRegister().getUnitMark() + ',');
                vals.append(val + ',');
            }
            clmn.deleteCharAt(clmn.length() - 1); clmn.append(')');
            vals.deleteCharAt(vals.length() - 1); vals.append(')');
            String result = String.format("INSERT INTO [%s](", table) + clmn.toString() + vals.toString();
            Console.out(result);
            int count = statement.executeUpdate(result);
            Console.out("Rows affected:" + count);
        }
    }
    @Override
    public void update(String groupTable, RegisteredValueSet toInsert, RegisteredValueSet keys) throws SQLException {
        if (connection == null || statement == null) throw new SQLException("No connection detected");
        if (keys == null) throw new SQLException("No keys detected");
        if (toInsert == null) throw new SQLException("No data to insert");
        String table = groupTable + "." + toInsert.first().getRegister().getGroupMark();
        RegisteredValue rv;
        StringBuilder query = new StringBuilder();
        query.append(String.format("UPDATE [%s] SET ", table));
        for (Iterator<RegisteredValue> it = toInsert.iterator(); it.hasNext(); ) {
            rv = it.next();
            String col = rv.getRegister().getUnitMark();
            String val = convertToDBType(rv.getRegister(), rv.getValue())[0];
            query.append(String.format("%s = %s,", col, val));
        }
        query.deleteCharAt(query.length() - 1);
        query.append(toWhereString(keys));
        Console.out(query.toString());
        int count = statement.executeUpdate(query.toString());
        Console.out("Rows affected:" + count);
        if(count == 0)throw new SQLException("Not found for updating");
    }

    @Override
    public void close(){
        if(connection == null)return;
        try {
            if(connection!=null)connection.close();
            if(statement !=null)statement.close();
            Console.out("SQLManager: Connection is closed");
        } catch (SQLException e) {}
    }

    @Override
    public ResultSet rawQuery(String query) throws SQLException {
        if(query.contains("select")|| query.contains("SELECT"))resultSet = statement.executeQuery(query);
        else statement.execute(query);
        return resultSet;
    }*/


}
