package com.tess.dao;

import com.tess.modbus.register.Register;
import com.tess.modbus.register.RegisteredValueSet;

import java.io.IOException;
import java.sql.SQLException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Set;

/**
 * Created by Admin on 09.04.2019.
 */
public class MsExcel implements DBase {
    @Override
    public void init() throws IOException {

    }

    @Override
    public String convertToDBType(Register register, String value) {
        String result = "NULL";
        if (register == null) return null;
        if(register.getTypeMapping().getCache() != null)
            switch (register.getTypeMapping().getCache()) {
                case INT_PRIMARYKEY:
                case PRESS160:
                case TOTAL:
                case INT:
                case LONG:
                case FLOAT:
                    if(value != null)result = value;
                    break;
                case CLOCK:
                default:
                    if (value != null) result = "'" + value + "'";
            }
        else {
            result = register.getTypeMapping().getConverter().UniToDao("MsExcel", value);
        }
        return result;
    }

    @Override
    public String getDBType(Register register) {
        return "MsExcel";
    }

    @Override
    public String convertFromDBType(Register register, Object object) {
        return null;
    }

    @Override
    public boolean isExist(String nameTable) {
        return false;
    }

    @Override
    public boolean createTable(String setName, Set<Register> set){
        return false;
    }

    @Override
    public List<RegisteredValueSet> select(String setName, Set<Register> lookup, RegisteredValueSet rvSet) throws SQLException {
        return null;
    }

    @Override
    public int insert(String prefix, RegisteredValueSet rvs) throws SQLException {
        return 0;
    }

    @Override
    public int update(String setName, RegisteredValueSet toInsert, RegisteredValueSet keys) throws SQLException {
        return 0;
    }

    @Override
    public Object rawQuery(String query) throws SQLException {
        return null;
    }

    @Override
    public void close() throws Exception {

    }
}
