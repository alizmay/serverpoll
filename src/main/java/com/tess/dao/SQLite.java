package com.tess.dao;

import com.tess.CfgRoot;
import com.tess.modbus.register.Register;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by TESS on 04.08.2016.
 * SQLite implementation
 */
public class SQLite extends SQLDataBase{
    static{
        databaseLockRepeater = new Repeater(10) {
            @Override
            boolean catchCase(Exception ex) {
                if(ex.getMessage().contains("database is locked"))return true;
                return false;
            }
        };
    }
    static final String CREATE_DEVICEHEAD = "CREATE TABLE [Common.DeviceHead](\n" +
            "DeviceID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,\n" +
            "Device CHAR(32) NOT NULL, " +
            "SerNo  CHAR(16) NOT NULL, " +
            "Vendor CHAR(32) NULL," +
            "URI varchar(64) NULL," +
            "LastSession TEXT," +
            "LastAttempt TEXT" +
            ")";

    public SQLite(){
        createDeviceHead = CREATE_DEVICEHEAD;
    }
    @Override
    public void init() throws IOException {
        if (connection != null)return;
        String path = CfgRoot.PATH + "\\Config\\SQLite.properties";
        String connectionUrl;
        try (FileInputStream fis = new FileInputStream(path)) {
            Properties prop = new Properties();
            prop.load(fis);
            connectionUrl = String.format(
                    prop.getProperty("SQLDriver"),
                    prop.getProperty("fileDB")
            );
            log.fine/*Console.out*/("SQLite database driver is " + connectionUrl);
            Class.forName("org.sqlite.JDBC");
            connection = DriverManager.getConnection(connectionUrl);
            statement = connection.createStatement();
        } catch (Exception e) {
            log.severe("Severe:" + e);
            throw new RuntimeException(e);
        }
        log.fine/*Console.out*/("SQLManager has connected successfully");
    }

    @Override
    public String convertToDBType(Register register, String value) {
        String result = "NULL";
        if (register == null) return null;
        if(register.getTypeMapping().getCache() != null)
            switch (register.getTypeMapping().getCache()) {
                case INT_PRIMARYKEY:
                case PRESS160:
                case TOTAL:
                case INT:
                case LONG:
                case FLOAT:
                    if(value != null)result = value;
                    break;
                case CLOCK:
                    if(value != null) {
                        java.util.Date d = new SimpleDateFormat("dd.MM.yy HH:mm:ss").parse(value, new ParsePosition(0));
                        String ts = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(d);
                        result = "'" + ts + "'";
                    }
                    break;
                default:
                    if (value != null) result = "'" + value + "'";
            }
        else {
            result = register.getTypeMapping().getConverter().UniToDao("SQLite", value);
        }
        return result;
    }

    @Override
    public String getDBType(Register register) {
        String type;
        if (register == null) return null;
        if(register.getTypeMapping().getCache() != null)
            switch (register.getTypeMapping().getCache()) {
                case INT_PRIMARYKEY:
                    type = "INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL";
                    break;
                case INT:
                    type = "INTEGER";
                    break;
                case PRESS160:
                case FLOAT:
                    type = "REAL";
                    break;
                case CLOCK:
                    type = "TEXT";
                    break;
                default:
                    type = "CHAR(32)";
            }
        else {
            type = register.getTypeMapping().getConverter().getDaoType("SQLite");
        }
        return type;
    }

    @Override
    public String convertFromDBType(Register r, Object o) {
        if(o == null)return null;
        if(o.toString().toUpperCase().equals("NULL"))return null;
        String result  = o.toString();
        if(r.getTypeMapping().getCache() != null)
            switch (r.getTypeMapping().getCache()) {
                case CLOCK:
                    java.util.Date d = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(result, new ParsePosition(0));
                    result = new SimpleDateFormat("dd.MM.yy HH:mm:ss").format(d);
                    break;
                default:result = o.toString();
        }else{
            result = r.getTypeMapping().getConverter().DaoToUni("SQLite", o);
        }
        return result;
    }

    @Override
    public boolean isExist(String nameTable) {
        String query = "SELECT * FROM [" + nameTable + "]";
        try {
            statement.execute(query);
        } catch (SQLException e) {
            return false;
        }
        return true;
    }


}
