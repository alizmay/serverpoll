package com.tess.dao;

import com.tess.ServerPollManager;
import com.tess.modbus.register.Register;
import com.tess.modbus.register.RegisteredValue;
import com.tess.modbus.register.RegisteredValueSet;
import com.tess.modbus.register.TypeMapping;

import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by TESS on 04.08.2016.
 * Basic class for databases of SQL type
 */
public abstract class SQLDataBase implements DBase, AutoCloseable, DataBase{
    static Logger log = Logger.getLogger(SQLDataBase.class.getName());
    String createDeviceHead = "";
    Statement statement = null;
    Connection connection = null;
    ResultSet resultSet = null;

    @Override
    public boolean createTable(String setName, Set<Register> set) {
        boolean result;
        try {
        if(statement == null)throw new SQLException("Using without initialisation");
        String createQuery;
        if(set == null || set.size() ==0)return false;
        if(setName == null || setName.isEmpty())
            setName = set.iterator().next().getGroupMark();
            else setName = setName + "." + set.iterator().next().getGroupMark();
        //if (isExist(setName)) return false;
        if (set.size() == 0) return false;
        createQuery = String.format("CREATE TABLE [%s](", setName);
        Iterator<Register> it = set.iterator();
        while (true) {
            Register r = it.next();
            createQuery += r.getUnitMark() + " " + getDBType(r);
            if (!it.hasNext()) break;
            createQuery += ",";
        }
        createQuery +=")";
        log.fine(createQuery);//Console.out(createQuery);
            if(result = statement.execute(createQuery))log.fine/*Console.out*/("Creation of " +setName + " was being done successfully ");
                //else log.fine/*Console.out*/("There was failure while creating " +setName);
        } catch (SQLException e) {result = false;
            log.fine(ServerPollManager.getTrackStackString(e));
            //System.out.println(e.getMessage());
        }
        return result;
    }

    protected String toWhereString(RegisteredValueSet keys){
        if(keys == null)return "";
        StringBuilder sb = new StringBuilder();
//        sb.append(" WHERE ");
        Iterator<RegisteredValue> it = keys.iterator();
        if(it.hasNext() == false)
            System.out.println(false);
        while(true){
            RegisteredValue registeredValue = it.next();
            String value = registeredValue.getValue();
            if(value != null) {
                sb.append(String.format("%s = %s", registeredValue.getRegister().getUnitMark(),
                        convertToDBType(registeredValue.getRegister(), value)));
            }
            if(!it.hasNext())break;
            sb.append(" AND ");
        }
        if(sb.length() == 0)return "";
        return " WHERE " + sb.toString();
    }

    @Override
    public List<RegisteredValueSet> select(String setName, Set<Register> lookup, RegisteredValueSet keySet) throws SQLException {
        if(connection == null || statement == null)throw  new SQLException("No connection detected");
        if(lookup == null || lookup.size() == 0)throw  new SQLException("No sets of registers detected");
        String table;
        if(keySet == null)table = lookup.iterator().next().getGroupMark();
            else table  = keySet.getRegisteredValue(0).getRegister().getGroupMark();
        String fullNameTable = setName + "." + table;
        String query;
//        if(keySet == null)query = String.format("SELECT * FROM [%s]", fullNameTable); else//!Here was: Warning:(67, 12) Condition 'keySet == null' is always 'false'
        query = String.format("SELECT * FROM [%s]", fullNameTable) + toWhereString(keySet);
        log.fine/*Console.out*/(query);
        resultSet  = statement.executeQuery(query);
        List<RegisteredValueSet>resultList = new ArrayList<>();
        while (resultSet.next()) {
            ResultSetMetaData rsmd = resultSet.getMetaData();
            RegisteredValueSet regList = new RegisteredValueSet();
            for (int x = 0; x < rsmd.getColumnCount(); x++) {
                String mark = table + ":" + rsmd.getColumnLabel(x + 1);
                Register r = null;
                for(Register reg: lookup)if(reg.getMark().equals(mark)){r = reg;break;}
                if (r == null) {
                    r = new Register(mark, -1, new  TypeMapping("UNKNOWN"));

                }
                Object o = resultSet.getObject(x + 1);
                String val = convertFromDBType(r, o);
                regList.add(new RegisteredValue(r, val));
            }
            if(regList.size() > 0)resultList.add(regList);
        }
        log.fine/*Console.out*/("Rows found:" + resultList.size());
        return resultList;
    }

    //@FunctionalInterface
    interface Repeatable{
        int repeatit()throws Exception;
    }

    static abstract class Repeater{
        int count;
        public Repeater(int count) {
            this.count = count;
        }

        abstract boolean catchCase(Exception ex);

        int run(Repeatable repeatable)throws Exception{
            int cnt = count;
            int result = 0;
            do {
                try {
                    result = repeatable.repeatit();
                    if(cnt != count)log.info("Process went off successfully after: " + (count - cnt) + " tries");
                    cnt = 0;
                }catch(Exception ex){
                    if (!catchCase(ex)) throw ex;
                        else {log.severe(ex.getMessage());}

                }
                if(cnt != 0) try { Thread.currentThread().sleep(100);cnt--;} catch (InterruptedException ignored) {}
            } while (cnt > 0);
            return result;
        }
    }

    static Repeater databaseLockRepeater = new Repeater(0) {
        @Override
        boolean catchCase(Exception ex) {
            return false;
        }
    };

//    private Map<String, RegisteredValueSet> getRvsGroup(RegisteredValueSet rvs){
//        Map<String, RegisteredValueSet> m = new HashMap<>();
//        for(RegisteredValue r : rvs){
//            if(!m.containsKey(r.getRegister().getGroupMark()))
//                m.put(r.getRegister().getGroupMark(), new RegisteredValueSet());
//            m.get(r.getRegister().getGroupMark()).add(r);
//        }
//        return m;
//    }
    @Override
    public int insert(String prefix, RegisteredValueSet rvs)throws SQLException{
        Map<String, RegisteredValueSet> map = rvs.getGrouped();
        if(connection == null || statement == null)throw  new SQLException("No connection detected");
        int totalRows = 0;
        try {
            for(Map.Entry<String, RegisteredValueSet> srvs :map.entrySet()) {
                RegisteredValueSet registeredValueSet = srvs.getValue();
                String table = registeredValueSet.getRegisteredValue(0).getRegister().getGroupMark();;
//                if(prefix !=null && !prefix.isEmpty()){
//                    table = prefix + "." + table;
//                }
                final String result = insertRequest(table, registeredValueSet);
//                StringBuilder clmn = new StringBuilder();
//                StringBuilder vals = new StringBuilder();
//                vals.append("VALUES(");
//                RegisteredValueSet registeredValueSet = srvs.getValue();
//                String table = registeredValueSet.getRegisteredValue(0).getRegister().getGroupMark();;
//                if(prefix !=null && !prefix.isEmpty()){
//                    table = prefix + "." + table;
//                }
//                for (RegisteredValue r : registeredValueSet) {
//                    String val = convertToDBType(r.getRegister(), r.getValue());
//                    clmn.append(r.getRegister().getUnitMark()).append(',');
//                    vals.append(val).append(',');
//                }
//                clmn.deleteCharAt(clmn.length() - 1); clmn.append(')');
//                vals.deleteCharAt(vals.length() - 1); vals.append(')');
//                final String result = String.format("INSERT INTO [%s](", table) + clmn.toString() + vals.toString();
                log.fine(result);
                int count = databaseLockRepeater.run(new Repeatable() {
                    @Override
                    public int repeatit() throws Exception {
                        return statement.executeUpdate(result);
                    }
                });
                log.fine("Rows affected:" + count);
                totalRows += count;
            }
        } catch (Exception e) {
            log.info("SQLite insert failure:" + e);
            throw new SQLException(e);
        }
        return totalRows;
    }

//    public int insert(Map<String, RegisteredValueSet> map) throws SQLException {
//        if(connection == null || statement == null)throw  new SQLException("No connection detected");
//        int totalRows = 0;
//        try {
//            for(Map.Entry<String, RegisteredValueSet> srvs :map.entrySet()) {
//                StringBuilder clmn = new StringBuilder();
//                StringBuilder vals = new StringBuilder();
//                vals.append("VALUES(");
//                RegisteredValueSet registeredValueSet = srvs.getValue();
//                String table = srvs.getKey() + "." + registeredValueSet.getRegisteredValue(0).getRegister().getGroupMark();
//                for (RegisteredValue r : registeredValueSet) {
//                    String val = convertToDBType(r.getRegister(), r.getValue());
//                    clmn.append(r.getRegister().getUnitMark()).append(',');
//                    vals.append(val).append(',');
//                }
//                clmn.deleteCharAt(clmn.length() - 1); clmn.append(')');
//                vals.deleteCharAt(vals.length() - 1); vals.append(')');
//                final String result = String.format("INSERT INTO [%s](", table) + clmn.toString() + vals.toString();
//                log.fine(result);
//                int count = databaseLockRepeater.run(new Repeatable() {
//                    @Override
//                    public int repeatit() throws Exception {
//                        return statement.executeUpdate(result);
//                    }
//                });//statement.executeUpdate(result);
//                log.fine("Rows affected:" + count);
//                totalRows += count;
//            }
//        } catch (Exception e) {
//            log.info("SQLite insert:" + e);
//            throw new SQLException(e);
//        }
//        return totalRows;
//    }

    @Override
    public int update(String prefix, RegisteredValueSet toInsert, RegisteredValueSet keys) throws SQLException {
        if (connection == null || statement == null) throw new SQLException("No connection detected");
        if (keys == null) throw new SQLException("No keys detected");
        if (toInsert == null) throw new SQLException("No data to insert");
        int totalRows = 0;
        String table = toInsert.first().getRegister().getGroupMark();
//        if(prefix !=null && !prefix.isEmpty()){
//            table = prefix + "." + table;
//        }
        final String result = updateRequest(table, toInsert, keys);
//        RegisteredValue rv;
//        final StringBuilder query = new StringBuilder();
//        query.append(String.format("UPDATE [%s] SET ", table));
//        for (RegisteredValue aToInsert : toInsert) {
//            rv = aToInsert;
//            String col = rv.getRegister().getUnitMark();
//            String val = convertToDBType(rv.getRegister(), rv.getValue());
//            query.append(String.format("%s = %s,", col, val));
//        }
//        query.deleteCharAt(query.length() - 1);
//        query.append(toWhereString(keys));
        if(log.getLevel() == Level.FINE)
        log.fine(result);
        int count = 0;
        try { count = databaseLockRepeater.run(new Repeatable() {
            @Override
            public int repeatit() throws Exception {
                return statement.executeUpdate(result.toString());
            }
        });}
            catch (Exception e) {throw new SQLException(e);}

        log.fine("Rows affected:" + count);
        totalRows += count;
        if(count == 0)throw new SQLException("Not found for updating");
        return totalRows;
    }

    public String selectRequest(String table, Set<Register> lookup, RegisteredValueSet keySet ){
        StringBuilder query = new StringBuilder();
        query.append(String.format(Locale.ENGLISH,"SELECT "));
        for(Register r: lookup)
            query.append('[' + r.getUnitMark() + "],");
        query.setLength(query.length() - 1);
        if(keySet == null)query.append(String.format(Locale.ENGLISH, " FROM [%s]", table));
        else query.append(String.format(Locale.ENGLISH, " FROM [%s]", table) + toWhereString(keySet));
        return query.toString();
    }


    public String insertRequest(String table, RegisteredValueSet registeredValueSet){
        StringBuilder clmn = new StringBuilder();
        StringBuilder vals = new StringBuilder();
        vals.append("VALUES(");
//        RegisteredValueSet registeredValueSet = srvs.getValue();
        for (RegisteredValue r : registeredValueSet) {
            String val = convertToDBType(r.getRegister(), r.getValue());
            clmn.append(r.getRegister().getUnitMark()).append(',');
            vals.append(val).append(',');
        }
        clmn.deleteCharAt(clmn.length() - 1); clmn.append(')');
        vals.deleteCharAt(vals.length() - 1); vals.append(')');
        final String result = String.format("INSERT INTO [%s](", table) + clmn.toString() + vals.toString();
        return result;
    }

    public String updateRequest(String table, RegisteredValueSet toInsert, RegisteredValueSet keys){
        if(keys == null)return " ";
        StringBuilder query = new StringBuilder();
        query.append(String.format("UPDATE [%s] SET ", table));
        for (RegisteredValue rv : toInsert) {
            if(rv.getRegister().getTypeMapping().toString().contains("PRIMARYKEY"))continue;
            String col = rv.getRegister().getUnitMark();
            String val = convertToDBType(rv.getRegister(), rv.getValue());
            query.append(String.format("%s = %s,", col, val));
        }
        query.deleteCharAt(query.length() - 1);
        query.append(toWhereString(keys));
        return query.toString();
    }





    @Override
    public String read(List<RegisteredValueSet>resultList, String prefix, Set<Register> lookup, RegisteredValueSet keySet) {
        StringBuilder logBuilder = new StringBuilder();
        if(connection == null || statement == null)logBuilder.append("SQL read failure: No connection detected\n");
        else if(lookup == null || lookup.size() == 0)logBuilder.append("SQL read failure: No sets of registers detected\n");
        else {
            String table;
            if (keySet == null) table = lookup.iterator().next().getGroupMark();
            else table = keySet.getRegisteredValue(0).getRegister().getGroupMark();
            //if(prefix != null && !prefix.isEmpty())prefix = prefix + "." + table;
            //    else
            prefix = table;
            String query = selectRequest(prefix, lookup, keySet);
//            if(log.getLevel() == Level.FINE)
            logBuilder.append(query + '\n');
            log.fine(query);
            try {
                resultSet = statement.executeQuery(query);
                while (resultSet.next()) {
                    ResultSetMetaData rsmd = resultSet.getMetaData();
                    RegisteredValueSet regList = new RegisteredValueSet();
                    for (int x = 0; x < rsmd.getColumnCount(); x++) {
                        String mark = table + ":" + rsmd.getColumnLabel(x + 1);
                        Register r = null;
                        for(Register reg: lookup)if(reg.getMark().equals(mark)){r = reg;break;}
                        if (r == null) {
                            r = new Register(mark, -1, new  TypeMapping("UNKNOWN"));

                        }
                        Object o = resultSet.getObject(x + 1);
                        String val = convertFromDBType(r, o);
                        regList.add(new RegisteredValue(r, val));
                    }
                    if(regList.size() > 0)resultList.add(regList);
                }
                log.fine("Rows found:" + resultList.size());
                logBuilder.append("Rows found:" + resultList.size() + '\n');
            } catch (Exception e) {
                String error = e.getClass().getSimpleName() + " : " + e.getMessage();
                StringWriter sw = new StringWriter();
                e.printStackTrace(new PrintWriter(sw));
                error += '\n' + sw.toString();
                log.severe(error);
                logBuilder.append(error);
            }
        }
        return logBuilder.toString();
    }

    @Override
    public String write(String prefix, RegisteredValueSet toInsert, RegisteredValueSet keys) {
        final StringBuilder logBuilder = new StringBuilder();
        if(connection == null || statement == null)logBuilder.append("SQL write failure: No connection detected\n");
        else if(toInsert == null || toInsert.size() == 0)logBuilder.append("SQL write failure: No sets of registers detected\n");
        else {
            try {
                String table;
                if(keys == null)table = toInsert.getRegisteredValue(0).getRegister().getGroupMark();
                    else table = keys.getRegisteredValue(0).getRegister().getGroupMark();
//                if (prefix != null && !prefix.isEmpty()) {
//                    table = prefix + "." + table;
//                }
                String select = selectRequest(table, toInsert.getSet(), keys);
                String insert = insertRequest(table, toInsert);
                String update = updateRequest(table, toInsert, keys);
                final String query = keys == null?insert :
                        String.format(Locale.ENGLISH, "IF NOT EXISTS (%s)", select) +
                        String.format(Locale.ENGLISH,"BEGIN %s END ELSE BEGIN %s END", insert, update);
                int count = 0;
                logBuilder.append('\n' + query + '\n');

                count = databaseLockRepeater.run(new Repeatable() {
                    @Override
                    public int repeatit() throws Exception {
                        return  statement.executeUpdate(query);
                    }
                });
                logBuilder.append("Rows affected: " + count + '\n');
                log.fine("Rows affected: " + count);
            }catch (Exception e) {
                String error = e.getClass().getSimpleName() + " : " + e.getMessage();
                StringWriter sw = new StringWriter();
                e.printStackTrace(new PrintWriter(sw));
                error += '\n' + sw.toString();
                log.severe(error);
                logBuilder.append(error);
                return logBuilder.toString();
            }
        }
        return logBuilder.toString();
    }


    @Override
    public Object rawQuery(String query) throws SQLException {
        if(query.contains("select")|| query.contains("SELECT"))resultSet = statement.executeQuery(query);
        else statement.execute(query);
        return resultSet;
    }

    @Override
    public void close(){
        if(connection == null)return;
        try {
            connection.close();
            if(statement !=null)statement.close();
            log.fine/*Console.out*/("SQLManager: Connection is closed");
        } catch (SQLException ignored) {}
    }
}

