package com.tess.dao;

import com.tess.modbus.register.Register;
import com.tess.modbus.register.RegisteredValueSet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by TESS on 22.07.2016.
 * Database interface
 */
public interface DBase extends AutoCloseable {
    /**
     * Connecting to dbase, extracting properties from file
     * @throws IOException
     */
    void init() throws IOException;

    /**
     * Conversion of incoming parameters to database understanding formats
     * @param register - info about value
     * @param value - value of registeredValue
     * @return String is converted value to DBType
     */
    String convertToDBType(Register register, String value);

    /**
     * Get name of type of the register in specific db type style
     * @param register
     * @return String type
     */
    String getDBType(Register register);

    /**
     * Conversin of received parameters from specified database to register format
     * @param register - definitions of expected dbase value
     * @param object - value from dbase
     * @return converted value as TypeMapping style
     */
    String convertFromDBType(Register register, Object object);

    boolean isExist(String nameTable);

    boolean createTable(String setName, Set<Register> set);

    List<RegisteredValueSet> select(String setName, Set<Register> lookup, RegisteredValueSet rvSet) throws SQLException;

    int insert(String prefix, RegisteredValueSet rvs)throws SQLException;

    int update(String setName, RegisteredValueSet toInsert, RegisteredValueSet keys) throws SQLException;

    //int insert(Map<String, RegisteredValueSet> map) throws SQLException;

    Object rawQuery(String query) throws SQLException;

}
