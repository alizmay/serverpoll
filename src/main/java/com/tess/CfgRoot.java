package com.tess;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.invoke.MethodHandles;
import java.util.logging.*;

/**
 * Created by Admin on 14.06.2019.
 */
public class CfgRoot {
    public static final String LOGGING_PROPERTIES =  "\\Config\\logging.properties";
    public static final String APPLICATION_PROPERTIES = "\\Config\\Application.properties";
    public static String PATH = System.getProperty("user.dir");
    static Logger log = Logger.getLogger(MethodHandles.lookup().toString());

    public static void initLogger(){
        try(InputStream is = new FileInputStream(CfgRoot.PATH + LOGGING_PROPERTIES)){//"E:\\tools\\JavaProjects\\TessBox\\Config\\logging.properties")) {
            LogManager.getLogManager().readConfiguration(is);
            Level l = log.getLevel();
            if(l==null)l = log.getParent().getLevel();
            log.log(l, "Current log level is " + l.toString());
//            log.setFilter(new Filter() {
//                @Override
//                public boolean isLoggable(LogRecord record) {
//                    if(record.getLoggerName().contains("tess"))return true;
//                    return false;
//                }
//            });
        } catch (IOException e) {
            System.out.println("Logger is not available:"+e); return;
        }
    }

    public static boolean debugDetector(String args[], String controlDir){
        boolean debug;
        if(controlDir != null && controlDir.equals(System.getProperty("user.dir")))
            debug = true;
        else {
            debug = false;
            for (String arg : args) {
                if("-d".equals(args)){
                    debug = true;
                    break;
                }
            }
        }
        if(debug){
            PATH = "E:\\TOOLS\\JavaAl\\jStu\\TessBox";
        }else PATH = System.getProperty("user.dir");
        return debug;
    }
}
