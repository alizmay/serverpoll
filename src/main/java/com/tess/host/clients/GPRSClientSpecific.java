package com.tess.host.clients;

import com.tess.modbus.register.RegisteredValueSet;
import com.tess.modbus.register.Registers;
import com.tess.modbus.types.ArchiveClock;

/**
 * Created by Admin on 03.12.2018.
 */
public interface GPRSClientSpecific extends RegistersClientSpecific {
    RegisteredValueSet getCppd(ArchiveClock from, RegisteredValueSet unitedGroups);
}
