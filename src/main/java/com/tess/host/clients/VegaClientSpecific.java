package com.tess.host.clients;

import com.tess.modbus.register.RegisteredValueSet;
import com.tess.modbus.register.Registers;

/**
 * Created by Admin on 19.12.2018.
 */
public interface VegaClientSpecific extends RegistersClientSpecific{
    RegisteredValueSet getCppd(RegisteredValueSet raw);
}
