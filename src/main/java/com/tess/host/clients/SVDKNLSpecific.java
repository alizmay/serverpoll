package com.tess.host.clients;

import com.tess.modbus.register.RegisteredValueSet;
import com.tess.modbus.types.ArchiveClock;

/**
 * SARATOV VODOKANAL CLIENT
 * Created by Admin on 23.07.2019.
 */
public class SVDKNLSpecific extends AbstractClientSpecific {

    public SVDKNLSpecific() {
        super("POLLERSVDKNL");
    }

    @Override
    public RegisteredValueSet getCppd(ArchiveClock from, RegisteredValueSet unitedGroups) {
        String q1 = unitedGroups.getRegisteredValue("q1").getValue();
        if(q1 == null || q1.contains("NULL"))return null;
        RegisteredValueSet cppd = registers.getRvsByGroup("Cppd");
        cppd.getRegisteredValue("Date_").setValue(from.toString());
        cppd.copy(unitedGroups, "Drill");
        cppd.copy(unitedGroups, "q1Hour = q1");
        cppd.copy(unitedGroups, "P1");
        cppd.copy(unitedGroups, "Downtime = stateDuration1");
        return cppd;
    }

    @Override
    public RegisteredValueSet getCppd(RegisteredValueSet raw) {
        throw new RuntimeException("Not created yet");
    }
}
