package com.tess.host.clients;

import com.tess.modbus.register.Registers;
import com.tess.modbus.register.RegistersFactory;

/**
 * Created by Admin on 03.12.2018.
 */
public abstract class AbstractClientSpecific implements GPRSClientSpecific, VegaClientSpecific {
    protected Registers registers;

    public AbstractClientSpecific(String pollerFileName) {
        registers = RegistersFactory.load(pollerFileName);
    }

    @Override
    public Registers getRegisters() {
        return registers;
    }


//    @Override
//    public RegisteredValueSet getCppd(ArchiveClock from, RegisteredValueSet unitedGroups) {
//        String q1 = unitedGroups.getRegisteredValue("q1").getValue();
//        if(q1 == null || q1.contains("NULL"))return null;
//        RegisteredValueSet cppd = registers.getRvsByGroup("Cppd");
//        cppd.getRegisteredValue("Date_").setValue(from.toString());
//        cppd.copy(unitedGroups, "Drill=Name");
//        cppd.copy(unitedGroups,"q1Hour = q1");
//        cppd.copy(unitedGroups, "Bkns, Depot, Group_");
//        cppd.getRegisteredValue("Size_").setValue("М3");
//        int upstate;
//        try {
//            int upstate1 = Integer.parseInt(unitedGroups.getRegisteredValue("Archive:stateDuration1").getValue());
//            int upstate2 = Integer.parseInt(unitedGroups.getRegisteredValue("Archive:stateDuration2").getValue());
//            upstate = upstate1 < upstate2 ? upstate1 : upstate2;
//        }catch(Exception ignored){upstate = 0;}
//        cppd.getRegisteredValue("Uptime").setValue(String.valueOf(upstate / 2));
//        cppd.getRegisteredValue("rowguid").setValue("NEWID()");
//        return cppd;
//    }
}
