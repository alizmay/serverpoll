package com.tess.host.clients;

/**
 * Created by Admin on 06.12.2018.
 */
public class ClientChooser {
    public static AbstractClientSpecific getClient(String cl){
        AbstractClientSpecific client;
        switch(cl){
            case "MMG": client = new MMGSpecific(); break;
            case "TTF": client = new TTFSpecific(); break;
            case "TTFLTD": client = new TTFLTDSpecific();break;
            case "SVDKNL": client = new SVDKNLSpecific();break;
            default: throw new RuntimeException(String.format("No such client was found: %s", cl));
        }
        return client;
    }
}
