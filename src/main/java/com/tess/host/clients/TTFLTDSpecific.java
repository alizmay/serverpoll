package com.tess.host.clients;

import com.tess.modbus.register.RegisteredValueSet;
import com.tess.modbus.types.ArchiveClock;

/**
 * Created by Admin on 04.03.2019.
 */
public class TTFLTDSpecific extends AbstractClientSpecific {

    public TTFLTDSpecific() {
        super("POLLERTTFLTD");
    }

    @Override
    public RegisteredValueSet getCppd(ArchiveClock from, RegisteredValueSet unitedGroups) {
        String q1 = unitedGroups.getRegisteredValue("q1").getValue();
        if(q1 == null || q1.contains("NULL"))return null;
        RegisteredValueSet cppd = registers.getRvsByGroup("Cppd");
        cppd.getRegisteredValue("Date_").setValue(from.toString());
        cppd.copy(unitedGroups, "Drill");
        cppd.copy(unitedGroups,"q1Hour = q1");
        int upstate;
        try {
            int upstate1 = Integer.parseInt(unitedGroups.getRegisteredValue("Archive:stateDuration1").getValue());
            int upstate2 = Integer.parseInt(unitedGroups.getRegisteredValue("Archive:stateDuration2").getValue());
            upstate = upstate1 < upstate2 ? upstate1 : upstate2;
        }catch(Exception ignored){upstate = 0;}
        cppd.getRegisteredValue("Downtime").setValue(String.valueOf(upstate / 2));
//        cppd.copy(unitedGroups, "T1");
        return cppd;
    }

    @Override
    public RegisteredValueSet getCppd(RegisteredValueSet raw) {
        RegisteredValueSet cppd = registers.getRvsByGroup("Cppd");
        ArchiveClock ac = new ArchiveClock(ArchiveClock.ArchiveMode.HOUR,
                raw.getRegisteredValue("LastSession").getValue());
        ac.increment(false);
        if(raw.getRegisteredValue("q1").getValue().equals("NaN")){
            raw.getRegisteredValue("q1").setValue("NULL");
//            raw.getRegisteredValue("T1").setValue("NULL");
            raw.getRegisteredValue("Downtime").setValue("NULL");
            return null;
        }else {
            double dt = Double.parseDouble(raw.getRegisteredValue("Downtime").getValue());
            cppd.getRegisteredValue("Downtime").setValue(String.valueOf(dt/2));
            cppd.getRegisteredValue("Date_").setValue(ac.toString());
            cppd.copy(raw, "Drill, q1Hour = q1");
//            if (cppd.getRegisteredValue("q1Hour").getValue().length() >= 8)
//                cppd.getRegisteredValue("q1Hour").setValue("#######");
        }
        return cppd;
    }

}
