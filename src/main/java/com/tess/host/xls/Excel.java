package com.tess.host.xls;

import com.tess.modbus.register.RegisteredValue;
import com.tess.modbus.register.RegisteredValueSet;

import java.lang.invoke.MethodHandles;
import java.util.ArrayList;
import java.util.List;


import com.tess.dao.DBase;
import com.tess.dao.MsExcel;
import com.tess.modbus.DeviceHead;
import com.tess.modbus.register.Register;
import com.tess.modbus.register.RegisteredValue;
import com.tess.modbus.register.RegisteredValueSet;
import com.tess.modbus.types.ArchiveClock;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.formula.FormulaParser;
import org.apache.poi.ss.formula.FormulaRenderer;
import org.apache.poi.ss.formula.FormulaType;
import org.apache.poi.ss.formula.ptg.AreaPtg;
import org.apache.poi.ss.formula.ptg.Ptg;
import org.apache.poi.ss.formula.ptg.RefPtgBase;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.AreaReference;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.usermodel.*;


import java.io.*;
import java.util.*;
import java.util.logging.Logger;

/**
 * Created by Admin on 08.08.2018.
 */
public class Excel {
    static Logger log = Logger.getLogger(MethodHandles.lookup().toString());
    static int savedFilesNum = 0;
    File template;
    FileInputStream fis;
    XSSFWorkbook workbook;
    String[] allNamedCells;
    //SQLite sqLite;
    DBase excelDb;
//------------CONSTRUCTORS
    public Excel() {
        excelDb = new MsExcel();
    }

    public Excel(String template) throws IOException {
        this();
        this.template = new File(template);
        fis = new FileInputStream(this.template);
        workbook = new XSSFWorkbook(fis);
    }
//------------------------
    public String formatExcelName(String regName) {
        return regName.replace(":", "_");
    }

    private final static String getFileDateFormat(ArchiveClock a) {
        switch (a.getArchiveMode()) {
            case HOUR:
                return String.format(Locale.ENGLISH, "%02d%02d%02d%02d", a.getRtClock().day, a.getRtClock().mon, a.getRtClock().year,
                        a.getRtClock().hour);
            case DAY:
                return String.format(Locale.ENGLISH, "%02d%02d%02d", a.getRtClock().day, a.getRtClock().mon, a.getRtClock().year);
            case MONTH:
                return String.format(Locale.ENGLISH, "%02d%02d", a.getRtClock().mon, a.getRtClock().year);
            case TWOMIN:
                return String.format(Locale.ENGLISH, "%02d%02d%02d%02d%02d", a.getRtClock().day, a.getRtClock().mon, a.getRtClock().year,
                        a.getRtClock().hour, a.getRtClock().min);
            default:
                return " ";
        }
    }

    public final static String createFileName(ArchiveClock start, ArchiveClock end, DeviceHead dh) {
        StringBuffer sb = new StringBuffer();
        switch (dh.getSubTypeDevice()) {
            case "STU-1":
                sb.append('s');
                break;
            case "URG2KM-1":
                sb.append('u');
                break;
            case "STM-1":
                sb.append('m');
                break;
            default:
                sb.append('d');
                break;
        }
        sb.append(String.valueOf(dh.getVersion() / 1000) + "_");
        sb.append(dh.getSerialNumber() + " ");
        sb.append(getFileDateFormat(start) + '_' + getFileDateFormat(end) + ".xlsx");
        return sb.toString();
    }



    public String[] getAllNamedCells() {
        String res[] = new String[workbook.getNumberOfNames()];
        for (int i = 0; i < workbook.getNumberOfNames(); i++) {
            res[i] = workbook.getNameAt(i).getNameName();
        }
        return res;
    }


    public void fillCells(RegisteredValueSet cells) {
        for (RegisteredValue rv : cells) {
            String cellName = formatExcelName(rv.getRegister().getMark());
            XSSFName c = workbook.getName(cellName);
            if (c == null) continue;
            try {
                AreaReference ar = new AreaReference(c.getRefersToFormula());
                CellReference cf = ar.getFirstCell();
//            XSSFSheet sheet = workbook.getSheet(cf.getSheetName());
//            Row row = sheet.getRow(cf.getRow());
//            Cell clee = row.getCell(cf.getCol());
//            System.out.println(sheet+" "+row + clee);
                Cell speccell = workbook.getSheet(cf.getSheetName()).getRow(cf.getRow()).getCell(cf.getCol());
                if (speccell == null)
                    speccell = workbook.getSheet(cf.getSheetName()).getRow(cf.getRow()).createCell(cf.getCol());
                String value = excelDb.convertToDBType(rv.getRegister(), rv.getValue());

                if (value.charAt(0) == '\'' && value.charAt(value.length() - 1) == '\'') {
                    speccell.setCellValue((String) value.substring(1, value.length() - 1));
                } else speccell.setCellValue(new Double(value));
            } catch (Exception e) {
                continue;
            }
            //Row row = sh.getRow(cf.getRow());
        }
    }

    private void copyFormula(Sheet sheet, Cell org, Cell dest) {
        if (org == null || dest == null || sheet == null
                || org.getCellType() != Cell.CELL_TYPE_FORMULA)
            return;
        if (org.isPartOfArrayFormulaGroup())
            return;
        String formula = org.getCellFormula();
        int shiftRows = dest.getRowIndex() - org.getRowIndex();
        int shiftCols = dest.getColumnIndex() - org.getColumnIndex();
        XSSFEvaluationWorkbook workbookWrapper =
                XSSFEvaluationWorkbook.create((XSSFWorkbook) sheet.getWorkbook());
        Ptg[] ptgs = FormulaParser.parse(formula, workbookWrapper, FormulaType.CELL
                , sheet.getWorkbook().getSheetIndex(sheet));
        for (Ptg ptg : ptgs) {
            if (ptg instanceof RefPtgBase) // base class for cell references
            {
                RefPtgBase ref = (RefPtgBase) ptg;
                if (ref.isColRelative())
                    ref.setColumn(ref.getColumn() + shiftCols);
                if (ref.isRowRelative())
                    ref.setRow(ref.getRow() + shiftRows);
            } else if (ptg instanceof AreaPtg) // base class for range references
            {
                AreaPtg ref = (AreaPtg) ptg;
                if (ref.isFirstColRelative())
                    ref.setFirstColumn(ref.getFirstColumn() + shiftCols);
                if (ref.isLastColRelative())
                    ref.setLastColumn(ref.getLastColumn() + shiftCols);
                if (ref.isFirstRowRelative())
                    ref.setFirstRow(ref.getFirstRow() + shiftRows);
                if (ref.isLastRowRelative())
                    ref.setLastRow(ref.getLastRow() + shiftRows);
            }
        }
        formula = FormulaRenderer.toFormulaString(workbookWrapper, ptgs);
        dest.setCellFormula(formula);
    }

    public Map<Sheet, List<Cell>> fillArchiveCells(RegisteredValueSet cells) {
        Map<Sheet, List<Cell>> inserts = new HashMap<>();
        for (RegisteredValue rv : cells) {
            String cellName = formatExcelName(rv.getRegister().getMark());
            XSSFName c = workbook.getName(cellName);
            if (c == null) continue;
            AreaReference ar = null;
            try {
                ar = new AreaReference(c.getRefersToFormula());
            } catch (Exception e) {
                continue;
            }
            CellReference cf = ar.getFirstCell();
            Sheet sheet = workbook.getSheet(cf.getSheetName());
            Cell speccell = sheet.getRow(cf.getRow()).getCell(cf.getCol());
            if (speccell == null)
                speccell = workbook.getSheet(cf.getSheetName()).getRow(cf.getRow()).createCell(cf.getCol());
            String value = excelDb.convertToDBType(rv.getRegister(), rv.getValue());
            if ("NULL".equals(value) || value.equals("'NULL'")) value = "'НетДанных'";
            if (value.charAt(0) == '\'' && value.charAt(value.length() - 1) == '\'') {
                speccell.setCellValue(value.substring(1, value.length() - 1));
            } else speccell.setCellValue(new Double(value));
            List<Cell> l = inserts.get(sheet);
            if (l == null) {
                l = new LinkedList<>();
                inserts.put(sheet, l);
            }
            l.add(speccell);
            //Row row = sh.getRow(cf.getRow());
        }
        return inserts;
    }

    public void insertCells(RegisteredValueSet cells) {
        Map<Sheet, List<Cell>> inserts = fillArchiveCells(cells);
//        FormulaEvaluator evaluator = workbook.getCreationHelper().createFormulaEvaluator();
        for (Map.Entry<Sheet, List<Cell>> pair : inserts.entrySet()) {
            pair.getKey().shiftRows(pair.getValue().get(0).getRowIndex() + 1, pair.getKey().getLastRowNum(), 1);
            Row oldRow = pair.getKey().getRow(pair.getValue().get(0).getRowIndex());
            Row newRow = pair.getKey().createRow(pair.getValue().get(0).getRowIndex() + 1);
            CellStyle newCellStyle = workbook.createCellStyle();
            for (int i = 0; i < oldRow.getLastCellNum(); i++) {
                Cell newCell = newRow.createCell(i);
                newCellStyle.cloneStyleFrom(oldRow.getCell(i).getCellStyle());
                newCell.setCellStyle(newCellStyle);
                if (oldRow.getCell(i).getCellComment() != null)
                    newCell.setCellComment(oldRow.getCell(i).getCellComment());
                newCell.setCellType(oldRow.getCell(i).getCellType());
                switch (oldRow.getCell(i).getCellType()) {
                    case Cell.CELL_TYPE_BLANK:
                        newCell.setCellValue(oldRow.getCell(i).getStringCellValue());
                        break;
                    case Cell.CELL_TYPE_BOOLEAN:
                        newCell.setCellValue(oldRow.getCell(i).getBooleanCellValue());
                        break;
                    case Cell.CELL_TYPE_ERROR:
                        newCell.setCellErrorValue(oldRow.getCell(i).getErrorCellValue());
                        break;
                    case Cell.CELL_TYPE_FORMULA:
                        //newCell.setCellFormula(oldRow.getCell(i).getCellFormula());
                        copyFormula(pair.getKey(), oldRow.getCell(i), newCell);
//                        evaluator.evaluate(newCell);
                        break;
                    case Cell.CELL_TYPE_NUMERIC:
                        newCell.setCellValue(oldRow.getCell(i).getNumericCellValue());
                        break;
                    case Cell.CELL_TYPE_STRING:
                        newCell.setCellValue(oldRow.getCell(i).getRichStringCellValue());
                        break;
                }
            }
//            for(Cell c: pair.getValue()){
//                if(c == null)continue;
//                CellStyle newCellStyle = workbook.createCellStyle();
//                Cell newCell = pair.getKey().getRow(c.getRowIndex() + 1).createCell(c.getColumnIndex());
//                newCellStyle.cloneStyleFrom(c.getCellStyle());
//                newCell.setCellStyle(newCellStyle);
//                if(c.getCellComment() != null)newCell.setCellComment(c.getCellComment());
//                newCell.setCellType(c.getCellType());
//                switch (c.getCellType()) {
//                    case Cell.CELL_TYPE_BLANK:
//                        newCell.setCellValue(c.getStringCellValue());
//                        break;
//                    case Cell.CELL_TYPE_BOOLEAN:
//                        newCell.setCellValue(c.getBooleanCellValue());
//                        break;
//                    case Cell.CELL_TYPE_ERROR:
//                        newCell.setCellErrorValue(c.getErrorCellValue());
//                        break;
//                    case Cell.CELL_TYPE_FORMULA:
//                        newCell.setCellFormula(c.getCellFormula());
//                        break;
//                    case Cell.CELL_TYPE_NUMERIC:
//                        newCell.setCellValue(c.getNumericCellValue());
//                        break;
//                    case Cell.CELL_TYPE_STRING:
//                        newCell.setCellValue(c.getRichStringCellValue());
//                        break;
//                }
//            }
        }

    }

    public void recalculateAllFormulas() {
        XSSFFormulaEvaluator.evaluateAllFormulaCells(workbook);
    }


    public String save(ArchiveClock start, ArchiveClock end, DeviceHead hd) {
//        String fileName;
//        savedFilesNum++;
        //String fileName = template.getAbsolutePath();
        //fileName = fileName.substring(0,fileName.lastIndexOf('.')) + savedFilesNum + fileName.substring(fileName.lastIndexOf('.'), fileName.length());
        String fileName = createFileName(start, end, hd);
        try {
            FileOutputStream fos = new FileOutputStream(fileName);
            workbook.write(fos);
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return fileName;
    }

    public void openExcelFile(String fileName) throws IOException {
        if (java.awt.Desktop.isDesktopSupported()) {
            java.awt.Desktop desktop = java.awt.Desktop.getDesktop();
            desktop.open(new File(fileName));
        }
    }


    public void close() throws IOException {
        workbook.close();
        fis.close();
    }


//    class CellAddress{
//        XSSFSheet sheet;
//        String column;
//        int row;
//        CellAddress(XSSFName cl){
//            String cell = cl.getRefersToFormula();
//            String array[] = cell.split("[!$]");
//            if(array.length != 4)throw new IllegalArgumentException(cell);
//            sheet = workbook.getSheet(array[0]);
//            column = array[2];
//            row = Integer.parseInt(array[3]);
//        }
//    }

//    public void run() throws IOException {
//        template = new File("Employee.xlsx");
//        fis = new FileInputStream(template);
//        workbook = new XSSFWorkbook(fis);
//        CellAddress ca = null;
//        for(int i = 0; i < workbook.getNumberOfNames(); i++){
//            if(workbook.getNameAt(i).getNameName().equals("ShitHo")){
//                ca = new CellAddress(workbook.getNameAt(i));
//            }
//        }
//        if(ca == null)return;
//        XSSFSheet mySheet = ca.sheet;
//        XSSFRow newRow = mySheet.getRow(ca.row);
//        if(newRow != null){
//            mySheet.shiftRows(ca.row, mySheet.getLastRowNum(), 1);
//        }
//        FileOutputStream os = new FileOutputStream("shit.xlsx");
//        workbook.write(os);
//        os.close();
//        workbook.close();
////        Iterator<Row> rowIterator = mySheet.iterator();
////        while (rowIterator.hasNext()) {
////            Row row = rowIterator.next();
////
////            // For each row, iterate through each columns
////            Iterator<Cell> cellIterator = row.cellIterator();
////            while (cellIterator.hasNext()) {
////
////                Cell cell = cellIterator.next();
////
////                switch (cell.getCellType()) {
////                    case Cell.CELL_TYPE_STRING:
////                        System.out.print(cell.getStringCellValue() + "\t");
////                        break;
////                    case Cell.CELL_TYPE_NUMERIC:
////                        System.out.print(cell.getNumericCellValue() + "\t");
////                        break;
////                    case Cell.CELL_TYPE_BOOLEAN:
////                        System.out.print(cell.getBooleanCellValue() + "\t");
////                        break;
////                    default:
////
////                }
////            }
////            System.out.println("");
////        }
//        fis.close();
//    }

    private Cell getCellByRegister(Register r, XSSFWorkbook workbook) {
        String cellName = formatExcelName(r.getMark());
        XSSFName c = workbook.getName(cellName);
        if (c == null) return null;
        AreaReference ar = null;
        try {
            ar = new AreaReference(c.getRefersToFormula());
        } catch (Exception e) {
            return null;
        }
        CellReference cf = ar.getFirstCell();
        Sheet sheet = workbook.getSheet(cf.getSheetName());
        Cell speccell = sheet.getRow(cf.getRow()).getCell(cf.getCol());
        return speccell;
    }

    public List<RegisteredValueSet> readArchive(String fileName, Set<Register> set) throws IOException, InvalidFormatException {
        //"s4_00001 0904191700_0904191710.xlsx"
        fis = new FileInputStream(fileName);
        XSSFWorkbook workbook = new XSSFWorkbook(fis);
        List<List<RegisteredValue>> m = new ArrayList<>();
        for (Register r : set) {
            Cell cell = getCellByRegister(r, workbook);
            if (cell == null || cell.getCellType() == Cell.CELL_TYPE_BLANK) continue;
            int col = cell.getColumnIndex();
            int row = cell.getRowIndex();
            Sheet sheet = cell.getSheet();
            List<RegisteredValue> list = new ArrayList<>();
            while (true) {
                String s;
                log.fine("" + col + " " + row);
                Row rr = sheet.getRow(row);
                if(rr == null)break;
                Cell cc = rr.getCell(col);
                if(cc == null)break;
                s = cc.toString();
                if (s == null || s.isEmpty()) break;
                if (s.equals("Нет данных")) s = "";
                RegisteredValue rv = new RegisteredValue(r, s);
                list.add(rv);
                row++;
            }
            m.add(list);
        }

        List<RegisteredValueSet> res = new ArrayList<>();
        int size = m.get(0).size();
        for (int listIndex = 0; listIndex < size; listIndex++) {
            RegisteredValueSet rvs = new RegisteredValueSet();
            for (List<RegisteredValue> lrv : m) {
                System.out.println("listIndex: " + listIndex);
                RegisteredValue ll = lrv.get(listIndex);
                rvs.add(ll);
            }
            res.add(rvs);
        }
        workbook.close();
        fis.close();
        return res;
    }
    public RegisteredValueSet readRegisters(String fileName, Set<Register>set) throws IOException {
        fis = new FileInputStream(fileName);
        XSSFWorkbook workbook = new XSSFWorkbook(fis);
        RegisteredValueSet result = new RegisteredValueSet();
        for(Register r: set){
            Cell cell = getCellByRegister(r, workbook);
            if (cell == null || cell.getCellType() == Cell.CELL_TYPE_BLANK) continue;
            String s = String.valueOf(cell.getNumericCellValue());//getStringCellValue();
            if(s == null || s.isEmpty())continue;
            result.add(new RegisteredValue(r, s));
        }
        workbook.close();
        fis.close();
        return result;
    }
}


