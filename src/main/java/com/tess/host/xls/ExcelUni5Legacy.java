package com.tess.host.xls;

/**
 * Created by Admin on 21.04.2019.
 */
public class ExcelUni5Legacy extends Excel {

    public ExcelUni5Legacy() {
        super();
    }

    @Override
    public String formatExcelName(String regName) {
        String[] arry = regName.split(":");
        if(arry[0].equals("AEUMark") || arry[0].equals("EUMark"))return arry[1];
        return super.formatExcelName(regName);
    }
}
