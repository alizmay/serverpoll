package com.tess.host.modbus;

/**
 * Created by Admin on 14.08.2018.
 */
public interface Cancelation {
    boolean isInterrupted();
}
