package com.tess.host.modbus;

/**
 * Created by Admin on 02.08.2018.
 */
public interface WriteEnabled {
    boolean isWriteEnabled();
}
