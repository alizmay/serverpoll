package com.tess.host.modbus;

import com.tess.modbus.register.loader.Converter;
import com.tess.modbus.types.Clock;

/**
 * Created by Admin on 26.10.2018.
 */
public class LogStruct {
//    public static String[] possibleValues = null;
//    public static Converter checkConverter;
    public Clock time;
    public int code;

    public LogStruct(Clock time, int code) {
        this.time = time;
        this.code = code;
    }

    public static class Rec {
        Clock time;
        boolean active;
        public Rec(Clock time, boolean active) {
            this.time = time;
            this.active = active;
        }
    }
}
