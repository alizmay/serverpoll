package com.tess.host.modbus;

import com.tess.dataLinks.Portal;
import com.tess.dataLinks.Portals;
import com.tess.host.xls.Excel;
import com.tess.host.xls.ExcelUni5Legacy;
import com.tess.modbus.*;
import com.tess.modbus.register.*;
import com.tess.modbus.register.loader.AbstractConverter;
import com.tess.modbus.register.loader.Converter;
import com.tess.modbus.types.ArchiveClock;
import com.tess.modbus.types.Clock;
import com.tess.modbus.types.ProgressMonitor;
import jssc.SerialPortException;


import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.ByteBuffer;
import java.util.*;
import java.util.logging.Logger;

/**
 * Created by Admin on 12.07.2018.
 */
public class StuHost implements IStu{
    static Logger log = Logger.getLogger(MethodHandles.lookup().toString());
    private Portal portal;
    private byte netAddress = (byte)1;
    private ModbusRegisterRead mRead;
    private ModbusRegisterWrite mWrite;
    private ModbusArchive mArchive;
    private ModbusDeviceID mId;
    private ModbusCoilWrite mCoilWrite;
    private ModbusCoilRead mCoilRead;
    private ModbusArchiveWrite mArchiveWrite;

    private Registers registers = null;
    private DeviceHead deviceHead = null;
    private WriteEnabled writeEnabled = new WriteEnabled() {
        @Override
        public boolean isWriteEnabled() {
            return false;
        }
    };
    private URI uri;

    private void modbusInit(Portal p, int netAddress) {
        mRead = new ModbusRegisterRead(p, netAddress);
        mWrite = new ModbusRegisterWrite(p, netAddress);
        mArchive = new ModbusArchive(p, netAddress);
        mId = new ModbusDeviceID(p, netAddress);
        mCoilWrite = new ModbusCoilWrite(p, netAddress);
        mCoilRead = new ModbusCoilRead(p, netAddress);
        mArchiveWrite = new ModbusArchiveWrite(p, netAddress);
    }

    public StuHost(){
    }

    public StuHost(Portal p){
        portal = p;
        modbusInit(p, netAddress);
    }

    @Override
    public RegisteredValueSet connect(String uriString) throws IOException {
        boolean isConnected = false;
        try {
            uri = new URI(uriString);
        } catch (URISyntaxException e) {
            throw new IOException(e);
        }
        if(portal != null && this.uri != null) {
            if (Portals.isEqual(this.uri, uri)) {
                isConnected = true;
                log.info("Old connection: " + uri.toString());
            } else portal.close();
        }
        if(!isConnected){
            portal = Portals.portalFactory(uri,logBuilder);
            this.uri = uri;

        }
        Byte na  = Portals.getNetAddress(uri);
        if(na == null)throw new RuntimeException("NetAddress is absent");
        netAddress = na;
        modbusInit(portal, netAddress);
        log.info("New connection: " + uri.toString());
        RegisteredValueSet rvsHead = read();

        return rvsHead;
    }

    public void reset() throws IOException {
        ModbusSystemReset msr = new ModbusSystemReset(portal);
        msr.reset((byte)1);
    }

    public byte getNetAddress() {
        return netAddress;
    }

    public void setNetAddress(byte netAddress) {
        this.netAddress = netAddress;
        mRead.setNetAddress(netAddress);
        mWrite.setNetAddress(netAddress);
        mArchive.setNetAddress(netAddress);
        mId.setNetAddress(netAddress);
        mCoilWrite.setNetAddress(netAddress);
        mCoilRead.setNetAddress(netAddress);
        mArchiveWrite.setNetAddress(netAddress);

    }

    private StringBuilder logBuilder = null;

    @Override
    public void setLogBuilder(StringBuilder logBuilder) {
        this.logBuilder = logBuilder;
        if(portal != null)portal.setLogBuilder(logBuilder);
    }


    private TreeSet<Register> structuredRegistersDetect(TreeSet<Register> into){
        TreeSet<Register> newset = new TreeSet<>();
        TreeSet<Register> rvset = new TreeSet<>(into);
        Iterator<Register>it = rvset.iterator();
        while(it.hasNext()){
            Register r = it.next();
            Register qr = registers.getRegister(r.getGroupMark());
            if(qr != null){
                newset.add(qr);
                it.remove();
            }
        }
        rvset.addAll(newset);
        return rvset;
    }

    private void structuredRegistersProcess(RegisteredValueSet rvs){
        Set<String>groups = registers.getGroupNames();
        RegisteredValueSet newrvs = new RegisteredValueSet();
        Iterator<RegisteredValue> it = rvs.iterator();
        while(it.hasNext()){
            RegisteredValue rv = it.next();
            String s = rv.getRegister().getUnitMark();
            for(String g: groups)if(s.equals(g)){
                String run = rv.getValue();
                RegisteredValueSet res = registers.getRvs(run);
                newrvs.addAll(res);
                it.remove();
            }

        }
        rvs.addAll(newrvs);
    }

    private RegisteredValueSet structuredRegistersPrepareToWrite(RegisteredValueSet into){
        RegisteredValueSet rvs = new RegisteredValueSet(into);
//        Set<String>groups = registers.getGroupNames();
        RegisteredValueSet newrvs = new RegisteredValueSet();
        Iterator<RegisteredValue> it = rvs.iterator();
        while(it.hasNext()){
            RegisteredValue rv = it.next();
            String s = rv.getRegister().getGroupMark();
            Register register = registers.getRegister(s);
            if(register != null){
                RegisteredValue registeredValue = newrvs.getRegisteredValue(register.getMark());
                if(registeredValue == null){
                    registeredValue = new RegisteredValue(register, "");
                    newrvs.add(registeredValue);
                }
                registeredValue.setValue(registeredValue.getValue() + rv.getRegister().getMark()+'='+rv.getValue()+',');
                it.remove();
            }
        }
        rvs.addAll(newrvs);
        return rvs;
    }

    @Override
    public RegisteredValueSet read(TreeSet<Register> in) throws IOException {
        RegisteredValueSet rvs = new RegisteredValueSet();
        TreeSet<Register>coilsSet = new TreeSet<>();
        TreeSet<Register>regSet = new TreeSet<>();
        RegisteredValueSet coil = null, regs = null;
        for(Register r : in)
            if(r.getTypeMapping().toString().contains("COIL"))coilsSet.add(r);
            else regSet.add(r);
        if(coilsSet.size() > 0){
            coil = mCoilRead.readCoil(coilsSet);
            if(coil != null)rvs.addAll(coil);
        }
        if(regSet.size() > 0) {
            TreeSet<Register> set = structuredRegistersDetect(regSet);
            regs  = mRead.readRegister(set);
            structuredRegistersProcess(regs);
            if(regs != null)rvs.addAll(regs);
        }
        return rvs;
    }

    @Override
    public void write(RegisteredValueSet in) throws IOException{
        if(portal == null || netAddress == 0)throw new IOException("It's necessary to create portal at first");
        //writeRegistersToPortal(in);
        RegisteredValueSet into =  structuredRegistersPrepareToWrite(in);
        RegisteredValueSet coilsSet = new RegisteredValueSet();
        Iterator<RegisteredValue> iterator = into.iterator();
        while(iterator.hasNext()){
            RegisteredValue rv = iterator.next();
            if(rv.getRegister().getTypeMapping().toString().contains("COIL")){
                coilsSet.add(rv);
                iterator.remove();
            }
        }
        if(into.size() > 0)mWrite.writeRegister(into);
        if(coilsSet.size() > 0) mCoilWrite.writeCoil(coilsSet);
    }

    private boolean helperWriteArchive(String comm) throws IOException  {
        try {
            boolean m2 = true;
            int srt = comm.indexOf("\"");
            if(srt <= 4)return false;
            String fileName = comm.substring(srt+1, comm.indexOf("\"", srt+1));
            String cmm = comm.split("=")[0].trim();
            ArchiveClock.ArchiveMode am = null;
            if(cmm.equals("ADay"))am = ArchiveClock.ArchiveMode.DAY; //TODO Auto identification type of the archive
            else if(cmm.equals("AHour"))am = ArchiveClock.ArchiveMode.HOUR;
            else if(cmm.equals("AMon"))am = ArchiveClock.ArchiveMode.MONTH;
            else if(cmm.equals("A2Min"))am = ArchiveClock.ArchiveMode.TWOMIN;
            else return false;
            int amd = Integer.parseInt(read("Amd"));
            int amd_day = amd >> 8;
            int amd_hour = amd & 0xFF;
            Registers r = getRegisters();
            Excel exc;
            Set<Register> arc;
            Register date;
            if(m2) {
                arc = r.getSetByGroup("AEUMark");
                exc = new ExcelUni5Legacy(); //TODO Auto identification of the type of the model by the Excel File
                date = r.getRegister("AEUMark:_aDateSh1");
                if(am == ArchiveClock.ArchiveMode.MONTH){
                    Register old = r.getRegister("AEUMark:_aNSTime1");
                    Register mo = new Register("AEUMark:_aNSTime1", old.getAddress(), new TypeMapping("INT"));
                    arc.remove(old);
                    arc.add(mo);
                }
            }else{
                arc = r.getSetByGroup("Archive");
                exc = new Excel(); //TODO Auto identification of the type of the model by the Excel File
                date = r.getRegister("Archive:Datetime");
            }
            System.out.print("Reading arc data from " + fileName + " ...");
            List<RegisteredValueSet> list = exc.readArchive(fileName, arc);
            System.out.println("Ok");
            //for(RegisteredValueSet rvs : list)System.out.println(rvs);
            System.out.print("Reading totals data from " + fileName + " ...");
            RegisteredValueSet totals = exc.readRegisters(fileName, r.getSetByGroup("EUMark"));
            System.out.println("Ok");
            System.out.println(list.size() + " records at total");

//        if(m2 && am == ArchiveClock.ArchiveMode.MONTH){
//            for(RegisteredValueSet rvs : list){
//                RegisteredValue rv = rvs.getRegisteredValue("_aNSTime1");
//                rv.getRegister().getTypeMapping().
//                rv.setValue(Integer.toString(Integer.parseInt(rv.getValue())/2));
//            }
//        }

            System.out.println("Starting to write...");
            for(RegisteredValueSet rvs : list){
                ArchiveClock recordClock = new ArchiveClock(am, amd_day, amd_hour);
                recordClock.setRtClock(new Clock(rvs.getRegisteredValue(date.getMark()).getValue()).getRtClock());
                //recordClock = new ArchiveClock(am, rvs.getRegisteredValue(date.getMark()).getValue());
                recordClock.setAmd(amd_day, amd_hour);
                System.out.print(recordClock.toShortedString() + "...");
                writeArchive(recordClock, rvs);
                System.out.println("Ok");
            }
            System.out.print("Writing totals...");
            if(m2){
                for(RegisteredValue rv :totals) {
                    RegisteredValueSet in = new RegisteredValueSet();
                    in.add(rv);
                    write(in);
                }
            }else write(totals);
            System.out.println("Ok");
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            log.severe(e.getMessage());
            throw new IOException(e.getMessage());
        }
    }

    @Override
    public void writeArchive(ArchiveClock date, RegisteredValueSet in) throws IOException {
        mArchiveWrite.writeArchive(date, in);
    }

    @Override
    public RegisteredValueSet readArchive(ArchiveClock clock, Set<Register> arcSet) throws IOException {
        return mArchive.readArchive(clock, arcSet);
    }

    @Override
    public void timeCorrection(RegisteredValue sysclock, int deviationTimeActivator) throws IOException {
        if(sysclock.getValue() == null)return;
        Clock sysclockClock = new Clock(sysclock.getValue());
        Clock currentClock = new Clock();
        if (Math.abs(sysclockClock.compareTo(currentClock)) > deviationTimeActivator) {
            sysclock.setValue(currentClock.toString());
            RegisteredValueSet timecor = new RegisteredValueSet();
            timecor.add(sysclock);
            mWrite.writeRegister(timecor);
//                timecor = modbusRegisterRead.readRegister(timecor.getSet());
//                log.append("DEBUG: Actual sysclock = " + timecor.getRegisteredValue("sysclock").getValue() + '\n');
            log.info("sysclock has been corrected to " + currentClock.toString());
        }

    }

    @Override
    public void open(String pathLocal, String fragment) throws IOException {
         portal.open(pathLocal, fragment);
    }

    @Override
    public Portal getPortal(){
        return portal;
    }

    @Override
    public void setPortal(Portal p) {
        portal = p;
    }

    @Override
    public RegisteredValueSet read() throws IOException {
        RegisteredValueSet dh = mId.readID();
        if(deviceHead == null)deviceHead = new DeviceHead(dh);
            else deviceHead.parseDeviceRvs(dh);
        deviceHead.recognize();
        if(registers == null) {
            RegistersFactory.setParserTypeName(deviceHead);
            registers = RegistersFactory.getInstance(dh.getRegisteredValue("DeviceHead:Device").getValue());
        }
        return dh;
    }

    @Override
    public Registers getRegisters() {
        return registers;
    }

    @Override
    public void setRegisters(Registers r) {
        this.registers = r;
    }

    public void write(String expression) throws IOException {
        if(registers == null)throw new NullPointerException("registers is empty");
        if(helperWriteArchive(expression))return ;
        String[] array = expression.split("[=,]");
        if((array.length & 0x01) == 1) throw new NumberFormatException(expression);

        RegisteredValueSet rvs = new RegisteredValueSet();
        for (int i = 0; i < array.length; i += 2) {
            Register r = registers.getRegister(array[i].trim());
            if(r == null)continue;
            rvs.add(new RegisteredValue(r, array[i + 1].trim()));
        }
        write(rvs);
    }



//    public RegisteredValueSet readRvs(String ...marks) throws IOException {
//        if(registers == null)throw new NullPointerException("registers is empty");
//        TreeSet<Register> r = registers.get(marks);
//        if(r == null || r.size() != marks.length)throw new NoSuchElementException(Arrays.toString(marks));
//        RegisteredValueSet rvs = read(r);
//        return rvs;
//    }

    public String read(String val) throws IOException{
        if(registers == null)throw new NullPointerException("registers is empty");
        TreeSet<Register>tr = registers.get(val);
        RegisteredValueSet rvs = read(tr);
        return rvs.getRegisteredValue(0).getValue();
    }

    public int bslEntry() throws IOException {
        ModbusBootloadEntrance mbe = new ModbusBootloadEntrance(portal);
        return mbe.bootloaderEntrance(netAddress);
    }

    public String[] read(String ... marks) throws IOException {
        RegisteredValueSet rvs = read(registers.get(marks));
        String res[] = new String[rvs.size()];
        for (int i = 0; i < res.length; i++) {
            res[i] = rvs.getRegisteredValue(marks[i]).getValue();
        }
        return res;
    }

    public List<RegisteredValueSet> readRepeating(TreeSet<Register> set, int times, int offset) throws IOException {
        return mRead.readRegistersRepeat(set, times, offset);
    };

    private ProgressMonitor progressMonitor;

    public void setProgressMonitor(ProgressMonitor  progressMonitor){
//        if(mArchive!=null)mArchive.setProgressMonitor(progressMonitor);
        this.progressMonitor = progressMonitor;
    }

    private Cancelation cancelation;

    public void setCancelation(Cancelation cancelation){
        this.cancelation = cancelation;
    }

    public List<RegisteredValueSet> read(ArchiveClock from, ArchiveClock to) throws IOException {
        TreeSet<Register>set = registers.getSetByGroup("Archive");
        if(set.size() == 0)throw new IOException("No 'Archive' registered values");
        List<RegisteredValueSet>list = new ArrayList<>();
        ArchiveClock f = from, t = to;
        boolean direction = false;
        if(from.asLong() < to.asLong())direction = true;
        ArchiveClock i = f;
        if(progressMonitor != null){
            progressMonitor.init(0, (int)(t.asLong() - f.asLong())/t.getIntervalMinutes());
        }
//        try {
            do{
               RegisteredValueSet rvs = readArchive(i, set);
               list.add(rvs);
               if(i.asLong() == t.asLong())break;
               i.increment(direction);
               if(progressMonitor != null)progressMonitor.step(i.toString());
               if(cancelation!=null)if(cancelation.isInterrupted())break;
            }while(true);
 //       } catch (IOException ifnored) {}
        return list;
    }

    public boolean isWriteEnabled() {
        for(String s : deviceHead.getVersionSystem().getServices())if(s.equals("write_service"))return true;
        return false;
    }

    public DeviceHead getDeviceHead() {
        return deviceHead;
    }

    public void setDeviceHead(DeviceHead deviceHead){
        this.deviceHead = deviceHead;
    }

    @Override
    public void close() throws IOException{
        if(portal != null)portal.close();
    }

    public Map<Integer, List<LogStruct.Rec>> analizeLogList(List<LogStruct> list) {
        Map<Integer, List<LogStruct.Rec>>m = new HashMap<>();
        Clock first = list.get(list.size() - 1).time;
        for(int i = 0; i < 32 ; i++){
            m.put(i, new LinkedList<LogStruct.Rec>());
            m.get(i).add(new LogStruct.Rec(first, false));
        }
        int past = 0;
        for (int i1 = list.size() - 1; i1 >=0 ; i1--) {
            LogStruct lg = list.get(i1);
            int diff = past ^ lg.code;
            if (diff == 0x40000000) continue; //SYS omit
            for (int i = 0x1, y = 0; i != 0; i <<= 1, y++) {
                if ((diff & i) != 0) {
                    boolean active = (lg.code & i) != 0;
                    m.get(y).add(new LogStruct.Rec(lg.time, active));
                }
            }
            past = lg.code;
        }
        return m;
    }


    public void anal(List<LogStruct>logStructList, List<RegisteredValueSet>archive, ArchiveClock.ArchiveMode mode, Register checkRegister){
        if(archive.size() <= 1)return; //TODO ?
        assert(logStructList.size() > 1);
        Collections.reverse(logStructList);

        Converter c = checkRegister.getTypeMapping().getConverter();
        c.UniToBin(ByteBuffer.allocate(4), 0," ");
        String[]psv = c.getPossibleValues();
  //      boolean incr = true;
        //TODO detection of the direction of the archive should do at level up, not here
        ArchiveClock reaperEnd, reaperStart = new ArchiveClock(mode, archive.get(0).getRegisteredValue("Archive:Datetime").getValue());
//        ArchiveClock reaperEnd   = new ArchiveClock(mode, archive.get(1).getRegisteredValue("Archive:Datetime").getValue());
//        incr = reaperStart.asLong() < reaperEnd.asLong();
//        if(incr == false){
//            Collections.reverse(archive);
//            reaperStart = new ArchiveClock(mode, archive.get(0).getRegisteredValue("Archive:Datetime").getValue());
////            reaperEnd   = new ArchiveClock(mode, archive.get(1).getRegisteredValue("Archive:DateTime").getValue());
//        }
        int archiveIndex = 0, lsIndex = logStructList.size() - 1;

        while(reaperStart.asLong() < logStructList.get(0).time.asLong()){
            archiveIndex++;
            if(archiveIndex >= archive.size())
                return;
            reaperStart = new ArchiveClock(mode, archive.get(archiveIndex).getRegisteredValue("Archive:Datetime").getValue());
        }

        while(logStructList.get(lsIndex).time.asLong() > reaperStart.asLong()){
            lsIndex--;
            if(lsIndex < 0)return;
        }

        double[] list32 = new double[32];
        Arrays.fill(list32, 0);
        int checkPlank = logStructList.get(lsIndex).code;
        Clock timePlank = reaperStart;
        //archiveIndex++;
        reaperEnd = new ArchiveClock(reaperStart);
        reaperEnd.increment(true);
        lsIndex++;
        int checkActual = logStructList.get(lsIndex).code;
        Clock timeActual = logStructList.get(lsIndex).time;
        do {
//            reaperEnd = new ArchiveClock(mode, archive.get(archiveIndex).getRegisteredValue("Archive:Datetime").getValue());
            double time;
            if(reaperEnd.asLong() > timeActual.asLong()){
                time = (double)(timeActual.getRtClock().sec - timePlank.getRtClock().sec)/60.0;
                time = timeActual.asLong() - timePlank.asLong() + time;

                if(time < 0)time = 0;
                pace(list32, checkPlank, time);
                checkPlank = checkActual;
                timePlank = timeActual;
                lsIndex++;
                if(lsIndex >= logStructList.size())break;
                checkActual = logStructList.get(lsIndex).code;
                timeActual = logStructList.get(lsIndex).time;
            }else{
                time = (reaperEnd.getRtClock().sec - timePlank.getRtClock().sec)/60.0;
                time = reaperEnd.asLong() - timePlank.asLong() + time;
                pace(list32, checkPlank, time);
                archive.get(archiveIndex).addAll(getPacedRvs(list32, psv));
                //timePlank = reaperEnd;
                archiveIndex++;
                if(archiveIndex >= archive.size())break;
                reaperEnd = new ArchiveClock(mode, archive.get(archiveIndex).getRegisteredValue("Archive:Datetime").getValue());
                timePlank = new ArchiveClock(reaperEnd);
                reaperEnd.increment(true);
                Arrays.fill(list32, 0);
            }
        }while(true);

//        if(incr == false)Collections.reverse(archive);

    }

    private RegisteredValueSet getPacedRvs(double[] list32, String[] psv) {
        RegisteredValueSet rvs = new RegisteredValueSet();
        for(int i = 0; i < list32.length; i++){
            rvs.add(new RegisteredValue(new Register("So:" + psv[i], -1, new TypeMapping("STR20")),
                    String.format(Locale.ENGLISH,"%.3f",list32[i])));
        }
        return rvs;
    }

    private void pace(double[] list32, int checkActual, double time) {
        for(int i = 0; i < list32.length; i++){
            if((checkActual & (0x01 << i)) != 0)list32[i] += time;
        }
    }

    public List<LogStruct> readLog(TreeSet<Register> logtree, Clock scoop ) throws IOException {
        final int MAX_RECORDS = 1000;
        int top = Integer.parseInt(read("Logger:Top"));
        System.out.println(top);
        //TreeSet<Register> logtree = registers.get("Logger:Time, Logger:Check");
        if (logtree == null) return null;
        int startAddress = logtree.first().getAddress();
        int sizeLog = logtree.last().getAddress() + logtree.last().getTypeMapping().getSize() - startAddress;
        int step = (230 / 2) / sizeLog;
        List<RegisteredValueSet> list = new ArrayList<>();
        List<LogStruct> lls = null;
        boolean brk = false;
        Clock lastClock = null;
        ByteBuffer bb = ByteBuffer.allocate(4);
        lls = new ArrayList<>();
        String[] arr = read("Measure:sysstate","Measure:sysclock");
        registers.getRegister("Logger:Check").getTypeMapping().transformation(bb, 0, arr[0]);
        AbstractConverter.swap(bb, 0, 2);
        lls.add(new LogStruct(new Clock(arr[1]), bb.getInt(0)));

        for (int i = top - step; i != top && !brk; i -= step) {
            list.clear();
            if (i < 0) {
                list.addAll(readRepeating(logtree, i + step, 0 + startAddress));
                step = -i;
                i = MAX_RECORDS - step;
            }
            int address = i * sizeLog;
            list.addAll(readRepeating(logtree, step, address));
            for (int i1 = list.size() - 1; i1 > 0; i1--) {
                RegisteredValueSet rvs = list.get(i1);
                System.out.print(rvs.getRegisteredValue("Logger:Time").getValue() + " ");
                System.out.println(rvs.getRegisteredValue("Logger:Check").getValue());
                String date = rvs.getRegisteredValue("Logger:Time").getValue();
                if(date == null){
                    brk = true; break;
                }
                Clock present = new Clock(date);
                if(present.asLong() <= scoop.asLong()){
                    brk = true; continue;
                }
                if (lastClock != null) {
                    if (present.asLong() > lastClock.asLong()) {
                        continue;
                    }
                }else {
//                    LogStruct.checkConverter =  rvs.getRegisteredValue("Logger:Check").getRegister().getTypeMapping().getConverter();
//                    rvs.getRegisteredValue("Logger:Check").getRegister().getTypeMapping().transformation(ByteBuffer.allocate(4), 0," ");
//                    LogStruct.possibleValues = rvs.getRegisteredValue("Logger:Check").getRegister().getTypeMapping().getPossibleValues();
                }

                rvs.getRegisteredValue("Logger:Check").getRegister().getTypeMapping().transformation(bb, 0, rvs.getRegisteredValue("Logger:Check").getValue());
                AbstractConverter.swap(bb, 0, 2);
                int integ = bb.getInt(0);
                lls.add(new LogStruct(present, integ));
                lastClock = present;
            }
//            if(scoop != null && present.asLong() < scoop.asLong()){
//                brk = true; break;
//            }

        }
        return lls;
    }
}

