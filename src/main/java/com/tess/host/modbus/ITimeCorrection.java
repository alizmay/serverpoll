package com.tess.host.modbus;

import com.tess.modbus.register.RegisteredValue;

import java.io.IOException;

/**
 * Created by Admin on 03.09.2019.
 */
public interface ITimeCorrection {
    void timeCorrection(RegisteredValue sysclock, int deviationTimeActivator) throws IOException;
}
