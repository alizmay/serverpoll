package com.tess.host.modbus;

import com.tess.dataLinks.ILogBuilder;
import com.tess.dataLinks.Portal;
import com.tess.modbus.register.Register;
import com.tess.modbus.register.RegisteredValueSet;
import com.tess.modbus.register.Registers;
import com.tess.modbus.types.ArchiveClock;

import java.io.Closeable;
import java.io.IOException;
import java.net.URI;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created by Admin on 03.09.2019.
 */
public interface IStu extends ILogBuilder, ITimeCorrection, Closeable {
    void open(String pathLocal, String fragment) throws IOException;
    RegisteredValueSet connect(String uriString) throws IOException;
    RegisteredValueSet read(TreeSet<Register> in) throws IOException;
    void write(RegisteredValueSet in) throws IOException;
    void writeArchive(ArchiveClock date, RegisteredValueSet in) throws IOException;
    RegisteredValueSet readArchive(ArchiveClock clock, Set<Register> arcSet) throws IOException;
    Portal getPortal();
    void setPortal(Portal p);
    RegisteredValueSet read() throws IOException;
    Registers getRegisters();
    void setRegisters(Registers r);
}
