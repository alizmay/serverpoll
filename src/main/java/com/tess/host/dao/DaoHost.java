package com.tess.host.dao;

import com.tess.dao.DataBase;
import com.tess.modbus.register.Register;
import com.tess.modbus.register.RegisteredValueSet;

import java.io.Closeable;
import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.sql.SQLException;
import java.util.*;
import java.util.logging.Logger;

/**
 * Created by Admin on 12.07.2018.
 */
public class DaoHost implements Closeable {
    static Logger log = Logger.getLogger(MethodHandles.lookup().toString());
    QueueMonitor queueMonitor;
    DataBase dbase;

    public DaoHost(DataBase database){
        this.dbase = database;
        queueMonitor = new QueueMonitor(this);
    }

    public void connect() throws IOException {
        dbase.init();
        queueMonitor.start();
    }

    public void createTables(String nameTable, Set<Register>table) throws SQLException {
        Map<String, Set<Register>> m = RegisteredValueSet.getSetGrouped(table);
        for(Map.Entry<String, Set<Register>> pair: m.entrySet()) {
            String query = String.format("SELECT COUNT(*) FROM " + nameTable + pair.getKey());
            try {
                dbase.rawQuery(query);
            } catch (SQLException e) {
                dbase.createTable(nameTable, pair.getValue());
            }

        }
    }

    @Override
    public void close() throws IOException {
        try {
            dbase.close();
            queueMonitor.interrupt();
            queueMonitor.join();
        } catch (Exception e) {
            throw new IOException(e.getCause());
        }
    }

//    public void createTable(String nameTable, Set<Register>table) throws SQLException {
//        Map<String, Set<Register>> m = RegisteredValueSet.getSetGrouped(table);
//        for(Map.Entry<String, Set<Register>> pair: m.entrySet())
//            dbase.createTable(nameTable, pair.getValue());
//    }

    public String write(String nameTable, RegisteredValueSet rvs) {
        return dbase.write(nameTable, rvs, null);
    }

    public String write(String nameTable, RegisteredValueSet rvs, RegisteredValueSet keys){
        return dbase.write(nameTable, rvs, keys);
    }

    public synchronized String read(String nameTable, List<RegisteredValueSet>list, Set<Register>set) {
        return dbase.read(list, nameTable, set, null);
    }

    public synchronized  String read(String nameTable, List<RegisteredValueSet>list, Set<Register>set, RegisteredValueSet keys){
        return dbase.read(list, nameTable, set, keys);
    }

    public synchronized void add(RequestOwner owner, Object o, RegisteredValueSet rvs){
        queueMonitor.add(owner, o, rvs);
    }
}
