package com.tess.host.dao;

import com.tess.modbus.register.RegisteredValueSet;

import java.util.List;

/**
 * Created by Admin on 12.07.2018.
 */
public interface RequestOwner {
    void readBack(List<RegisteredValueSet> list);
    void log(String string);
    String getTableName();
}
