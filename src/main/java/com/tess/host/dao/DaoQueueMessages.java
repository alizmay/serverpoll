package com.tess.host.dao;

import com.tess.host.queuemessages.QueueMessageAction;
import com.tess.host.queuemessages.QueueMessages;
import com.tess.modbus.register.Register;
import com.tess.modbus.register.RegisteredValueSet;

import java.lang.invoke.MethodHandles;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Admin on 26.08.2019.
 */
public class DaoQueueMessages extends QueueMessages<DataQM>{
    static Logger log = Logger.getLogger(MethodHandles.lookup().toString());
    private DaoHost dao = null;

    public DaoQueueMessages(DaoHost dao) {
        super("DaoQueueMessages");
        this.dao = dao;
    }

    @Override
    public void action() {
        DataQM dqm = getQueue().poll();
        if(dqm.data instanceof RegisteredValueSet) {
            String result = dao.write(dqm.owner.getTableName(), (RegisteredValueSet)dqm.data, dqm.keys);
            if (log.getParent().getLevel() == Level.FINE)
                dqm.owner.log(result);
        }else
        if(dqm.data instanceof Set){
            List<RegisteredValueSet> list = new ArrayList<>();
            String result = dao.read(dqm.owner.getTableName(),list, (Set<Register>)dqm.data, dqm.keys); //TODO remove sync from dao read
            dqm.owner.readBack(list);
            if (log.getParent().getLevel() == Level.FINE)
                dqm.owner.log(result);
        }
    }
}
