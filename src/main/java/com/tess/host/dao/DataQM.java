package com.tess.host.dao;

import com.tess.modbus.register.RegisteredValueSet;

/**
 * Created by Admin on 12.07.2018.
 */
public class DataQM {
    RequestOwner owner;
    Object data;
    RegisteredValueSet keys;

    public DataQM(RequestOwner owner, Object data, RegisteredValueSet keys) {
        this.owner = owner;
        this.data = data;
        this.keys = keys;
    }
}
