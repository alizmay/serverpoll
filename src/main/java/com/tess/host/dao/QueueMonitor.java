package com.tess.host.dao;

import com.tess.modbus.register.Register;
import com.tess.modbus.register.RegisteredValueSet;

import java.lang.invoke.MethodHandles;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Admin on 23.06.2018.
 */
public class QueueMonitor extends Thread {
    static Logger log = Logger.getLogger(MethodHandles.lookup().toString());
    private DaoHost dao;
    private ConcurrentLinkedQueue<DataQM> queue = new ConcurrentLinkedQueue<>();

    public QueueMonitor(DaoHost dao) {
        super("QueueMonitor");
        this.dao = dao;
    }

    private Object lock = new Object();
    protected void msdelay(long ms) throws InterruptedException {
        synchronized (lock){
            lock.wait(ms);
        }
    }

    @Override
    public void run() {
        try {
            while(!isInterrupted()){
                while(queue.size() > 0){
                    DataQM dqm = queue.poll();
                    if(dqm.data instanceof RegisteredValueSet) {
                        String result = dao.write(dqm.owner.getTableName(), (RegisteredValueSet)dqm.data, dqm.keys);
                        if (log.getParent().getLevel() == Level.FINE)
                            dqm.owner.log(result);
                    }else
                    if(dqm.data instanceof Set){
                        List<RegisteredValueSet> list = new ArrayList<>();
                        String result = dao.read(dqm.owner.getTableName(),list, (Set<Register>)dqm.data, dqm.keys); //TODO remove sync from dao read
                        dqm.owner.readBack(list);
                        if (log.getParent().getLevel() == Level.FINE)
                            dqm.owner.log(result);
                    }
                }
                msdelay(1000);
//                System.out.println("QM");
    //            try {
    //              Thread.sleep(1000);
    //            }catch (InterruptedException e) {
    //                interrupt();
    //                break;
    //            }
            }
        } catch (InterruptedException ignored) { }
        log.info("QueueMonitor process is interrupted");
    }


    public void add(RequestOwner owner, Object data, RegisteredValueSet keys) {
        queue.add(new DataQM(owner, data, keys));
    }


}
