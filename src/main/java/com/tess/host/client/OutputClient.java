package com.tess.host.client;

import com.tess.modbus.register.RegisteredValueSet;
import com.tess.modbus.types.ArchiveClock;

/**
 * Created by Admin on 26.08.2019.
 */
public interface OutputClient {
    void pushArchive(RequestOwner owner, ArchiveClock from, RegisteredValueSet united);
    void pushCurrent(RequestOwner owner, RegisteredValueSet set);

//    void sendMessage(RequestOwner owner, RegisteredValueSet rvs, RegisteredValueSet keys);
}
