package com.tess.host.client;

import com.tess.ServerPollManager;
import com.tess.dao.DBase;
import com.tess.dao.DataBase;
import com.tess.dao.MsSQLServer2000;
import com.tess.modbus.DeviceHead;
import com.tess.modbus.register.Register;
import com.tess.modbus.register.RegisteredValue;
import com.tess.modbus.register.RegisteredValueSet;
import com.tess.modbus.register.Registers;
import com.tess.modbus.types.ArchiveClock;
import com.tess.modbus.types.Clock;
import com.tess.mqtt.DataMM;
import com.tess.mqtt.MqttClientStu;
import org.eclipse.paho.client.mqttv3.MqttException;

import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.sql.SQLException;
import java.util.Locale;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Admin on 27.08.2019.
 */
public class ClientTTF3 extends AbstractClient {
    final static java.util.logging.Logger log = Logger.getLogger(MethodHandles.lookup().toString());
    protected DaoClientStu dcs = null;
    protected MqttClientStu mcs = null;
    private boolean mqqtEnable = true;
    public ClientTTF3(){
        super("POLLERTTF");
    }

    public void init() throws IOException, SQLException, MqttException {
        if(dcs == null)dcs = new DaoClientStu(new MsSQLServer2000()) {
            @Override
            protected void createTables() throws IOException {
                createTable("", getRegisters().getSetByGroup("Cppd"));
                createTable("", getRegisters().getSetByGroup("SingleUrgUnits"));
            }

            @Override
            public void pushArchive(RequestOwner owner, ArchiveClock from, RegisteredValueSet united) {
                RegisteredValueSet cppd = getCppd(from, united);
                if(cppd != null) {
                    RegisteredValueSet keys = cppd.pickOut("Date_, Drill");
                    sendMessage(owner, cppd, keys);
                }
            }

            @Override
            public void pushCurrent(RequestOwner owner, RegisteredValueSet set) {
                set = set.getGrouped().get("SingleUrgUnits");
                RegisteredValueSet keys = set.pickOutRemove("_ID, Drill");
                sendMessage(owner, set, keys);
            }
        };


        setInputClient(dcs);
        addOutputClient(dcs);

        if(mqqtEnable) {

            try {
                if(mcs == null)mcs = new MqttClientStu() {//"tcp://10.0.0.16:1883")
                    @Override
                    public void pushArchive(RequestOwner owner, ArchiveClock from, RegisteredValueSet united) {
                        if (!mcs.isConnected()) return;
                        RegisteredValueSet cppd = getCppd(from, united);
                        if(cppd == null)return;
                        String drill = "CPPDID_" + united.getRegisteredValue("Drill").getValue() +  '/';
                        String date = cppd.getRegisteredValue("Cppd:Date_").getValue();
                        Clock.SRTClock rt  = new Clock(date).getRtClock();
                        String rtString = String.format(Locale.ENGLISH, "%02d%02d%02d%02d",
                                rt.day,rt.mon,rt.year,rt.hour);
                        drill += "archive/a"+rtString + '/';
                        sendMessage(owner, drill, cppd);
                    }

                    @Override
                    public void pushCurrent(RequestOwner owner, RegisteredValueSet united) {
                        if (!mcs.isConnected()) return;
                        RegisteredValueSet mqttSingle = united.getGrouped().get("SingleUrgUnits");
    //                    RegisteredValueSet dh = set.getGrouped().get("DeviceHead");
                        String id = "CPPDID_" + united.getRegisteredValue("SerNo").getValue() + '/';
                        id += "current/";
                        //RegisteredValueSet mqttSingle = set.getGrouped().get("SingleUrgUnits");
                        mqttSingle.pickOutRemove("LastSession");
                        sendMessage(owner, id, mqttSingle);
                    }
                };
                addOutputClient(mcs);
            } catch (Exception e) {
                mqqtEnable = false;
                log.log(Level.SEVERE, "MQTT Client: ", e);
            }
        }
    }

//    @Override
//    public void pushArchive(RequestOwner owner, ArchiveClock from, RegisteredValueSet united) {
////        RegisteredValueSet cppd = getCppd(from, united);
//        super.pushArchive(owner, from, cppd);
//    }

    private RegisteredValueSet getCppd(ArchiveClock from, RegisteredValueSet unitedGroups) {
        String q1 = unitedGroups.getRegisteredValue("q1").getValue();
        if(q1 == null || q1.contains("NULL"))return null;
        RegisteredValueSet cppd = registers.getRvsByGroup("Cppd");
        cppd.getRegisteredValue("Date_").setValue(from.toString());
        cppd.copy(unitedGroups, "Drill, T1, P1, AccV, q1Hour = q1");
        int upstate;
        try {
            int upstate1 = Integer.parseInt(unitedGroups.getRegisteredValue("Archive:stateDuration1").getValue());
            int upstate2 = Integer.parseInt(unitedGroups.getRegisteredValue("Archive:stateDuration2").getValue());
            upstate = upstate1 < upstate2 ? upstate1 : upstate2;
        }catch(Exception ignored){upstate = 0;}
        cppd.getRegisteredValue("Downtime").setValue(String.valueOf(upstate));
        return cppd;
    }

    @Override
    public void close() throws IOException {
        if(mcs!=null)mcs.close();
        if(dcs!=null)dcs.close();
    }

    public void setDao(DaoClientStu daoClientStu){
        dcs = daoClientStu;
    }

    public void setMqqt(MqttClientStu mqttClientStu){
        mcs = mqttClientStu;
    }

    public DataBase getDatabase() {
        return dcs.dbase;
    }

    public void setMqqtEnable(boolean mqqtEnable) {
        this.mqqtEnable = mqqtEnable;
    }
}
