package com.tess.host.client;

import com.tess.modbus.register.Register;
import com.tess.modbus.register.RegisteredValue;
import com.tess.modbus.register.RegisteredValueSet;

import java.util.List;
import java.util.Set;

/**
 * Created by Admin on 27.08.2019.
 */
public interface InputClient {
    void requestMessage(RequestOwner owner, Set<Register> set, RegisteredValueSet registeredValueSet);
}
