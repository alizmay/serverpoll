package com.tess.host.client;

import com.tess.modbus.register.Register;
import com.tess.modbus.register.RegisteredValueSet;
import com.tess.modbus.register.Registers;
import com.tess.modbus.types.ArchiveClock;

import java.io.Closeable;
import java.util.Set;

/**
 * Created by Admin on 27.08.2019.
 */
public interface ClientClass extends OutputClient, Closeable{
    Registers getRegisters();
    void requestMessage(RequestOwner owner, Set<Register> set, RegisteredValueSet keys);

}
