package com.tess.host.client;

import com.tess.dao.DataBase;
import com.tess.host.dao.*;
import com.tess.host.queuemessages.QueueMessages;
import com.tess.modbus.register.Register;
import com.tess.modbus.register.RegisteredValueSet;
import com.tess.modbus.register.Registers;
import com.tess.modbus.types.ArchiveClock;
import org.eclipse.paho.client.mqttv3.MqttException;


import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Admin on 27.08.2019.
 */
public abstract class DaoClientStu implements InputClient, OutputClient{
    final static Logger log = Logger.getLogger(MethodHandles.lookup().toString());
    QueueMessages<DataQM> daoMessagesThread = null;
    final DataBase dbase;


    public DaoClientStu(DataBase db) throws IOException {
        this.dbase = db;
        connect();
        log.info("Dao client is connected");
        daoMessagesThread = new QueueMessages<DataQM>("DaoMessagesThread") {
            @Override
            public void action() {
                DataQM dqm = getQueue().poll();
                if(dqm.data instanceof RegisteredValueSet) {
                    String result = dbase.write(dqm.owner.getTableName(), (RegisteredValueSet)dqm.data, dqm.keys);
                    if (log.getParent().getLevel() == Level.FINE)
                        dqm.owner.log(result);
                }else
                if(dqm.data instanceof Set){
                    List<RegisteredValueSet> list = new ArrayList<>();
                    String result = dbase.read(list, dqm.owner.getTableName(), (Set<Register>)dqm.data, dqm.keys);
                    dqm.owner.readBack(list);
                    if (log.getParent().getLevel() == Level.FINE)
                        dqm.owner.log(result);
                }

            }
        };
        daoMessagesThread.start();
    }

    protected abstract void createTables()throws IOException;

    protected void createTable(String nameTable, Set<Register>table) {
        Map<String, Set<Register>> m = RegisteredValueSet.getSetGrouped(table);
        for(Map.Entry<String, Set<Register>> pair: m.entrySet()) {
            String query = String.format("SELECT COUNT(*) FROM " + nameTable + pair.getKey());
            try {
                dbase.rawQuery(query);
            } catch (SQLException e) {
                try{
                    dbase.createTable(nameTable, pair.getValue());
                }catch(Exception exp){
                    log.log(Level.SEVERE,"Unable to create tables", exp);
                }
            }

        }
    }

    protected void connect() throws IOException {
        dbase.init();
        createTables();
    }

    @Override
    public void requestMessage(RequestOwner owner, Set<Register> set, RegisteredValueSet keys) {
        daoMessagesThread.add(new DataQM(owner, set, keys));
    }

    protected void sendMessage(RequestOwner owner, RegisteredValueSet rvs, RegisteredValueSet keys) {
        daoMessagesThread.add(new DataQM(owner, rvs, keys));
    }

    public void close(){
        while(daoMessagesThread.getQueue().size() != 0);
        daoMessagesThread.interrupt();
        try {
            daoMessagesThread.join();
            dbase.close();
        } catch (Exception ignored) {}
    };

}
