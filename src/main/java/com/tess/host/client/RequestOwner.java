package com.tess.host.client;

import com.tess.modbus.register.RegisteredValueSet;

import java.util.List;
import java.util.logging.*;

/**
 * Created by Admin on 12.07.2018.
 */
public interface RequestOwner extends Logger {
    void readBack(List<RegisteredValueSet> list);
    String getTableName();
}
