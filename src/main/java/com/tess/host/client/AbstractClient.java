package com.tess.host.client;

import com.tess.modbus.register.Register;
import com.tess.modbus.register.RegisteredValueSet;
import com.tess.modbus.register.Registers;
import com.tess.modbus.register.RegistersFactory;
import com.tess.modbus.types.ArchiveClock;

import java.util.LinkedList;
import java.util.Set;

/**
 * Created by Admin on 27.08.2019.
 */
public abstract class AbstractClient implements ClientClass {
    protected Registers registers = null;
    private  LinkedList<OutputClient>outputBox = new LinkedList<>();
    private  InputClient inputClient = null;

    public AbstractClient(String registersName) {
        this.registers = RegistersFactory.load(registersName);
    }

    @Override
    public Registers getRegisters() {
        return registers;
    }

    @Override
    public void requestMessage(RequestOwner owner, Set<Register> set, RegisteredValueSet keys) {
        inputClient.requestMessage(owner, set, keys);
    }


    @Override
    public void pushArchive(RequestOwner owner, ArchiveClock from, RegisteredValueSet set) {
        if (set != null) {
//            RegisteredValueSet keys = set.pickOut("Date_, Drill");
            for(OutputClient oc : outputBox){
                oc.pushArchive(owner, from, set);
            }
        }
    }

    @Override
    public void pushCurrent(RequestOwner owner, RegisteredValueSet set){
        for(OutputClient oc : outputBox){
            oc.pushCurrent(owner, set);
        }
    }

    protected void addOutputClient(OutputClient outputClient){
        outputBox.add(outputClient);
    }

    protected void setInputClient(InputClient inputClient){
        this.inputClient = inputClient;
    }


}
