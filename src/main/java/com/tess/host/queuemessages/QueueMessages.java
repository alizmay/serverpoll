package com.tess.host.queuemessages;

import java.lang.invoke.MethodHandles;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Admin on 26.08.2019.
 */
public abstract class QueueMessages<T> extends Thread implements QueueMessageAction{
    static Logger log = Logger.getLogger(MethodHandles.lookup().toString());
    ConcurrentLinkedQueue<T> queue = new ConcurrentLinkedQueue<>();
    public QueueMessages(String threadName){
        super(threadName);
    }

    public synchronized void add(T item){
        queue.add(item);
    }

    public ConcurrentLinkedQueue<T> getQueue(){
        return queue;
    }

    @Override
    public void run() {
        try{
            while(!isInterrupted()){
                while(queue.size() > 0){
                    action();
                    TimeUnit.MILLISECONDS.sleep(100);
                }
            }
        }catch(InterruptedException e){
            log.log(Level.SEVERE, getName() + " thread is stopped", e);
        }
    }
}
