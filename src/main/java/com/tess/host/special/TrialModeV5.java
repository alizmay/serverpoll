package com.tess.host.special;

import com.tess.dataLinks.ListenService;
import com.tess.host.modbus.StuHost;
import com.tess.modbus.register.Register;
import com.tess.modbus.register.RegisteredValueSet;
import com.tess.modbus.register.TypeMapping;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Locale;

/**
 * Created by Admin on 10.04.2019.
 */
public class TrialModeV5 {
    final static byte[] PREAMBLE = new byte[]{(byte) 0xFE, (byte) 0xA5};
    private StuHost stu;
    private ListenService listenService;
    private float scale;
    interface OutletProcessing{
        String process(byte[] array, StuHost stu, float scale);
    }
    enum OutletType implements OutletProcessing {

        FREQ_OTLET {
            @Override
            public String process(byte[] array, StuHost stu, float scale) {
                if (array.length != 6) return null;
                Register r = stu.getRegisters().getRegister("Measure:q1");
                ByteBuffer bb = ByteBuffer.wrap(array);
                bb.order(ByteOrder.LITTLE_ENDIAN);
                float ratio = Float.parseFloat(r.getTypeMapping().transformation(bb, 2));
                if(ratio == 0)return "0.000";
                return String.format(Locale.ENGLISH, "%.3f", scale / ratio);
            }
        },
        IMPULSE_OUTLET {
            @Override
            public String process(byte[] array, StuHost stu, float scale) {
                if (array.length != 8) return null;
                Register q1 = stu.getRegisters().getRegister("Measure:q1");
                Register i  = new Register("Nonce", -1, new TypeMapping("INT"));
                ByteBuffer bb = ByteBuffer.wrap(array);
                bb.order(ByteOrder.LITTLE_ENDIAN);
                float q1s = Float.parseFloat(q1.getTypeMapping().transformation(bb, 2));
                int nn = Integer.parseInt(i.getTypeMapping().transformation(bb,6)) & 0xFFFF;
                double v1 = scale/500/3600.0 * nn;
                return String.format("q1=%.3f N=%d V1=%.6f", q1s, nn, v1);
            }
        }
    }


    private OutletType outletType = OutletType.IMPULSE_OUTLET;
    public TrialModeV5(StuHost stu) {
        this.stu = stu;
    }

    public void startTrial() throws IOException {
        RegisteredValueSet rvs = stu.read(stu.getRegisters().get("Param:Scale1"));
        stu.write(stu.getRegisters().getRvs("trial=ON"));
        scale = Float.parseFloat(rvs.getRegisteredValue(0).getValue());
        listenService = new ListenService(stu.getPortal(), 200);
        listenService.start();
    }

    public void stopTrial() throws IOException {
        listenService.stop();
        stu.write(stu.getRegisters().getRvs("trial=OFF"));
    }

    public String getFlow1StringMessage() {
        if (listenService.isDataReady() == false) return null;
        byte[] array = listenService.getMessages();
        if (array[0] != PREAMBLE[0] || array[1] != PREAMBLE[1]) return null;
        return outletType.process(array, stu, scale);
//        if (array.length != 6) return null;
//        Register r = stu.getRegisters().getRegister("Measure:q1");
//        ByteBuffer bb = ByteBuffer.wrap(array);
//        bb.order(ByteOrder.LITTLE_ENDIAN);
//        float ratio = Float.parseFloat(r.getTypeMapping().transformation(bb, 2));
//        if(ratio == 0)return "0.000";
//        return String.format(Locale.ENGLISH, "%.3f", scale / ratio);

    }
}
