package com.tess;



import com.tess.host.client.ClientClass;
import com.tess.host.client.ClientTTF3;
import com.tess.host.clients.ClientChooser;
import com.tess.host.clients.GPRSClientSpecific;
import com.tess.modbus.ModbusBootloadEntrance;
import com.tess.modbus.register.Register;
import com.tess.modbus.register.RegisteredValue;
import com.tess.modbus.register.RegisteredValueSet;
import com.tess.modbus.register.TypeMapping;
import com.tess.multiserver.MultiModbusServer;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.eclipse.paho.client.mqttv3.MqttException;


import java.io.*;

import java.lang.invoke.MethodHandles;
import java.net.URI;
import java.net.URISyntaxException;
import java.sql.SQLException;
import java.util.Properties;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;


/**
 * Main poll manager
 * -GprsCfg=com.rtu://COM2?1 E:\tools\JavaProjects\TessBox\script.txt
 * Created by TESS on 06.06.2017.
 */
public class ServerPollManager {
    static Logger log = Logger.getLogger(MethodHandles.lookup().toString());

    public static final String COMMAND_EXIT = "exit";
    public static final String COMMAND_READ = "read ";
    public static final String COMMAND_WRITE = "write ";
    public static final String COMMAND_BSL_ENTRANCE = "bsl";
    public static int depthHours = 12;
    private static ClientTTF3 client;
    private static boolean mqttEnabled = true;

    public static Properties loadProperties() {
        Properties prop = new Properties();
        try (InputStream is = new FileInputStream(CfgRoot.PATH + CfgRoot.APPLICATION_PROPERTIES)) {
            prop.load(is);
            depthHours = Integer.parseInt(prop.getProperty("DepthHours"));

            //client = ClientChooser.getClient(prop.getProperty("Client"));
            //MultiModbusServer.serverPort  = Integer.parseInt(prop.getProperty("portListen"));
            String ipFilters[] = prop.getProperty("FilterIP").split("-");
            if(ipFilters.length == 2)MultiModbusServer.setIpFilter(ipFilters[0].trim(), ipFilters[1].trim());
            StringWriter sw = new StringWriter();
            String me = prop.getProperty("mqtt");
            if(me == null || me.equals("disabled"))mqttEnabled =  false;
            prop.list(new PrintWriter(sw));

//            uri = new URI(prop.getProperty("addressUri"));
//            method = prop.getProperty("method", "add");
//            showMenuDescription = Boolean.parseBoolean(prop.getProperty("showMenuDescription","false"));
            log.fine("Application config was loaded successfully");

        }catch (Exception e) {
            System.out.println("Load application config failure:" + e.getMessage());
            log.severe(getTrackStackString(e));
            throw new RuntimeException(e);
        }
        log.info("The depth of the request is made up of " + depthHours + " hours.");
        return prop;
    }

    //write GprsConfig:IP=192.168.84.255
    //write GprsConfig:Alarm=0.0.0 0:15:0
    //write ADay="16.xlsx"
    public static void consoleWindow(String suri, String fileScript){
        URI uri;
        try{
            uri = new URI(suri);
        } catch (URISyntaxException e) {
            Console.outt(e.getMessage());
            throw new RuntimeException("Error URI parsing", e);
        }

        try(Stu stu = new Stu(uri, null)/*new Stu(uri, new SQLite())*/){
            stu.connect();
            Console.outt("Stu was created and opened successfully");
            if(fileScript != null){
                String command = null;
                try(BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(fileScript)))){
                    String line;
                    StringBuilder sb = new StringBuilder();
                    while((line = br.readLine()) != null){
                        sb.append(line);
                        sb.append(',');
                    }
                    sb.deleteCharAt(sb.length() - 1);
                    command = sb.toString();
                }catch (Exception e){}
                if(command != null){
                    try{
                        stu.forceRegistersArray(false, command);
                        Console.outt("Written successfully");
                    }catch (IOException io){
                        Console.outt(io.getMessage());
                    }
                }
                return;
            }
            Console reply = new Console();
            while(true){
                String r = reply.read();
                String str[] = r.split(",");
                if(COMMAND_EXIT.equals(str[0]))break;
                if(COMMAND_BSL_ENTRANCE.equals(str[0])){
                    stu.bslEntry();
                }
                if(str[0].contains(COMMAND_READ)){
                    if(str[0].contains(" 0x")){
                        try {
                            int start = str[0].indexOf("0x");
                            String s = str[0].substring(start+2);
                            String[] sss = s.split(" ");
                            int address = Integer.parseInt(sss[0], 16);
                            if(false) {
                                TreeSet<Register> ss = new TreeSet<>();
                                for (int i = 0; i < 50; i++) {
                                    ss.add(new Register(String.format("explicit%d", i), address + i, new TypeMapping("INT")));
                                }
                                RegisteredValueSet rvs = stu.readRegistersFromPortal(ss);
                                for(RegisteredValue rv:rvs)
                                    Console.outt(rv.toString());
                                Console.outt("Press the button");
                                Thread.sleep(5000);
                                RegisteredValueSet rr = stu.readRegistersFromPortal(ss);
                                for(RegisteredValue rv:rr)
                                    Console.outt(rv.toString());
                                Console.outt("Differecies:...............");
                                for(RegisteredValue r1:rvs)
                                        if(!r1.getValue().equals(rr.getRegisteredValue(r1.getRegister().getMark()).getValue()))
                                            Console.outt(r1 + " ||| " + rr.getRegisteredValue(r1.getRegister().getMark()));

                            }else {
                                Register reg = new Register("Measure:explicit", address, new TypeMapping(sss[1]));
                                TreeSet<Register> ss = new TreeSet<>();
                                ss.add(reg);
                                RegisteredValueSet rvs = stu.readRegistersFromPortal(ss);
                                Console.outt(rvs.getRegisteredValue(0).toString());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }else {
                        str[0] = str[0].substring(COMMAND_READ.length());
                        try {
                            RegisteredValueSet rvs;
                            if (str.length == 1 && !(str[0].contains(":"))) rvs = stu.fetchRegisters(str[0], false);
                            else rvs = stu.fetchRegistersArray(false, str);
                            //for (RegisteredValue rv : rvs) Console.outt(rv.toString());
                            Console.outt(rvs.toString());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
                if(str[0].contains(COMMAND_WRITE)){
                    str[0] = str[0].substring(COMMAND_WRITE.length());
                    try{
                        //String sss = "";
                        //for(int i = 0; i < str.length; i++) sss += str[i] + ',';
                        //stu.forceRegistersArray(false, sss);
                        stu.getStuHost().write(str[0]);
                        Console.outt("Written successfully");
                    }catch(Exception e){
                        e.printStackTrace();
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void startServer() throws MqttException, SQLException, IOException {
  //      Console.outt("MultiModbusServer is being lunched.");
        log.info("MultiModbusServer is being lunched.");
        client = new ClientTTF3();
        client.setMqqtEnable(mqttEnabled);
        client.init();
        MultiModbusServer mms = new MultiModbusServer(client);
        mms.run();
//        Thread ser = new Thread(mms);
//        ser.start();
//        try {
//            ser.join();
//        } catch (InterruptedException e) {

            log.info("MMS has finished its work");
//        }
        mms.stop();
        client.close();
    }



    public static String getTrackStackString(Exception e){
        StringWriter sw = new StringWriter();
        e.printStackTrace(new PrintWriter(sw));
        return sw.toString();
    }

    public static void main(String arg[]) throws Exception {
        CfgRoot.initLogger();
        log.info("Version: 2.2.");
        loadProperties();
        if(arg.length > 0 && arg[0].contains("-GprsCfg="))
            consoleWindow(arg[0].substring(9), arg.length==2?arg[1]:null);
        else startServer();
    }
}

//    private static final String DEVICE = "URG2KM";
//    private static final int VERSION  = 2000;
//    private static ServerSocket ss;
//    public final static int serverPort = 502;
//    private static ServerPollManager instance = null;
//
//    public static ServerPollManager getInstance(){
//        if(instance == null)instance = new ServerPollManager();
//        return instance;
//    }
//
//    protected ServerPollManager(){
//    }
//
//    public static void main(String[] arg) throws IOException, InterruptedException, ParseException {
//        Console.out("Server is running...");
//        ss = new ServerSocket(serverPort);
//        Socket socket = ss.accept();
//        PollHandler ph = new PollHandler(socket);
//        ph.start();
//        ph.join();
//    }
//
//
//    private static  class PollHandler extends Thread{
//        private Socket socket;
//
//        public PollHandler(Socket socket) {
//            this.socket = socket;
//        }
//
//        public void run(){
//            Console.out("New client has been found: " + socket.getInetAddress());
//            AcceptedPortal sp;
//            try{
//                sp = new AcceptedPortal("RTU", socket);
//            }catch(Exception e){
//                Console.out(e.getFlow1StringMessage());
//                Console.log(e.getFlow1StringMessage());
//                return;
//            }
//            try(PortalBus pb = new PortalBus(sp)){
//                pb.connect(null);
//            }catch (Exception e){
//                Console.out(e.getFlow1StringMessage());
//                Console.log(e.getFlow1StringMessage());
//            }
///*            Console.out("New client has been found: " + socket.getInetAddress());
//
//            try(ServerPortal sp = new ServerPortal("")) {
//                sp.open(socket);
//                ModbusDeviceID mdid = new ModbusDeviceID(sp);
//                List<RegisteredValueSet> mdidList =  mdid.readRegister(1, null);
//                RegisteredValueSet rvs = mdidList.getRegister(0);
//                //for(RegisteredValue rv:rvs)Console.out(rv.toString());
//                Console.out(rvs.getRegisteredValue("DeviceHead:SerNo").toString());
//            }catch (Exception e){
//                Console.out(e.getFlow1StringMessage());
//                Console.log(e.getFlow1StringMessage());
//            }*/
//        }
//    }
//}
