package com.tess;

import com.tess.dao.DBase;
import com.tess.dao.DataBase;
import com.tess.dataLinks.AcceptedPortal;
import com.tess.dataLinks.ComRTU;
import com.tess.dataLinks.InetPortal;
import com.tess.dataLinks.Portal;
import com.tess.host.dao.DaoHost;
import com.tess.host.modbus.StuHost;
import com.tess.modbus.*;
import com.tess.modbus.register.*;
import com.tess.modbus.types.ArchiveClock;
import com.tess.modbus.types.Clock;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.sql.SQLException;
import java.util.*;
import java.util.logging.Logger;

/**
 * Device class for modbus devices
 * Created by TESS on 14.06.2017.
 */
public class Stu implements AutoCloseable {
    private static final boolean debug = true;
    private static final boolean debugTypeArchiveTwoMin = false;
    static Logger log = Logger.getLogger(Stu.class.getName());
    private int deviceID = 0;
    private Registers registers;
    //private Registers common;
    //private Portal portal = null;
    //private DBase dbase;
    private byte netAddress;
    private boolean autoCreateTables = true, autoCreateRecords = true;
    private URI uri;
    private StuHost stuHost = null;
    private DaoHost daoHost = null;
    private DBase dbase = null;
//    ModbusRegisterRead modbusRegisterRead = null;
//    ModbusRegisterWrite modbusRegisterWrite = null;
//    ModbusArchive modbusArchive = null;
//    RegisteredValueSet deviceHead = null;
//    ModbusCoilWrite modbusCoilWrite = null;

    public Stu(AcceptedPortal p, DataBase sqlDataBase, int netAddress) throws IOException, SQLException {
        stuHost = new StuHost(p);
        daoHost  = new DaoHost(sqlDataBase);
        dbaseInit();
        String suri = String.format("tcpaccepted.rtu://%s?%d", p.getSocket().getInetAddress()+":"
                + p.getSocket().getLocalPort(), netAddress);
        try {uri = new URI(suri);} catch (URISyntaxException e) {uri =null;}
        //init(uri);
    }

    public Stu(DataBase sqlDataBase) throws IOException, SQLException {
        daoHost = new DaoHost(sqlDataBase);
        dbaseInit();
    }

    public Stu(URI uri) throws IOException {
        init(uri);
    }

    /**
     * Create an interface without connection to a device
     * @param uri such as com.rtu://COM2?1#9600baud,2tries,2000ms
     * @throws IOException
     */
    public Stu(URI uri, DataBase db) throws IOException {
        daoHost = new DaoHost(db);
        init(uri);
    }

    private  void init(URI uri) throws IOException {
        if(uri == null)throw new IOException("URI is empty");
        if(!uri.toString().contains("?"))throw new IOException("NetAddress required");
        this.uri = uri;
        String scheme, type, pathLocal;
        try {
            scheme = uri.getScheme().split("\\.")[0].toUpperCase();
            type = uri.getScheme().split("\\.")[1].toUpperCase();
            pathLocal = uri.getHost();
        }catch(Exception e){throw new IOException("Error URI processing in init:", e);}
        try{
            netAddress = (byte)((Integer.parseInt(uri.getQuery())) & 0xFF);
            switch(scheme){
                case "COM":
                    if(stuHost != null)throw new IOException("Portal has already opened");
                    if("RTU".equals(type)){
                        stuHost = new StuHost(new ComRTU());
                        stuHost.open(pathLocal, uri.getFragment());
                    }
                    break;
                case "TCPCLIENT":
                    //TODO RTU ASCII TCP
                    if(stuHost != null)throw new IOException("Portal has already opened");
                    stuHost = new StuHost(new InetPortal(type));
                    stuHost.open(pathLocal+ ":" + uri.getPort(), uri.getFragment());
                    break;
                case "TCPACCEPTED":  //Here already socket is exist and opened
                    //TODO RTU ASCII TCP
                    //if(portal == null)throw new IOException("No portal detected");
                    break;
                default:throw new IOException("Unknown type of URI has found");
            }
        }catch (Exception e){throw new IOException(e.getMessage());}
    }

    private void dbaseInit() throws IOException, SQLException {
        daoHost.connect();
        daoHost.createTables("Common", RegistersFactory.getCommon().getAll());
    }

    /**
     * Create connection via interfaces. Create deviceHead set.
     * @throws IOException in failure
     */
    private void portalConnect() throws IOException {
        RegisteredValueSet deviceHead = stuHost.read();
    }

    /**
     * Look up into the Common.DeviceHead table for dbaseHead. If autoCreateTable is enabled then program
     * creates respective tables.
     * @param type such as "URG2KM 4."
     * @param serno such as "00001"
     * @throws IOException if no one has been found
     */
    private void dbaseConnect(String type, String serno) throws IOException{
//        if(daoHost == null)return;
//        RegisteredValueSet rvs = new RegisteredValueSet();
//
//        rvs.add(new RegisteredValue(RegistersFactory.getCommon().getRegister("DeviceHead:SerNo"), serno));
//        List<RegisteredValueSet> list;
//        try {
//            //TODO list = dbase.select(RegistersFactory.getCommon().getSetName(), RegistersFactory.getCommon().getSetByGroup("DeviceHead"), rvs);
//            if(list.size() == 0) throw new IOException("No such device has been detected");
//            int indexList = 0;
//            if(list.size() > 1){
//                String tt = type.substring(0, type.indexOf('.'));
//                for(indexList = 0; indexList < list.size(); indexList++){
//                   RegisteredValueSet regs = list.get(indexList);
//                   if(regs.getRegisteredValue("DeviceHead:Device").getValue().contains(tt))break;
//                }
//            }
//            //TODO deviceHead = list.get(indexList);
//            //if (list.size() > 1) throw new IOException("Wrong amount of devices");
//            //TODO deviceID = Integer.parseInt(deviceHead.getRegisteredValue("DeviceHead:DeviceID").getValue());
//            //String version = list.getRegister(0).getRegisteredValue("DeviceHead:Version").getValue();
//            //int version = Integer.parseInt(list.getRegister(0).getRegisteredValue("DeviceHead:Version").getValue());
//
//
//            registers = RegistersFactory.getInstance(deviceHead.getRegisteredValue("DeviceHead:Device").getValue());
//
//            if(autoCreateTables){
//                dbase.createTable(registers.getSetName(), registers.getSetByGroup("Measure"));
//                dbase.createTable(registers.getSetName(), registers.getSetByGroup("Archive"));
//                dbase.createTable(registers.getSetName(), registers.getSetByGroup("Params"));
//            }
//        }
//        catch (IOException e) {throw new IOException(e.getFlow1StringMessage());}
//        catch (SQLException e) {throw new IOException(e.getFlow1StringMessage(), e);}
    }

    private int insertHelper(final RegisteredValueSet toInsert, final String setName) throws IOException {
//        try {
//            return dbase.insert(setName, toInsert);
//        } catch (SQLException e) {throw new IOException(e.getFlow1StringMessage());}
return 0;
    }
    private int updateInsertHelper(RegisteredValueSet toInsert, RegisteredValueSet keys, String setName) throws IOException {

//        if (keys == null) {
//            return insertHelper(toInsert, setName);
//        }else {
//            try {
//                return dbase.update(setName, toInsert, keys);
//            } catch (SQLException e){
//                toInsert.add(keys);
//                try{
//                    return insertHelper(toInsert, setName);
//                }catch(IOException e1){
//                    throw new IOException("Update and Insert operations are failed",e1);}
//            }
//        }
        return 0;
    }

    public void connect() throws IOException {
        stuHost.read();
//        if(portal == null || uri == null)throw new IOException("No portal or URI has been detected");
//        portalConnect();
//        String type = deviceHead.getRegisteredValue("DeviceHead:Device").getValue();
//        String serno= deviceHead.getRegisteredValue("DeviceHead:SerNo").getValue();
//        if(dbase == null){
//            RegistersFactory.setParserTypeName(new AtlasParserTypeName());
//            registers = RegistersFactory.getInstance(deviceHead.getRegisteredValue("DeviceHead:Device").getValue());
//            return;
//        }
//        try{
//            dbaseConnect(type, serno);
//        }catch(IOException e){
//            //Create new record in Common.DeviceHead
//            deviceHead.add(new RegisteredValue(RegistersFactory.getCommon().getRegister("DeviceHead:URI"),uri.toString()));
//            //deviceHead.getRegisteredValue("DeviceHead:URI").setValue(uri.toString());
//            deviceHead.delete("DeviceHead:DeviceID");
////TODO check whether is null in value
//            updateInsertHelper(deviceHead, null, "Common");
//            dbaseConnect(type, serno);
//            log.info/*Console.out*/("New device has been added");
//        }
//        String clock = new Clock().toString();
//        RegisteredValueSet ss = new RegisteredValueSet();
//        ss.add(deviceHead.getRegisteredValue("DeviceHead:LastAttempt"));
//        ss.add(deviceHead.getRegisteredValue("DeviceHead:URI"));
//        deviceHead.getRegisteredValue("DeviceHead:LastAttempt").setValue(clock);
//        deviceHead.getRegisteredValue("DeviceHead:URI").setValue(uri.toString());
//        set(ss);//set("DeviceHead:LastAttempt", clock);
//
////        deviceHead.getRegisteredValue("DeviceHead:LastAttempt").setValue(clock);
////        deviceHead.getRegisteredValue("DeviceHead:LastSession").setValue(clock);
////        updateInsertHelper(deviceHead, deviceHead.getDeviceIDKeys(RegistersFactory.getCommon(), deviceID), "Common");
    }

    public void connect(String type, String serno) throws IOException {
        dbaseConnect(type, serno);
    }

    public void createPortal() throws IOException {
//        if(deviceHead == null ||deviceHead.size() == 0)throw new IOException("Database connection must be done at first");
//        try{uri = new URI(deviceHead.getRegisteredValue("DeviceHead:URI").getValue());}catch(URISyntaxException e){throw new IOException(e.getFlow1StringMessage());}
//        init(uri);
//        portalConnect();
//        String ct = new Clock().toString();
//        set("DeviceHead:LastAttempt", ct);
    }

//    public RegisteredValueSet getDeviceHead() {
//        return deviceHead;
//    }

    public RegisteredValueSet readRegistersFromPortal(TreeSet<Register> set) throws IOException {
//        if(modbusRegisterRead == null )modbusRegisterRead = new ModbusRegisterRead(portal, netAddress);
//        return modbusRegisterRead.readRegister(set);

        return stuHost.read(set);
    }

    /**
     * Read registers from portal and write them to the dbase if it's necessary. Doesn't work with Common and Archive
     * groups
     * @param group as "Measure", "Params", "GprsConfig" and so on
     * @param dbaseUpdate if it true the results will be written to the dbase
     * @return Set of group
     * @throws IOException
     */
    public RegisteredValueSet fetchRegisters(String group, boolean dbaseUpdate) throws IOException {
        return stuHost.read(stuHost.getRegisters().getSetByGroup(group));
//        if(portal == null || netAddress == 0)throw new IOException("It's necessary to create portal at first");
//        RegisteredValueSet rvs = readRegistersFromPortal(registers.getSetByGroup(group));
//        if(dbaseUpdate) {
//            RegisteredValueSet keys = rvs.getDeviceIDKeys(null, deviceID); //Here is certainly explicit set of registers
//            updateInsertHelper(rvs, keys, registers.getSetName());
//        }
//        return rvs;
//        return null;
    }

    /**
     * Reading registers from portal.
     * @param input string that can consist of several registers, Only full marks
     * @param dbaseUpdate whether is need updating tables
     * @return RegisteredValueSet result
     * @throws IOException
     */
    public RegisteredValueSet fetchRegisters(boolean dbaseUpdate, String input)throws IOException{
        if(input == null || input.isEmpty())throw new IOException("No incoming data detected");
        String regs[] = input.split(",");
        if(regs.length == 0){regs = new String[1];regs[0] = input;}
        return fetchRegistersArray(dbaseUpdate, regs);
    }

    public RegisteredValueSet fetchRegistersArray(boolean dbaseUpdate, String...regs) throws IOException {
        return stuHost.read(stuHost.getRegisters().get(regs));
//        if(regs.length == 0)throw new IOException("Wrong incoming data");
//        if(portal == null || netAddress == 0)throw new IOException("It's necessary to create portal at first");
//        TreeSet<Register>rs = registers.get(regs);
//        if(rs == null)throw new IOException("No suitable registers are found");
//        RegisteredValueSet rvs = readRegistersFromPortal(rs);
//        if(dbaseUpdate){
//            RegisteredValueSet keys = rvs.getDeviceIDKeys(null, deviceID);
//            updateInsertHelper(rvs, keys, registers.getSetName());
//        }
//        return rvs;
//        return null;
    }

    private void writeRegistersToPortal(RegisteredValueSet into) throws IOException{
//        RegisteredValueSet coilsSet = new RegisteredValueSet();
//        Iterator<RegisteredValue>iterator = into.iterator();
//        while(iterator.hasNext()){
//            RegisteredValue rv = iterator.next();
//            if(rv.getRegister().getTypeMapping().toString().contains("COIL")){
//                coilsSet.add(rv);
//                iterator.remove();
//            }
//        }
//        if(modbusRegisterWrite == null)modbusRegisterWrite = new ModbusRegisterWrite(portal, netAddress);
//        if(into.size() > 0)modbusRegisterWrite.writeRegister(into);
//        if(modbusCoilWrite == null)modbusCoilWrite = new ModbusCoilWrite(portal, netAddress);
//        if(coilsSet.size() > 0)modbusCoilWrite.writeCoil(coilsSet);

    }

    public void forceRegisters(RegisteredValueSet in) throws IOException{
//        if(portal == null || netAddress == 0)throw new IOException("It's necessary to create portal at first");
//        writeRegistersToPortal(in);
        stuHost.write(in);
    }

    public void forceRegisters(boolean dbaseUpdate, String input)throws IOException {
        if (input == null || input.isEmpty()) throw new IOException("No incoming data detected");
//        String regs[] = input.split(",");
//        if(regs.length == 0){regs = new String[1];regs[0] = input;}
        forceRegistersArray(dbaseUpdate, input);

    }
    public void forceRegistersArray(boolean dbaseUpdate, String regvals)throws IOException{
//        RegisteredValueSet rvs = new RegisteredValueSet();
//        for(String s:regvals){
//            String ss[] = s.split("=");
//            if(ss.length != 2)continue;
//            Registers rgs = ss[0].contains("DeviceHead")?RegistersFactory.getCommon():registers;
//            rvs.add(new RegisteredValue(rgs.getRegister(ss[0].trim()), ss[1].trim()));
//        }
//        if(rvs.size() == 0)throw new IOException("Unknown registers");
//        forceRegisters(rvs);
        RegisteredValueSet rvs = stuHost.getRegisters().getRvs(regvals);
        forceRegisters(rvs);
    }


    public RegisteredValueSet getByGroup(String group) throws IOException, SQLException {
        Set<Register> set = registers.getSetByGroup(group);
        if(set == null || set.size() == 0)throw new IOException("No such group");
        RegisteredValueSet keys = new RegisteredValueSet(){{add(new RegisteredValue(registers.getRegister("Measure:DeviceID"),
                String.valueOf(deviceID)));}};
        List<RegisteredValueSet> list = dbase.select(registers.getSetName(), set, keys);
        return list.get(0);
    }

    public RegisteredValue get(String mark) throws IOException, SQLException {
        final Registers reggs = mark.contains("DeviceHead")?RegistersFactory.getCommon():registers; //TODO move to Registers
        Set<Register> set = new TreeSet<>();
        Register r = reggs.getRegister(mark);
        if(r == null)throw new IOException("No such mark");
        set.add(r);
        List<RegisteredValueSet> list = dbase.select(reggs.getSetName(), set,
                new RegisteredValueSet(){{add(new RegisteredValue(reggs.getByUnitMark("DeviceID"),
                        String.valueOf(deviceID)));}});
        return list.get(0).getRegisteredValue(mark);
    }

    public void set(String mark, String value) throws IOException {
        Registers rregs = registers;
        if(mark.contains("DeviceHead"))rregs = RegistersFactory.getCommon();
        RegisteredValueSet rvs = new RegisteredValueSet();
        RegisteredValue r = new RegisteredValue(rregs.getRegister(mark), value);
        rvs.add(r);
        RegisteredValueSet key = rvs.getDeviceIDKeys(rregs, deviceID);
        updateInsertHelper(rvs, key, rregs.getSetName());
    }

    public void set(RegisteredValueSet rvs)throws IOException{
        String group = null;
        for(RegisteredValue rv:rvs)
            if(group == null)group = rv.getRegister().getGroupMark();
            else if(!rv.getRegister().getGroupMark().equals(group))
                throw new IOException("Wrong incoming data. Rvs group must be the one");
        Registers rregs = registers;
        if(group.contains("DeviceHead"))rregs = RegistersFactory.getCommon();
        RegisteredValueSet keys = rvs.getDeviceIDKeys(rregs, deviceID);
        updateInsertHelper(rvs, keys, rregs.getSetName());
    }

    @SuppressWarnings("unchecked")
    public Map<ArchiveClock, RegisteredValueSet> getArchive(ArchiveClock from, ArchiveClock to) {
        Map<ArchiveClock, RegisteredValueSet> data = new HashMap<>();
        boolean dir = (from.asLong() < to.asLong());
        for (; from.asLong() != to.asLong(); from.increment(dir)) {
            int interval = from.getIntervalMinutes();
            if (interval > 1440) interval = 44640;
            RegisteredValueSet keySelect = new RegisteredValueSet();
            keySelect.add(new RegisteredValue(registers.getRegister("Archive:DeviceID"), String.valueOf(deviceID)));
            keySelect.add(new RegisteredValue(registers.getRegister("Archive:Interval"), String.valueOf(interval)));
            keySelect.add(new RegisteredValue(registers.getRegister("Archive:Datetime"), from.toString()));
            try {
                List<RegisteredValueSet> reply = dbase.select(registers.getSetName(), registers.getSetByGroup("Archive"), keySelect);
                assert (reply.size() == 1);
                data.put(new ArchiveClock(from), reply.get(0));
            } catch (Exception ignored) {
            }
        }
        return data;
    }

    private int archiveUploadHelper(Map<ArchiveClock, RegisteredValueSet>archiveUpload) throws IOException {
        int totalRows = 0;
        for(Map.Entry<ArchiveClock, RegisteredValueSet> pair:archiveUpload.entrySet()){
            ArchiveClock date = pair.getKey();
            RegisteredValueSet value = pair.getValue();
            int interval = date.getIntervalMinutes(); //TODO look at the month type behavior
            if(interval > 1440)interval = 44640;
            RegisteredValueSet keySelect = new RegisteredValueSet();
            keySelect.add(new RegisteredValue(registers.getRegister("Archive:DeviceID"), String.valueOf(deviceID)));
            keySelect.add(new RegisteredValue(registers.getRegister("Archive:Interval"), String.valueOf(interval)));
            keySelect.add(new RegisteredValue(registers.getRegister("Archive:Datetime"), date.toString()));
            totalRows += updateInsertHelper(value, keySelect, registers.getSetName());
        }
        return totalRows;
    }

    public int archiveUploadHelperTest(Map<ArchiveClock, RegisteredValueSet>archiveUpload) throws IOException{
        return archiveUploadHelper(archiveUpload);
    }

    private Map<ArchiveClock, RegisteredValueSet> readArchive(ArchiveClock from, ArchiveClock to)throws IOException{
//        if(modbusArchive == null)modbusArchive = new ModbusArchive(portal, netAddress);
//        Set<Register>regs = registers.getSetByGroup("Archive");
//        return modbusArchive.readArchive(from, to, regs);
        return null;
    }

    public Map<ArchiveClock, RegisteredValueSet> fetchArchive(ArchiveClock from, ArchiveClock to)throws IOException{
        Map<ArchiveClock, RegisteredValueSet> archive = readArchive(from, to);
        archiveUploadHelper(archive);
        return archive;
    }


    public Map<ArchiveClock, RegisteredValueSet> fetchArchive(ArchiveClock.ArchiveMode type) throws SQLException, IOException {
//        ArchiveClock to = new ArchiveClock(type/*ArchiveClock.ArchiveMode.HOUR*/);
//        to.increment(false);
//        String f = get("DeviceHead:LastSession").getValue();
//        ArchiveClock from;
//        if(f == null || f.isEmpty()){
//            f = new Clock().toString();
//            from = new ArchiveClock(type/*ArchiveClock.ArchiveMode.HOUR*/, f).increment(false);
//            from.increment(false);
//        }else from = new ArchiveClock(type/*ArchiveClock.ArchiveMode.HOUR*/, f);
//        if(to.asLong()==from.asLong())return new HashMap<>();
//        log.fine("#" + deviceHead.getRegisteredValue("DeviceHead:SerNo").getValue() + " is going to read " + from.getArchiveMode()+" archive:" + from.toString()+" - "+to.toString());
//        Map<ArchiveClock, RegisteredValueSet> mm = readArchive(from , to);
//        archiveUploadHelper(mm);
//        if(mm.size() > 0) {
//            ArchiveClock lastDate = Collections.max(mm.keySet());
//            if(debug) {
//                ArchiveClock ac;
//                if(debugTypeArchiveTwoMin){
//                    ac  = new ArchiveClock(ArchiveClock.ArchiveMode.TWOMIN);
//                    ac.addUnits(-12);
//                }else {
//                    ac = new ArchiveClock(ArchiveClock.ArchiveMode.HOUR);
//                    ac.addUnits(-24);
//                }
//                set("DeviceHead:LastSession", ac.toString());
//                log.fine("(debug edition)");
//            }else
//                set("DeviceHead:LastSession", lastDate.toString());
//        }
//        return mm;
        return null;
    }

    public boolean timeCorrection() throws IOException {
        RegisteredValueSet rvs = fetchRegisters(false, "Measure:sysclock");
        Clock currentTime = new Clock();
        boolean done = false;
        Clock systime = new Clock(rvs.getRegisteredValue("Measure:sysclock").getValue());
        if (currentTime.asLong() != systime.asLong() || (debug)) {
            rvs.getRegisteredValue("Measure:sysclock").setValue(currentTime.toString());
            if(debug)log.fine("(Timecorrection caused by debug edition)");
            try {
                forceRegisters(rvs);
                done = true;
            } catch (IOException e) {
                throw new IOException("Unavailable to perform writing time to portal");
            }
        }
        return done;
    }

    public StuHost getStuHost() {
        return stuHost;
    }

    public void bslEntry() throws IOException {
        stuHost.bslEntry();
    }

    public void close() {
//        try {
//            log.fine/*Console.out*/("Stu is being closed. All connections will be cut off.");
//            dbase.close();
//            if(portal != null)portal.close();
//        } catch (Exception ignored) {}
    }
}
